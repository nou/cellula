/**************************************************************************
*   Cellula, is cellular automata simulator accelerated on GPU.           *
*   Copyright (C) 2011  Dušan Poizl                                       *
*                                                                         *
*   This program is free software: you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation, either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
***************************************************************************/

#include "CDparser/CDparser.h"
#include <QRegExp>
#include <QStringList>
#include "model.h"
#include "clutility.h"
#include "machine.h"
#include <sstream>
#include <math.h>
#include "rand/randomc.h"

typedef cl_float _time;
typedef cl_float _state;
typedef cl_uint _seed;

typedef struct
{
    _time time;
    _state state;
}_event;

typedef struct
{
    cl_int2 index;
    cl_int2 index2;
    _event queue[8];
}_queue;

typedef struct
{
    _seed seed;
    _state state;
    _queue queue;
}_cell;

AsynchronousModel::AsynchronousModel(Machine *parent, string model_definition) : Model(parent)
{
    queue_len = 8;
    neighbor = 8;
    init_kernel = 0;
    intermediate_kernel = 0;
    compile(model_definition);
    loadSource();
}

AsynchronousModel::~AsynchronousModel()
{
    clReleaseKernel(init_kernel);
    clReleaseKernel(intermediate_kernel);
}

int AsynchronousModel::getCellSize()
{
    //size_t event = sizeof(float)*2;
    //size_t cell = sizeof(int)*4+event*queue_len+event+sizeof(cl_uint);
    return sizeof(_cell);
}

int AsynchronousModel::loadSource()
{
    stringstream s;
    s << getKernelInclude() << " -D NEIGHBOR=" << neighbor << " -D QUEUE_LEN=" << queue_len;
    string source = neighbors;//"__constant int neighbors[] = {-1,1, 1,1, -1,-1, 1,-1, 0,1, -1,0, 1,0, 0,-1, 0,0 };\n";
    source += "#include \"asynchr.cl\"\n";
    source += transfer_functions;
    cout << source << endl;

    if(init_kernel){ clReleaseKernel(init_kernel); init_kernel = 0; }
    if(intermediate_kernel){ clReleaseKernel(intermediate_kernel); intermediate_kernel = 0; }

    if(build(source, s.str()))
    {
        error_message += getBuildLog(program);
        return -1;
    }

    init_kernel = clCreateKernel(program, "init", NULL);
    intermediate_kernel = clCreateKernel(program, "intermediateStep", NULL);
    return 0;
}

void AsynchronousModel::renderToTexture(CellSpace *pattern, int which, cl_mem tex)
{
    cl_int err_code;
    err_code = clSetKernelArg(render_kernel, 0, sizeof(cl_mem), pattern->getSpace());
    err_code |= clSetKernelArg(render_kernel, 1, sizeof(cl_mem), &tex);
    if(err_code){ cerr << "Nepodarilo sa nastavit argumenty "; clGetError(err_code); }
    parent->run(render_kernel, pattern->getWidth(), pattern->getHeight());
}

int AsynchronousModel::compile(string model_definition)
{
    DEVS::CDparser *parser = new DEVS::CDparser(model_definition);
    int ret = parser->parse();
    error_message = parser->error.str();
    neighbors = parser->neighbors();
    transfer_functions = parser->compiledSource();
    initvalue = parser->model.section.initialvalue;
    timewindow = parser->model.section.timewindow;
    width = parser->model.section.width;
    height = parser->model.section.height;
    neighbor = parser->model.section.neighbors.size();
    delete parser;
    return ret;
}

string AsynchronousModel::getError()
{
    return error_message;
}

bool cmp16(float *a, float *b)
{
    bool ret = true;
    for(int i=0;i<16;i++)
        if(a[i]!=b[i])ret=false;
    return ret;
}

void AsynchronousModel::run(CellSpace *pattern, int which, int iteration)
{
    cl_int err_code;
    err_code = clSetKernelArg(kernel, 0, sizeof(cl_mem), pattern->getSpace());
    err_code |= clSetKernelArg(intermediate_kernel, 0, sizeof(cl_mem), pattern->getSpace());
    Variant f = pattern->getValue("current_time");
    if(err_code){ cerr << "Nepodarilo sa nastavit argumenty kernela "; clGetError(err_code); }

    for(int i=0;i<iteration;i++)
    {
        err_code = clSetKernelArg(kernel, 1, sizeof(float), &f);
        f.f += timewindow;
        err_code |= clSetKernelArg(kernel, 2, sizeof(float), &f);
        if(err_code)cerr << "Nepodarilo sa nastavit argument " << endl;
        parent->run(kernel, pattern->getWidth(), pattern->getHeight());
        parent->run(intermediate_kernel, pattern->getWidth(), pattern->getHeight());
    }
    parent->finish();
    pattern->setValue("current_time", f);
}

/**
inicializuje data
@param pattern
*/
void AsynchronousModel::initialize(CellSpace *pattern, float *data)
{
    int w = pattern->getWidth();
    int h = pattern->getHeight();
    bool del = false;
    if(data==0)
    {
        del = true;
        data = new float[w*h];
        for(int i=0;i<h;i++)
        {
            for(int o=0;o<w;o++)
            {
                data[i*w+o] = initvalue;
            }
        }
    }

    uint32_t *seed = new uint32_t[w*h];
    CRandomMersenne mersene(time(0));
    for(int i=0;i<h;i++)
    {
        for(int o=0;o<w;o++)
        {
            seed[i*w+o] = mersene.BRandom();
        }
    }

    cl_int err_code;
    cl_mem buffer = parent->createBuffer(CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof(float)*pattern->getHeight()*pattern->getWidth(), data);
    cl_mem seedbuffer = parent->createBuffer(CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof(uint32_t)*w*h, seed);
    if(!buffer || !seedbuffer){ cerr << "Nepodarilo sa vytvorit buffer pre inicializaciu modelu" << endl; return; }
    err_code = clSetKernelArg(init_kernel, 0, sizeof(cl_mem), pattern->getSpace());
    err_code|= clSetKernelArg(init_kernel, 1, sizeof(cl_mem), &buffer);
    err_code|= clSetKernelArg(init_kernel, 2, sizeof(cl_mem), &seedbuffer);
    if(err_code){ cerr << "Nepodarilo sa nastavit argument pre inicializacny kernel"; clGetError(err_code); return; }

    parent->run(init_kernel, pattern->getWidth(), pattern->getHeight());
    parent->finish();

    clReleaseMemObject(buffer);
    clReleaseMemObject(seedbuffer);

    if(del)delete [] data;
    delete [] seed;
}

int AsynchronousModel::loadVAL(QString &input, CellSpace *cellspace)
{
    size_t s = cellspace->getHeight()*cellspace->getWidth();
    float *data = new float[s];
    for(size_t i=0;i<s;i++)data[i] = initvalue;
    int w = cellspace->getWidth();
    int h = cellspace->getHeight();
    QRegExp regexp("\\(\\s*([0-9]+)\\s*,\\s*([0-9]+)\\s*\\)\\s*=\\s*(-?([0-9]*\\.[0-9]+)|-?[0-9]+|\\?)");
    QStringList list = input.split('\n', QString::SkipEmptyParts);
    int x,y;
    float f;
    for(QStringList::iterator i = list.begin();i != list.end();i++)
    {
        int pos = regexp.indexIn(*i);
        if(pos > -1)
        {
            x = regexp.cap(1).toInt();
            y = regexp.cap(2).toInt();
            QString val = regexp.cap(3);
            if(val=="?")f = NAN;
            else f = val.toFloat();
            if(x<w && y<h)data[x+y*w] = f;
        }
    }

    initialize(cellspace, data);
    delete [] data;
    return 0;
}
