/**************************************************************************
*   Cellula, is cellular automata simulator accelerated on GPU.           *
*   Copyright (C) 2011  Dušan Poizl                                       *
*                                                                         *
*   This program is free software: you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation, either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
***************************************************************************/

#include "CDparser/CDparser.h"
#include "texteditor.h"
#include <QMdiSubWindow>
#include <QFileDialog>
#include <QMessageBox>
#include <QCloseEvent>
#include <QSettings>

Hightligter::Hightligter(QTextDocument *document) : QSyntaxHighlighter(document)
{
    HightlightingRule rule;
    //puncations {} :
    rule.pattern = QRegExp("[\\{\\}:<>=\\(\\)]");
    rule.format = QTextCharFormat();
    rule.format.setForeground(Qt::red);
    rules.append(rule);
    //number
    rule.pattern = QRegExp("[0123456789]");
    rule.format = QTextCharFormat();
    rule.format.setForeground(Qt::magenta);
    rules.append(rule);
    //keywords
    rule.pattern = QRegExp("\\b(rule|else|and|or|xor|imp|eqv|t|f|\\?)\\b");
    rule.format = QTextCharFormat();
    rule.format.setForeground(Qt::darkBlue);
    rule.format.setFontWeight(QFont::Bold);
    rules.append(rule);
    //section [top]
    rule.pattern = QRegExp("\\[[a-zA-Z_\\-0-9]*\\]");
    rule.format = QTextCharFormat();
    rule.format.setFontWeight(QFont::Bold);
    rule.format.setForeground(Qt::green);
    rules.append(rule);
    //comments %
    rule.pattern = QRegExp("[%#].*");
    rule.format = QTextCharFormat();
    rule.format.setFontItalic(true);
    rule.format.setForeground(Qt::gray);
    rules.append(rule);
}

void Hightligter::highlightBlock(const QString &text)
{
    for(QVector<HightlightingRule>::iterator i = rules.begin();i != rules.end();i++)
    {
         QRegExp expression((*i).pattern);
         int index = expression.indexIn(text);
         while (index >= 0)
         {
             int length = expression.matchedLength();
             setFormat(index, length, (*i).format);
             index = expression.indexIn(text, index + length);
         }
     }
     setCurrentBlockState(0);
}

int TextEditor::number = 1;

TextEditor::TextEditor(const QString &text, Model *model, QWidget *parent)
{
    document.setPlainText(text);
    highlighter = new Hightligter(&document);
    setDocument(&document);
    QFont font;
    font.setFamily("Courier");
    font.setFixedPitch(true);
    font.setPointSize(10);
    setFont(font);
    setAcceptRichText(false);
    setLineWrapMode(QTextEdit::NoWrap);
    modelName = trUtf8("Nový model %1").arg(getUntitledNumber());
    setMinimumSize(400, 300);
    fr = NULL;
    this->model = model;
    compiled = true;
    connect(&document, SIGNAL(contentsChanged()), this, SLOT(changed()));
}

TextEditor::~TextEditor()
{
    delete highlighter;
    delete fr;
    delete model;
}

void TextEditor::setName(const QString &name)
{
    modelName = name;
    setSubWindowTitle();
}

int TextEditor::compile(QTextEdit *log)
{
    string source = document.toPlainText().toStdString();
    int ret = model->compile(source);
    if(log && ret)
    {
        log->moveCursor(QTextCursor::End, QTextCursor::MoveAnchor);
        QTextCursor t = log->textCursor();
        t.insertBlock();
        string err = model->getError();
        t.insertText(err.c_str());
        log->ensureCursorVisible();
    }
    if(ret)QMessageBox::warning(this, trUtf8("Chyba pri parsovani"), trUtf8("Nepodarilo sa sparsovať definiciu modelu"));
    else
    {
        if(model->loadSource() && log)
        {
            QTextCursor t = log->textCursor();
            t.insertBlock();
            string err = model->getError();
            t.insertText(err.c_str());
            log->ensureCursorVisible();
        }
    }
    compiled = true;
    setSubWindowTitle();
    return 0;
}

void TextEditor::setSubWindowTitle()
{
    QMdiSubWindow *parent = dynamic_cast<QMdiSubWindow*>(parentWidget());
    if(parent)
    {
        QString flag;
        if(!compiled)flag+="#";
        if(document.isModified())flag+="*";
        parent->setWindowTitle(flag+" "+modelName);
    }
}

int TextEditor::load(const QString &file)
{
    if(fr != NULL)delete fr;
    fr = new QFile(file);
    if(!fr->exists())return fr->error();
    if(!fr->open(QIODevice::ReadOnly | QIODevice::Text))return fr->error();

    QByteArray text = fr->readAll();
    fr->close();
    document.setPlainText(text);
    document.setModified(false);
    modelName = file;
    setSubWindowTitle();
    return 0;
}

QString TextEditor::save(const QString &dir)
{
    if(fr == NULL)
    {
        QString filename = QFileDialog::getSaveFileName(this, trUtf8("Uložiť súbor"), dir+"/"+modelName, trUtf8("Definícia Cellula modelu alebo stavu (*)"));
        if(filename.isNull())return filename;
        fr = new QFile(filename);
    }

    if(!fr->open(QIODevice::ReadWrite | QIODevice::Text | QIODevice::Truncate))
    {
        QMessageBox::critical(this, trUtf8("Chyba súboru"), trUtf8("Nepodarilo sa otvoriť súbor %1 na zápis\n").arg(fr->fileName()).append(fr->errorString()));
        delete fr;
        fr = NULL;
        return QString();
    }
    if(fr->error() == QFile::NoError)
    {
        fr->write(document.toPlainText().toUtf8());
        fr->close();
    }
    document.setModified(false);
    setSubWindowTitle();
    return fr->fileName();
}

QString TextEditor::getFileName()
{
    return modelName;
}

void TextEditor::closeEvent(QCloseEvent *event)
{
    if(document.isModified())
    {
        enum QMessageBox::StandardButton button = QMessageBox::question(this, trUtf8("Uložiť?"), trUtf8("Model bol zmenený. Chcete uložiť zmeny?"), QMessageBox::Save | QMessageBox::Cancel | QMessageBox::Discard);
        switch(button)
        {
            case QMessageBox::Save:
            save("");
            break;
            case QMessageBox::Cancel:
            event->ignore();
            break;
            case QMessageBox::Discard:
            break;
            default: break;
        }
    }
}

void TextEditor::changed()
{
    compiled = false;
    setSubWindowTitle();
}
