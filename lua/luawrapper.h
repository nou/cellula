/**************************************************************************
*   Cellula, is cellular automata simulator accelerated on GPU.           *
*   Copyright (C) 2011  Dušan Poizl                                       *
*                                                                         *
*   This program is free software: you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation, either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
***************************************************************************/

#include "../texteditor.h"
#include "../glwidget.h"

class CellulaMainWindow;
class LuaCellSpace;

//tolua_begin
class LuaModel
{
    public:
    //tolua_end
    TextEditor *editor;
    CellulaMainWindow *mainw;
    //tolua_begin
    virtual ~LuaModel();
};

class LuaSynchronousModel : public LuaModel
{
    public:
    LuaCellSpace* loadRLE(string file);
};

class LuaAsynchronousModel : public LuaModel
{
    public:
    LuaCellSpace* loadVAL(string file);
    int getWidth();
    int getHeight();
};

class LuaCellSpace
{
    public:
    //tolua_end
    GLwidget *glwidget;
    //tolua_begin
    ~LuaCellSpace();
    void run(int iteration);
    int getWidth();
    int getHeight();
    void saveImage(string file);
};

class LuaMain
{
    //tolua_end
    private:
    CellulaMainWindow *mainw;
    public:
    LuaMain(CellulaMainWindow *main) : mainw(main){}
    //tolua_begin
    public:
    LuaSynchronousModel* createNewSynchModel(string definition);
    LuaAsynchronousModel* createNewAsynchModel(string definition);
    void print();
};

//tolua_end
