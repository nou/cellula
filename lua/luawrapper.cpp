/**************************************************************************
*   Cellula, is cellular automata simulator accelerated on GPU.           *
*   Copyright (C) 2011  Dušan Poizl                                       *
*                                                                         *
*   This program is free software: you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation, either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
***************************************************************************/

#include "luawrapper.h"
#include "../cellulamainwindow.h"
#include <QFile>
#include <QString>
#include <fstream>

LuaModel::~LuaModel()
{
    delete editor;
}

LuaCellSpace::~LuaCellSpace()
{
    delete glwidget;
}

void LuaCellSpace::run(int iteration)
{
    glwidget->setSteps(iteration);
    glwidget->runSimulation();
    glwidget->setSteps(1);
}

int LuaCellSpace::getWidth()
{
    return glwidget->getSpace()->getWidth();
}

int LuaCellSpace::getHeight()
{
    return glwidget->getSpace()->getHeight();
}

void LuaCellSpace::saveImage(string file)
{
    QString s(file.c_str());
    glwidget->saveImage(s);
}

LuaSynchronousModel* LuaMain::createNewSynchModel(string definition)
{
    LuaSynchronousModel *model;
    model = new LuaSynchronousModel();
    model->mainw = mainw;
    model->editor = mainw->createNewModel(1, definition);
    return model;
}

LuaCellSpace* LuaSynchronousModel::loadRLE(string file)
{
    SynchronousModel* m = dynamic_cast<SynchronousModel*>(editor->getModel());
    if(m)
    {
        ifstream fr(file.c_str(), ios::binary|ios::in);
        if(!fr.is_open())return 0;
        SynchronousModel::CellType *data;
        size_t w,h;
        int ret = m->loadRLE(fr, &w, &h, &data);
        if(ret==0)
        {
            CellSpace *space = new CellSpace(m, w, h);
            space->loadData(w*h*sizeof(SynchronousModel::CellType)*2, data);
            delete [] data;
            LuaCellSpace *new_space = new LuaCellSpace;
            new_space->glwidget = mainw->addCellSpace(space);
            return new_space;
        }
    }
    return 0;
}

LuaCellSpace* LuaAsynchronousModel::loadVAL(string file)
{
    AsynchronousModel* m = dynamic_cast<AsynchronousModel*>(editor->getModel());
    if(m)
    {
        QFile fr(file.c_str());
        if(!fr.open(QIODevice::ReadOnly))return 0;
        QString val = fr.readAll();
        CellSpace *space = new CellSpace(m, m->getWidth(), m->getHeight());
        m->loadVAL(val, space);
        LuaCellSpace *new_space = new LuaCellSpace;
        new_space->glwidget = mainw->addCellSpace(space);
        return new_space;
    }
    return 0;
}

int LuaAsynchronousModel::getWidth()
{
    AsynchronousModel* m = dynamic_cast<AsynchronousModel*>(editor->getModel());
    if(m)return m->getWidth();
    else return 0;
}

int LuaAsynchronousModel::getHeight()
{
    AsynchronousModel* m = dynamic_cast<AsynchronousModel*>(editor->getModel());
    if(m)return m->getHeight();
    else return 0;
}


LuaAsynchronousModel* LuaMain::createNewAsynchModel(string definition)
{
    LuaAsynchronousModel *model;
    model = new LuaAsynchronousModel();
    model->mainw = mainw;
    model->editor = mainw->createNewModel(2, definition);
    return model;
}

void LuaMain::print()
{
    cout << "LuaMain" << endl;
}
