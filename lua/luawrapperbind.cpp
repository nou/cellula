/*
** Lua binding: luawrapper
** Generated automatically by tolua++-1.0.93 on Tue May 10 12:44:26 2011.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua++.h"

/* Exported function */
TOLUA_API int  tolua_luawrapper_open (lua_State* tolua_S);

#include "luawrapper.h"

/* function to release collected object via destructor */
#ifdef __cplusplus

static int tolua_collect_LuaCellSpace (lua_State* tolua_S)
{
 LuaCellSpace* self = (LuaCellSpace*) tolua_tousertype(tolua_S,1,0);
	Mtolua_delete(self);
	return 0;
}

static int tolua_collect_LuaModel (lua_State* tolua_S)
{
 LuaModel* self = (LuaModel*) tolua_tousertype(tolua_S,1,0);
	Mtolua_delete(self);
	return 0;
}
#endif


/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"LuaCellSpace");
 tolua_usertype(tolua_S,"LuaSynchronousModel");
 tolua_usertype(tolua_S,"LuaMain");
 tolua_usertype(tolua_S,"LuaAsynchronousModel");
 tolua_usertype(tolua_S,"LuaModel");
}

/* method: delete of class  LuaModel */
#ifndef TOLUA_DISABLE_tolua_luawrapper_LuaModel_delete00
static int tolua_luawrapper_LuaModel_delete00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"LuaModel",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  LuaModel* self = (LuaModel*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'delete'", NULL);
#endif
  Mtolua_delete(self);
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'delete'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: loadRLE of class  LuaSynchronousModel */
#ifndef TOLUA_DISABLE_tolua_luawrapper_LuaSynchronousModel_loadRLE00
static int tolua_luawrapper_LuaSynchronousModel_loadRLE00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"LuaSynchronousModel",0,&tolua_err) ||
     !tolua_iscppstring(tolua_S,2,0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  LuaSynchronousModel* self = (LuaSynchronousModel*)  tolua_tousertype(tolua_S,1,0);
  string file = ((string)  tolua_tocppstring(tolua_S,2,0));
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'loadRLE'", NULL);
#endif
  {
   LuaCellSpace* tolua_ret = (LuaCellSpace*)  self->loadRLE(file);
    tolua_pushusertype(tolua_S,(void*)tolua_ret,"LuaCellSpace");
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'loadRLE'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: loadVAL of class  LuaAsynchronousModel */
#ifndef TOLUA_DISABLE_tolua_luawrapper_LuaAsynchronousModel_loadVAL00
static int tolua_luawrapper_LuaAsynchronousModel_loadVAL00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"LuaAsynchronousModel",0,&tolua_err) ||
     !tolua_iscppstring(tolua_S,2,0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  LuaAsynchronousModel* self = (LuaAsynchronousModel*)  tolua_tousertype(tolua_S,1,0);
  string file = ((string)  tolua_tocppstring(tolua_S,2,0));
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'loadVAL'", NULL);
#endif
  {
   LuaCellSpace* tolua_ret = (LuaCellSpace*)  self->loadVAL(file);
    tolua_pushusertype(tolua_S,(void*)tolua_ret,"LuaCellSpace");
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'loadVAL'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: getWidth of class  LuaAsynchronousModel */
#ifndef TOLUA_DISABLE_tolua_luawrapper_LuaAsynchronousModel_getWidth00
static int tolua_luawrapper_LuaAsynchronousModel_getWidth00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"LuaAsynchronousModel",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  LuaAsynchronousModel* self = (LuaAsynchronousModel*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getWidth'", NULL);
#endif
  {
   int tolua_ret = (int)  self->getWidth();
   tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getWidth'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: getHeight of class  LuaAsynchronousModel */
#ifndef TOLUA_DISABLE_tolua_luawrapper_LuaAsynchronousModel_getHeight00
static int tolua_luawrapper_LuaAsynchronousModel_getHeight00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"LuaAsynchronousModel",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  LuaAsynchronousModel* self = (LuaAsynchronousModel*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getHeight'", NULL);
#endif
  {
   int tolua_ret = (int)  self->getHeight();
   tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getHeight'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: delete of class  LuaCellSpace */
#ifndef TOLUA_DISABLE_tolua_luawrapper_LuaCellSpace_delete00
static int tolua_luawrapper_LuaCellSpace_delete00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"LuaCellSpace",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  LuaCellSpace* self = (LuaCellSpace*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'delete'", NULL);
#endif
  Mtolua_delete(self);
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'delete'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: run of class  LuaCellSpace */
#ifndef TOLUA_DISABLE_tolua_luawrapper_LuaCellSpace_run00
static int tolua_luawrapper_LuaCellSpace_run00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"LuaCellSpace",0,&tolua_err) ||
     !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  LuaCellSpace* self = (LuaCellSpace*)  tolua_tousertype(tolua_S,1,0);
  int iteration = ((int)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'run'", NULL);
#endif
  {
   self->run(iteration);
  }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'run'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: getWidth of class  LuaCellSpace */
#ifndef TOLUA_DISABLE_tolua_luawrapper_LuaCellSpace_getWidth00
static int tolua_luawrapper_LuaCellSpace_getWidth00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"LuaCellSpace",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  LuaCellSpace* self = (LuaCellSpace*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getWidth'", NULL);
#endif
  {
   int tolua_ret = (int)  self->getWidth();
   tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getWidth'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: getHeight of class  LuaCellSpace */
#ifndef TOLUA_DISABLE_tolua_luawrapper_LuaCellSpace_getHeight00
static int tolua_luawrapper_LuaCellSpace_getHeight00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"LuaCellSpace",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  LuaCellSpace* self = (LuaCellSpace*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getHeight'", NULL);
#endif
  {
   int tolua_ret = (int)  self->getHeight();
   tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getHeight'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: saveImage of class  LuaCellSpace */
#ifndef TOLUA_DISABLE_tolua_luawrapper_LuaCellSpace_saveImage00
static int tolua_luawrapper_LuaCellSpace_saveImage00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"LuaCellSpace",0,&tolua_err) ||
     !tolua_iscppstring(tolua_S,2,0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  LuaCellSpace* self = (LuaCellSpace*)  tolua_tousertype(tolua_S,1,0);
  string file = ((string)  tolua_tocppstring(tolua_S,2,0));
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'saveImage'", NULL);
#endif
  {
   self->saveImage(file);
  }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'saveImage'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: createNewSynchModel of class  LuaMain */
#ifndef TOLUA_DISABLE_tolua_luawrapper_LuaMain_createNewSynchModel00
static int tolua_luawrapper_LuaMain_createNewSynchModel00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"LuaMain",0,&tolua_err) ||
     !tolua_iscppstring(tolua_S,2,0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  LuaMain* self = (LuaMain*)  tolua_tousertype(tolua_S,1,0);
  string definition = ((string)  tolua_tocppstring(tolua_S,2,0));
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'createNewSynchModel'", NULL);
#endif
  {
   LuaSynchronousModel* tolua_ret = (LuaSynchronousModel*)  self->createNewSynchModel(definition);
    tolua_pushusertype(tolua_S,(void*)tolua_ret,"LuaSynchronousModel");
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'createNewSynchModel'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: createNewAsynchModel of class  LuaMain */
#ifndef TOLUA_DISABLE_tolua_luawrapper_LuaMain_createNewAsynchModel00
static int tolua_luawrapper_LuaMain_createNewAsynchModel00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"LuaMain",0,&tolua_err) ||
     !tolua_iscppstring(tolua_S,2,0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  LuaMain* self = (LuaMain*)  tolua_tousertype(tolua_S,1,0);
  string definition = ((string)  tolua_tocppstring(tolua_S,2,0));
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'createNewAsynchModel'", NULL);
#endif
  {
   LuaAsynchronousModel* tolua_ret = (LuaAsynchronousModel*)  self->createNewAsynchModel(definition);
    tolua_pushusertype(tolua_S,(void*)tolua_ret,"LuaAsynchronousModel");
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'createNewAsynchModel'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: print of class  LuaMain */
#ifndef TOLUA_DISABLE_tolua_luawrapper_LuaMain_print00
static int tolua_luawrapper_LuaMain_print00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"LuaMain",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  LuaMain* self = (LuaMain*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'print'", NULL);
#endif
  {
   self->print();
  }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'print'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* Open function */
TOLUA_API int tolua_luawrapper_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
  #ifdef __cplusplus
  tolua_cclass(tolua_S,"LuaModel","LuaModel","",tolua_collect_LuaModel);
  #else
  tolua_cclass(tolua_S,"LuaModel","LuaModel","",NULL);
  #endif
  tolua_beginmodule(tolua_S,"LuaModel");
   tolua_function(tolua_S,"delete",tolua_luawrapper_LuaModel_delete00);
  tolua_endmodule(tolua_S);
  tolua_cclass(tolua_S,"LuaSynchronousModel","LuaSynchronousModel","LuaModel",NULL);
  tolua_beginmodule(tolua_S,"LuaSynchronousModel");
   tolua_function(tolua_S,"loadRLE",tolua_luawrapper_LuaSynchronousModel_loadRLE00);
  tolua_endmodule(tolua_S);
  tolua_cclass(tolua_S,"LuaAsynchronousModel","LuaAsynchronousModel","LuaModel",NULL);
  tolua_beginmodule(tolua_S,"LuaAsynchronousModel");
   tolua_function(tolua_S,"loadVAL",tolua_luawrapper_LuaAsynchronousModel_loadVAL00);
   tolua_function(tolua_S,"getWidth",tolua_luawrapper_LuaAsynchronousModel_getWidth00);
   tolua_function(tolua_S,"getHeight",tolua_luawrapper_LuaAsynchronousModel_getHeight00);
  tolua_endmodule(tolua_S);
  #ifdef __cplusplus
  tolua_cclass(tolua_S,"LuaCellSpace","LuaCellSpace","",tolua_collect_LuaCellSpace);
  #else
  tolua_cclass(tolua_S,"LuaCellSpace","LuaCellSpace","",NULL);
  #endif
  tolua_beginmodule(tolua_S,"LuaCellSpace");
   tolua_function(tolua_S,"delete",tolua_luawrapper_LuaCellSpace_delete00);
   tolua_function(tolua_S,"run",tolua_luawrapper_LuaCellSpace_run00);
   tolua_function(tolua_S,"getWidth",tolua_luawrapper_LuaCellSpace_getWidth00);
   tolua_function(tolua_S,"getHeight",tolua_luawrapper_LuaCellSpace_getHeight00);
   tolua_function(tolua_S,"saveImage",tolua_luawrapper_LuaCellSpace_saveImage00);
  tolua_endmodule(tolua_S);
  tolua_cclass(tolua_S,"LuaMain","LuaMain","",NULL);
  tolua_beginmodule(tolua_S,"LuaMain");
   tolua_function(tolua_S,"createNewSynchModel",tolua_luawrapper_LuaMain_createNewSynchModel00);
   tolua_function(tolua_S,"createNewAsynchModel",tolua_luawrapper_LuaMain_createNewAsynchModel00);
   tolua_function(tolua_S,"print",tolua_luawrapper_LuaMain_print00);
  tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}


#if defined(LUA_VERSION_NUM) && LUA_VERSION_NUM >= 501
 TOLUA_API int luaopen_luawrapper (lua_State* tolua_S) {
 return tolua_luawrapper_open(tolua_S);
};
#endif

