/************************************************************************** 
*   Cellula, is cellular automata simulator accelerated on GPU.           *
*   Copyright (C) 2011  Dušan Poizl                                       *
*                                                                         *
*   This program is free software: you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation, either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
***************************************************************************/

#ifndef _CLUTILITY_H_NOU_
#define _CLUTILITY_H_NOU_

#include <iostream>
#include <string>
#include <CL/cl.h>

using namespace std;

void getDeviceInfo(cl_device_id device);
void clGetError(cl_int err_code);
string getPlatformInfo(cl_platform_id platform_id);
string getDeviceInfoShort(cl_device_id device);
string getBuildLog(cl_program program);
size_t getLocalWorkSize(cl_device_id device);

#endif // _CLUTILITY_H_NOU_
