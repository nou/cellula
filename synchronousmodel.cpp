/**************************************************************************
*   Cellula, is cellular automata simulator accelerated on GPU.           *
*   Copyright (C) 2011  Dušan Poizl                                       *
*                                                                         *
*   This program is free software: you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation, either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
***************************************************************************/

#include "model.h"
#include "clutility.h"
#include "machine.h"
#include <memory.h>
#include <sstream>
#include "treescanner.h"

SynchronousModel::SynchronousModel(Machine *parent, string model_definition) : Model(parent)
{
    compile(model_definition);
    loadSource();
}

int SynchronousModel::loadSource()
{
    /*"__constant int rule_tree[] = {"
    "0,0,0,0,0,1,0,2,1,3,1,1,2,5,3,6,4,7,5,0,6,9,7,10,8,11,9,1,10,13,11,14,12,15,1,1,13,17,14,"
    "18,15,19,16,20,17,17,18,22,19,23,20,24,21,25,22,22,23,27,24,28,25,29,26,30,};\n"*/
    const char *colors = "__constant uint4 colors[] = { (uint4)(48,48,48,0), (uint4)(255,0,0,0), (uint4)(255,125,0,0), (uint4)(255,150,25,0), (uint4)(255,175,50,0),"
        "(uint4)(255,200,75,0), (uint4)(255,225,100,0), (uint4)(255,250,125,0), (uint4)(251,255,0,0), (uint4)(89,89,255,0),"
        "(uint4)(106,106,255,0), (uint4)(122,122,255,0), (uint4)(139,139,255,0), (uint4)(27,176,27,0), (uint4)(36,200,36,0),"
        "(uint4)(73,255,73,0), (uint4)(106,255,106,0), (uint4)(255,36,36,0), (uint4)(255,56,56,0), (uint4)(255,73,73,0),"
        "(uint4)(255,89,89,0), (uint4)(185,56,255,0), (uint4)(191,73,255,0), (uint4)(197,89,255,0), (uint4)(203,106,255,0),"
        "(uint4)(0,255,128,0), (uint4)(255,128,64,0), (uint4)(255,255,128,0), (uint4)(33,215,215,0) };\n #include \"synchr.cl\"";
    const char *colors2 = "__constant uint4 colors[] = { (uint4)(0,0,0,0), (uint4)(0,255,0,0), (uint4)(0,0,255,0),"
        "(uint4)(255,255,0,0),(uint4)(255,0,255,0),(uint4)(0,255,255,0),(uint4)(255,255,125,0),(uint4)(255,125,255,0),"
        "(uint4)(125,255,255,0) }; \n#include \"synchr.cl\"";

    const char *colors3 = "__constant uint4 colors[] = { (uint4)(48,48,48,0),(uint4)(0,128,255,0),(uint4)(255,255,255,0),(uint4)(255,128,0,0) }; \n#include \"synchr.cl\"";
    string source = nodes+colors3;//"__constant uint4 colors[] = { (uint4)(0, 0, 0, 0), (uint4)(255, 255, 255, 255) };\n #include \"synchr.cl\" ";
    stringstream options;
    options << getKernelInclude();
    if(num_neighbors==8)options << " -D MOORE";
    else options << " -D NEUMANN";
    options << " -D NUM_STATES="<<num_states;
    if(build(source, options.str()))
    {
        error_message += getBuildLog(program);
        return -1;
    }
    return 0;
}

void SynchronousModel::run(CellSpace *pattern, int which, int iteration)
{
    cl_int err_code;
    err_code = clSetKernelArg(kernel, 0, sizeof(cl_mem), pattern->getSpace());
    err_code |= clSetKernelArg(kernel, 1, sizeof(int), &which);
    if(err_code){ cerr << "Nepodarilo sa nastavit argumenty kernela "; clGetError(err_code); }

    for(int i=0;i<iteration;i++)
    {
        err_code = clSetKernelArg(kernel, 1, sizeof(int), &which);
        if(err_code)cerr << "Nepodarilo sa nastavit argument " << endl;
        parent->run(kernel, pattern->getWidth(), pattern->getHeight());
        which = (which+1)&1;
    }
    parent->finish();
}

void SynchronousModel::renderToTexture(CellSpace *pattern, int which, cl_mem tex)
{
    cl_int err_code;
    err_code = clSetKernelArg(render_kernel, 0, sizeof(cl_mem), pattern->getSpace());
    err_code |= clSetKernelArg(render_kernel, 1, sizeof(int), &which);
    err_code |= clSetKernelArg(render_kernel, 2, sizeof(cl_mem), &tex);
    if(err_code){ cerr << "Nepodarilo sa nastavit argumenty "; clGetError(err_code); }
    parent->run(render_kernel, pattern->getWidth(), pattern->getHeight());
}

int SynchronousModel::compile(string model_definition)
{
    error_message.clear();
    num_neighbors = num_nodes = num_states = 0;
    stringstream ss(model_definition);
    stringstream serr;
    stringstream n;
    TreeScanner scanner(&ss, &serr);
    TreeScanner::TreeToken token;
    int val;
    int state = 0;// 1-neighbors, 2-states, 4-nodes
    int loaded = 0;
    token = scanner.lex(&val);
    while(token != TreeScanner::END && loaded!=7)
    {
        switch(token)
        {
            case TreeScanner::NUMBER:
                switch(state)
                {
                    case 0: n << val << ", "; break;
                    case 1: num_neighbors = val; state = 0; loaded|=1; break;
                    case 2: num_states = val; state = 0; loaded|=2; break;
                    case 4: num_nodes = val; state = 0; loaded|=4; break;
                }
            break;
            case TreeScanner::NEIGHBORS: state = 1; break;
            case TreeScanner::STATES: state = 2; break;
            case TreeScanner::NODES: state = 4; break;
            case TreeScanner::END: if(loaded!=7){ error_message = "Incomplete model definition"; return -1; } break;
        }
        token = scanner.lex(&val);
    }
    n << "__constant int rule_tree[] = {";
    for(int i=0;i<num_nodes;i++)
    {
        if(i)token = scanner.lex(&val);//preskakujeme hlbku
        for(int o=0;o<num_states;o++)
        {
            token = scanner.lex(&val);
            if(token!=TreeScanner::NUMBER){ error_message = "Incomplete model definition"; return -1; }
            n << val << ", ";
        }
        n << "  ";
    }
    n << "};\n";
    nodes = n.str();
    //cout << num_neighbors << " " << num_nodes << " " << num_states << " " << nodes << endl;
    return 0;
}

string SynchronousModel::getError()
{
    return error_message;
}

int SynchronousModel::loadRLE(istream &fr, size_t *w, size_t *h, CellType **data)
{
    *data = 0;
    const unsigned int big = 256*256*256*127;
    char eqv1,eqv2,colon,chary;
    int x,y;
    int i=0,o=0;
    int repeat=1;
    int state=-1;
    int z;
    char znak;
    do
    {
        fr.get(znak);
        if(znak=='#')fr.ignore(big, '\n');
        if(fr.fail())return -1;
    }while(znak!='x');
    fr >> eqv1 >> x >> colon >> chary >> eqv2 >> y;//x=1,y=1

    if(x&0x7)x = (y&(~0x7))+8;
    if(y&0x7)y = (y&(~0x7))+8;

    if(fr.fail())return -1;
    *w = x;
    *h = y;
    fr.ignore(big, '\n');//ignorovat do konca riadku teda rule= B3/S23


    CellType *tmp = new CellType[x*y*2];
    memset(tmp, 0, x*y*2*sizeof(CellType));
    while(!fr.fail())
    {
        z = fr.peek();
        if(z>='0' && z <= '9')
        {
            fr >> repeat;
        }
        else
        {
            fr.get(znak);
            if(znak=='#')fr.ignore(big, '\n');//komentar
            else if(znak=='$')
            {
                o+=repeat;
                i=0;
                repeat=1;
            }
            else if(znak=='!')break;
            else if(znak=='b' || znak=='.')state = 0;
            else if(znak=='o')state = 1;
            else if(znak>='A' && znak<='X')
            {
                if(state<-1)state = (-state) + (znak-'A');
                else state = znak-'A'+1;
            }
            else if(znak>='p' && znak<='y')state = -25-(znak-'p');
        }
        if(state>=0)//zapise stav iba ak je nastaveny stav
        {
            for(int p=0;p<repeat;p++,i++)
                if(i<x)tmp[2*(o*x+i)]=state;
            state = -1;
            repeat=1;
        }
    }
    *data = tmp;
    return 0;
}
