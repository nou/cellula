/**************************************************************************
*   Cellula, is cellular automata simulator accelerated on GPU.           *
*   Copyright (C) 2011  Dušan Poizl                                       *
*                                                                         *
*   This program is free software: you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation, either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
***************************************************************************/

#include "cellspace.h"
#include "model.h"
#include "machine.h"
#include "string.h"
#include <stdlib.h>
/**
Vytvory stavovy priestor modelu
@param parent rodic ku ktoremu je dany stavovy priestor priradeny
@param w sirka v pocte buniek
@param h vyska v pocte buniek
*/
CellSpace::CellSpace(Model *parent, int w, int h)
{
    this->parent = parent;
    parent->addChild(this);
    which = 0;
    width = w;
    height = h;
    int size = parent->getCellSize()*w*h;
    unsigned char *data = new unsigned char [size];
    memset(data, 0, size);
    space = parent->getParent()->createBuffer(CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, size, data);
    delete [] data;
    tex = NULL;
    gl_texture = 0;
}

CellSpace::~CellSpace()
{
    parent->removeChild(this);
    clReleaseMemObject(space);
    clReleaseMemObject(tex);
}

/**
Spusti simulaciu daneho pristoru
@param iteration pocet iteracii ktore sa maju vykonat
*/
void CellSpace::run(int iteration)
{
    parent->run(this, which, iteration);
    which=(which+iteration)&1;//binarna hodnota
    /*int *bit = new int[width*height*2];
    parent->getParent()->copy(space, bit, 4*width*height*2, true);
    for(int i=0;i<height;i++)
    {
        for(int o=0;o<width;o++)
        {
            cout << "(" << bit[(i*width+o)*2] << "," << bit[(i*width+o)*2+1] << ") ";
        }
        cout << endl;
    }
    cout << endl;*/
}
/**
Vykresli do textury aktualny stav priestoru
@return pointer na pole pixmapy s formatom RGBA s rozmermy width height ktore vracia
@sa CellSpace::getWidth CellSpace::getHeight
*/
void CellSpace::renderToTexture()
{
    if(gl_texture)
    {
        if(parent->getParent()->GLinteroprability())
        {
            parent->getParent()->acquireGLObject(tex);
            parent->renderToTexture(this, which, tex);
            parent->getParent()->releaseGLObject(tex);
            parent->getParent()->finish(0);
        }
        else
        {
            parent->renderToTexture(this, which, tex);
            parent->getParent()->finish(0);
            unsigned char *data = new unsigned char[width*height*4];
            size_t origin[] = {0,0,0};
            size_t region[] = {width, height,1};
            parent->getParent()->copyImage(tex, data, origin, region);
            glBindTexture(GL_TEXTURE_2D, gl_texture);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
            delete [] data;
        }
    }
};

void CellSpace::setValue(string key, Variant v)
{
    values[key] = v;
}

Variant CellSpace::getValue(string key)
{
    return values[key];
}

void CellSpace::setGLTexture(GLuint texture)
{
    gl_texture = texture;
    if(parent->getParent()->GLinteroprability())tex = parent->getParent()->createFromGLTexture(CL_MEM_WRITE_ONLY, texture);
    else tex = parent->getParent()->createTexture(CL_MEM_WRITE_ONLY, width, height);
}

void CellSpace::loadData(size_t size, void *data)
{
    size_t insidesize = width*height*parent->getCellSize();
    size_t s = size>insidesize ? insidesize : size;
    parent->getParent()->copy(data, space, s);
}
