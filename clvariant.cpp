/************************************************************************** 
*   Cellula, is cellular automata simulator accelerated on GPU.           *
*   Copyright (C) 2011  Dušan Poizl                                       *
*                                                                         *
*   This program is free software: you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation, either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
***************************************************************************/

#include "clvariant.h"

CLVariant::CLVariant(cl_int i)
{
    data.int1 = i;
    type = CL_FLOAT1;
}

CLVariant::CLVariant(cl_int2 i)
{
    data.int2 = i;
}

CLVariant::CLVariant(cl_int4 i)
{
    data.int4 = i;
}

CLVariant::CLVariant(cl_int8 i)
{
    data.int8 = i;
}

CLVariant::CLVariant(cl_int16 i)
{
    data.int16 = i;
}

CLVariant::CLVariant(cl_float i)
{
    data.float1 = i;
}

CLVariant::CLVariant(cl_float2 i)
{
    data.float2 = i;
}

CLVariant::CLVariant(cl_float4 i)
{
    data.float4 = i;
}

CLVariant::CLVariant(cl_float8 i)
{
    data.float8 = i;
}

CLVariant::CLVariant(cl_float16 i)
{
    data.float16 = i;
}

CLVariant::CLVariant(cl_mem m)
{
    data.mem = m;
}


