/************************************************************************** 
*   Cellula, is cellular automata simulator accelerated on GPU.           *
*   Copyright (C) 2011  Dušan Poizl                                       *
*                                                                         *
*   This program is free software: you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation, either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
***************************************************************************/

#ifndef _CELLSPACE_H_NOU_
#define _CELLSPACE_H_NOU_

#include <GL/gl.h>
#include <CL/cl.h>
#include <iostream>
#include <map>
#include <string>

using namespace std;

class Model;

typedef union
{
    float f;
    int i;
    unsigned int ui;
    long long l;
    unsigned long long ul;
    double d;
}Variant;

class CellSpace
{
    private:
    cl_mem space;
    cl_mem tex;
    GLuint gl_texture;
    int width,height;
    int which;//executiva bude prebiehat na zaklade striedania sa bufferov
    Model *parent;
    map<string, Variant> values;
    public:
    CellSpace(Model *parent, int w, int h);
    ~CellSpace();
    void run(int iteration = 1);
    Model* getParent(){ return parent; }
    cl_mem* getSpace(){ return &space; }
    int getWidth(){ return width; }
    int getHeight(){ return height; }
    void setValue(string key, Variant v);
    Variant getValue(string key);
    void renderToTexture();
    void setGLTexture(GLuint texture);
    /**
    nahraje do buffera raw data ktore su ulozene v buffery-
    @param size velkost dat v data.
    @param data pointer na data. ocakava ze tam bude width*heigh*cellsize. prekopruje sa minimum(size , w*h*cellsize)
    */
    void loadData(size_t size, void *data);
    protected:
};

#endif // _CELLSPACE_H_NOU_
