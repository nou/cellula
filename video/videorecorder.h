#ifndef _VIDEORECORDER_H_NOU_
#define _VIDEORECORDER_H_NOU_

#include <vector>
#include <string>
extern "C"
{
    #define __STDC_CONSTANT_MACROS
    #include <libavcodec/avcodec.h>
    #include <libavformat/avformat.h>
    #include <libswscale/swscale.h>
}

using namespace std;

namespace Video
{
    class Codec;
    typedef vector<Codec> CodecV;

    class Codec
    {
        AVCodec *codec;
        public:
        Codec(AVCodec *c){ codec = c; }
        AVCodec* getCodec()const { return codec; }
        string getName()const { return codec->name; }
        string getLongName()const { return codec->long_name; }
        vector<PixelFormat> getPixFmts()const ;
        PixelFormat getPixFmt()const { return codec->pix_fmts != NULL ? codec->pix_fmts[0] : PIX_FMT_NONE; }
        static CodecV getCodecs();
        static Codec findCodec(const char *name);
    };

    class Frame
    {
        AVFrame *frame;
        uint8_t *buf;
        int width,height;
        PixelFormat format;
        public:
        Frame(int w, int h, PixelFormat f);
        Frame(Frame &d);
        ~Frame();
        int getWidth(){ return width; }
        int getHeight(){ return height; }
        uint8_t* getBuf(){ return buf; }
        AVFrame* getAVFrame(){ return frame; }
        PixelFormat getFormat(){ return format; }
        bool operator==(Frame &d);
        bool operator!=(Frame &d){ return !(*this==d); }
    };

    class Recorder
    {
        int ret;
        AVFormatContext *oc;
        AVStream *video_st;
        SwsContext *img_convert_ctx;
        int width,height;
        int video_outbuf_size;
        uint8_t *video_outbuf;
        Frame *frame,*tmp_frame;
        SwsContext *sws_context;
        PixelFormat format;
        void openVideoCodec(const Codec &codec);
        public:
        Recorder(const char *filename, int w, int h, Codec codec);
        ~Recorder();
        int openFile();
        int closeFile();
        int writeFrame(Frame &f);
        void setInputFormat(Frame &f);
        static void initFFMPEG();
        int getError(){ return ret; }
    };
}

#endif // _VIDEORECORDER_H_NOU_
