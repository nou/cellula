#include "videorecorder.h"
#include <iostream>

namespace Video
{
    vector<PixelFormat> Codec::getPixFmts()const
    {
        vector<PixelFormat> formats;
        if(codec->pix_fmts == NULL)return formats;
        for(int i=0;codec->pix_fmts[i] != PIX_FMT_NONE; i++)
            formats.push_back(codec->pix_fmts[i]);
        return formats;
    }

    CodecV Codec::getCodecs()
    {
        vector<Codec> codecs;
        AVCodec *c = NULL;
        while((c = av_codec_next(c)) != NULL)
        {
            if(c->pix_fmts != NULL && c->encode)//pridame iba encodery s nastavenym vstupnym formatom
                codecs.push_back(Codec(c));
        }
        return codecs;
    }

    Codec Codec::findCodec(const char *name)
    {
        return Codec(avcodec_find_encoder_by_name(name));
    }

    Frame::Frame(int w, int h, PixelFormat f)
    {
        width = w;
        height = h;
        format = f;
        frame = avcodec_alloc_frame();
        buf = (uint8_t*)av_malloc(avpicture_get_size(format, w, h));
        avpicture_fill((AVPicture*)frame, buf, format, w, h);
    }

    Frame::Frame(Frame &d)
    {
        width = d.width;
        height = d.height;
        format = d.format;
        buf = (uint8_t*)av_malloc(avpicture_get_size(format, width, height));
        memcpy(buf, d.buf, avpicture_get_size(format, width, height));
        frame = avcodec_alloc_frame();
        avpicture_fill((AVPicture*)frame, buf, format, width, height);
    }

    Frame::~Frame()
    {
        av_free(buf);
        av_free(frame);
    }

    bool Frame::operator==(Frame &d)
    {
        return format == d.format && width == d.width && height == d.height;
    }

    Recorder::Recorder(const char *filename, int w, int h, Codec codec)
    {
        width = w;
        height = h;
        img_convert_ctx = NULL;

        oc = avformat_alloc_context();//alokujem vystupny kontext
        oc->oformat = guess_format("matroska", NULL, NULL);//pouzijem MKV
        snprintf(oc->filename, sizeof(oc->filename), "%s", filename);

        cout << oc->oformat->name << " " << oc->oformat->long_name << endl;

        openVideoCodec(codec);
        av_set_parameters(oc, NULL);
        dump_format(oc, 0, filename, 1);

        format = codec.getPixFmt();
        frame = new Frame(width, height, format);// frame z ktoreho sa bude enkodovat

        video_outbuf_size = 1<<21;//2MiB
        video_outbuf = (uint8_t*)av_malloc(video_outbuf_size);

        if (!(oc->oformat->flags & AVFMT_NOFILE))
        {
            if (url_fopen(&oc->pb, filename, URL_WRONLY) < 0)
            {
                cerr << "Could not open " << filename << endl;
            }
            else av_write_header(oc);
        }
    }

    Recorder::~Recorder()
    {
        av_write_trailer(oc);
        avcodec_close(video_st->codec);
        url_fclose(oc->pb);
        for(unsigned int i=0; i < oc->nb_streams; i++)
        {
            av_freep(&oc->streams[i]->codec);
            av_freep(&oc->streams[i]);
        }
        av_free(oc);
        delete frame;
        av_free(video_outbuf);
        if(img_convert_ctx)sws_freeContext(img_convert_ctx);

    }

    void Recorder::initFFMPEG()
    {
        avcodec_init();
        av_register_all();
    }

    void Recorder::openVideoCodec(const Codec &codec)
    {
        AVCodecContext *c;
        AVCodec *av_codec;

        video_st = av_new_stream(oc, 0);
        c = video_st->codec;
        c->codec_id = codec.getCodec()->id;
        c->codec_type = CODEC_TYPE_VIDEO;
        c->bit_rate = 5000000;//bitrate
        c->width = width;
        c->height = height;
        c->time_base.den = 25;//framerate
        c->time_base.num = 1;
        c->gop_size = 12;
        c->pix_fmt = codec.getPixFmt();

        if(oc->oformat->flags & AVFMT_GLOBALHEADER)
            c->flags |= CODEC_FLAG_GLOBAL_HEADER;

        av_codec = avcodec_find_encoder(c->codec_id);
        avcodec_open(video_st->codec, av_codec);
    }

    int Recorder::writeFrame(Frame &f)
    {
        AVCodecContext *c = video_st->codec;
        AVPacket pkt;
        AVFrame *tmp_frame = f.getAVFrame();// mozno budeme enkodovat priamo z parametra

        if(f != *frame)//test ci maju rozdielny format
        {
            sws_scale(img_convert_ctx, f.getAVFrame()->data, f.getAVFrame()->linesize, 0, f.getHeight(), frame->getAVFrame()->data, frame->getAVFrame()->linesize);
            tmp_frame = frame->getAVFrame();// takze ideme enkodovat z konvertovaneho frame
        }

        int out_size = avcodec_encode_video(c, video_outbuf, video_outbuf_size, tmp_frame);

        if(out_size > 0)
        {
            av_init_packet(&pkt);
            if (c->coded_frame->pts != AV_NOPTS_VALUE)
                pkt.pts = av_rescale_q(c->coded_frame->pts, c->time_base, video_st->time_base);

            if(c->coded_frame->key_frame)
                pkt.flags |= PKT_FLAG_KEY;

            pkt.stream_index= video_st->index;
            pkt.data = video_outbuf;
            pkt.size = out_size;

            av_write_frame(oc, &pkt);
        }
        return 0;
    }

    void Recorder::setInputFormat(Frame &f)
    {
        img_convert_ctx = sws_getContext(f.getWidth(), f.getHeight(), f.getFormat(), width, height, format, SWS_BICUBIC, NULL, NULL, NULL);
    }
}
