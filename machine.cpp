/************************************************************************** 
*   Cellula, is cellular automata simulator accelerated on GPU.           *
*   Copyright (C) 2011  Dušan Poizl                                       *
*                                                                         *
*   This program is free software: you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation, either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
***************************************************************************/

#include "machine.h"
#include "cellspace.h"
#include <CL/cl_gl.h>
/**
Konstruktor ktory vytvory context s danymi zariadeniami
@param platform_index cislo platformy ktora sa ma pouzit
@param devices vektor zariadeni z ktorych bude vytvoreny kontext ak je prazdny pouziju sa vsetky zariadenia
*/
Machine::Machine(int platform_index, cl_device_v devices)
{
    cl_platform_v platform_ids = getPlatforms();

    cl_context_properties cl_properties[] = { CL_CONTEXT_PLATFORM, (cl_context_properties)platform_ids[platform_index], 0 };
    init(cl_properties, devices);
    gl_interoperability = false;
}
/**
Konstruktor ktory vytvory context s danymi zariadeniami
@param gl_context OpenGL kontext s ktorym bude zviazany OpenCL kontext na sfunkcnenie OpenGL/OpenGL interoperability
@param display GLX display na ktorom je dany OpenGL zviazany
@param platform_index cislo platformy ktora sa ma pouzit
@param devices vektor zariadeni z ktorych bude vytvoreny kontext
*/
Machine::Machine(cl_context_properties gl_context, cl_context_properties display, int platform_index, cl_device_v devices)
{
    if(gl_context == 0 || display == 0)cerr << "Nie je inicializovane OpenGL" << endl;

    cl_platform_v platform_ids = getPlatforms();

    #ifdef __linux__
    cl_context_properties cl_properties[] = { CL_CONTEXT_PLATFORM, (cl_context_properties)platform_ids[platform_index],
        CL_GL_CONTEXT_KHR, gl_context, CL_GLX_DISPLAY_KHR, display, 0 };
    #endif
    #ifdef _WIN32
    cl_context_properties cl_properties[] = { CL_CONTEXT_PLATFORM, (cl_context_properties)platform_ids[platform_index],
        CL_GL_CONTEXT_KHR, gl_context, CL_WGL_HDC_KHR, display, 0 };
    #endif

    init(cl_properties, devices);
    gl_interoperability = true;
}
/**
pokracovanie konstruktora so spolocnym kodom.
@param cl_properties pole nastaveni privytvarani kontextu
@param devices vektor zariadeni ktore sa pouziju
*/
void Machine::init(cl_context_properties *cl_properties, cl_device_v &devices)
{
    cl_int err_code;
    if(devices.empty())context = clCreateContextFromType(cl_properties, CL_DEVICE_TYPE_ALL, NULL, NULL, &err_code);
    else context = clCreateContext(cl_properties, devices.size(), &devices[0], NULL, NULL, &err_code);

    if(err_code){ cerr << "Nepodarilo sa inicializovat OpenCL "; clGetError(err_code); return; }

    cl_char info[1000];
    cout << "Inicializujem OpenCL" << endl;
    clGetPlatformInfo((cl_platform_id)cl_properties[1], CL_PLATFORM_NAME, 1000, info, NULL); cout << "\tName:      \t" << info << endl;
    clGetPlatformInfo((cl_platform_id)cl_properties[1], CL_PLATFORM_VENDOR, 1000, info, NULL); cout << "\tVendor:  \t" << info << endl;
    clGetPlatformInfo((cl_platform_id)cl_properties[1], CL_PLATFORM_VERSION, 1000, info, NULL); cout << "\tVersion:\t" << info << endl;
    if(devices.empty())device = getDevices((cl_platform_id)cl_properties[1]);//ak je zoznam zariadeni prazny napln ho doplna
    else device = devices;
    for(cl_device_v::iterator i = device.begin(); i != device.end();i++)
    {
        queues.push_back(clCreateCommandQueue(context, *i, NULL, &err_code));
        if(err_code)
        {
            cerr << "Nepodarilo sa vytvorit OpenCL prikazove fronty "; clGetError(err_code);
            break;
        }
    }
    deleting = false;
}
/**
destruktor uvolni vsetky prostriedky a taktiez aj svojich potomkov
*/
Machine::~Machine()
{
    for(vector<cl_command_queue>::iterator i = queues.begin(); i != queues.end(); i++)
        clReleaseCommandQueue(*i);
    clReleaseContext(context);
}
/**
@return vektor obsahujuci vsetky platformy pristupne v systeme
*/
cl_platform_v Machine::getPlatforms()
{
    cl_int err_code;
    cl_platform_v platform_ids;
    cl_uint num_entries;
    err_code = clGetPlatformIDs(0, NULL, &num_entries);
    if(err_code)return platform_ids;
    platform_ids.resize(num_entries);
    err_code = clGetPlatformIDs(num_entries, &platform_ids[0], NULL);
    return platform_ids;
}
/**
@param platform_id ID platformy z ktorej sa maju vratit zariadenia
@param type typ zariadenia ktore ssa muju vratit CL_DEVICE_TYPE_GPU CL_DEVICE_TYPE_CPU
@return vektor zariadeni z danej platformy a typu
*/
cl_device_v Machine::getDevices(cl_platform_id platform_id, cl_device_type type)
{
    cl_device_v devices;
    cl_uint num_entries;
    cl_int err_code = clGetDeviceIDs(platform_id, type, 0, NULL, &num_entries);
    if(err_code)return devices;
    devices.resize(num_entries);
    clGetDeviceIDs(platform_id, type, num_entries, &devices[0], NULL);
    return devices;
}
/**
Registruje potomka.
@param child ukazovatel na potomka vola sa automaticky v konstruktore Model
*/
void Machine::addChild(Model *child)
{
    models.push_back(child);
}
/**
Odstrani potomka zo zoznau potomkov. Treba zmazat potom rucne. Volat z destruktora
@param child potomok ktory sa ma odstranit
*/
void Machine::removeChild(Model *child)
{
    if(!deleting)
        for(vector<Model*>::iterator i = models.begin(); i != models.end();i++)
            if(*i == child)
            {
                models.erase(i);
                break;
            }
}
/**
Prida do fronty na spustenie kernel aj s parametrami. Tie treba nastvit predtym ako sa zavola tato metoda.
Tato metoda nie je urcena na priame volanie z programu ale volanie z potomka.
@param kernel kernel ktory sa ma spustit
@param w sirka vypoctovej domeny
@param h vyska vypoctovej domeny
@param device index zariadenia na ktorom ma kernel bezat
*/
void Machine::run(cl_kernel kernel, size_t w, size_t h, int device)
{
    device=0;
    if(queues.size()==0)return;
    cl_int err_code;
    size_t global_work[] = { w, h };
    size_t local_work[] = { 8, 8, 1 };
    err_code = clEnqueueNDRangeKernel(queues[device], kernel, 2, NULL, global_work, local_work, 0, NULL, NULL);
    if(err_code){ cerr << "Chyba pri spusteni kernela "; clGetError(err_code); }
}

/**
Ziska OpenGL objekt pre pracu v OpenCL
@param obj OpenGL objekt ktory sa ziska
@param device index zariadenia na ktorom sa objekt ziska
*/
void Machine::acquireGLObject(cl_mem obj, int device)
{
    cl_int err_code;
    if(queues.size()==0)return;
    err_code = clEnqueueAcquireGLObjects(queues[device], 1, &obj, 0, NULL, NULL);
    if(err_code){ cerr << "Chyba pri ziskavani OpenGL objektu "; clGetError(err_code); }
}

/**
Uvolni OpenGL objekt pre dalsie pouzitie
@param obj OpenGL objekt ktory sa uvolni
@param device index zariadenia na ktorom sa objekt uvolni
*/
void Machine::releaseGLObject(cl_mem obj, int device)
{
    cl_int err_code;
    if(queues.size()==0)return;
    err_code = clEnqueueReleaseGLObjects(queues[device], 1, &obj, 0, NULL, NULL);
    if(err_code){ cerr << "Chyba pri uvolnovani OpenGL objektu "; clGetError(err_code); }
}

/**
Zavola nad kazdou frontou clFinish co zabezpeci ze vsetky cakajuce udalosti sa vykonaju
@param device index zaraidenia. Ak je definovane tak sa vykona clFinish iba na danom zariadeni.
*/
void Machine::finish(int device)
{
    if(device == -1)
        for(vector<cl_command_queue>::iterator i = queues.begin(); i != queues.end();i++)
            clFinish(*i);
    else if(device < (int)queues.size())clFinish(queues[device]);
}
/**
Skopiruje z CL buffera do pamete hosta
@param buf buffer z ktoreho sa bude kopirovat
@param data ukazovatel kam sa bude kopirovat
@param size velkost kopirovanych dat v bajtoch
@param block ak je true funkcia sa nevrati pokial nie su data skopirovane. ak je false tak sa kopirovanie vykona asynchronne
@return udalost ktora posluzi na synchronizaciu kopirovania. ak je blokove vrati NULL
*/
cl_event Machine::copy(cl_mem buf, void *data, size_t size, bool block)
{
    cl_int err_code;
    cl_event event = NULL;
    if(block)err_code = clEnqueueReadBuffer(queues[0], buf, CL_TRUE, 0, size, data, 0, NULL, NULL);
    else err_code = clEnqueueReadBuffer(queues[0], buf, CL_FALSE, 0, size, data, 0, NULL, &event);
    if(err_code){ cerr << "Chyba pri kopirovani z buffera"; clGetError(err_code); }
    return event;
}
/**
Skopiruje z CL buffera do pamete hosta
@param data ukazovatel odkial sa bude kopirovat
@param buf buffer do ktoreho sa bude kopirovat
@param size velkost kopirovanych dat v bajtoch
@param block ak je true funkcia sa nevrati pokial nie su data skopirovane. ak je false tak sa kopirovanie vykona asynchronne
@return udalost ktora posluzi na synchronizaciu kopirovania. ak je blokove vrati NULL
*/
cl_event Machine::copy(void *data, cl_mem buf, size_t size, bool block)
{
    cl_int err_code;
    cl_event event = NULL;
    if(block)err_code = clEnqueueWriteBuffer(queues[0], buf, CL_TRUE, 0, size, data, 0, NULL, NULL);
    else err_code = clEnqueueReadBuffer(queues[0], buf, CL_FALSE, 0, size, data, 0, NULL, &event);
    if(err_code){ cerr << "Chyba pri kopirovani do buffera"; clGetError(err_code); }
    return event;
}
/**
Skopiruje data medzi dvoma CL buffermi
@param buf1 buffer z ktoreho sa bude kopirovat
@param buf2 buffer do ktoreho sa bude kopirovat
@param size velkost kopirovanych dat v bajtoch
@param block ak je true funkcia vrati NULL. inak vracia event
@return udalost ktora posluzi na synchronizaciu kopirovania. ak je blokove vrati NULL
*/
cl_event Machine::copy(cl_mem buf1, cl_mem buf2, size_t size, bool block)
{
    cl_int err_code;
    cl_event event = NULL;
    if(block)err_code = clEnqueueCopyBuffer(queues[0], buf1, buf2, 0, 0, size, 0, NULL, NULL);
    else err_code = clEnqueueCopyBuffer(queues[0], buf1, buf2, 0, 0, size, 0, NULL, &event);
    if(err_code){ cerr << "Chyba pri kopirovani z buffera do buffera"; clGetError(err_code); }
    return event;
}
/**
Kopiruje data z OpenCL obrazka do pamete hosta
@param img OpenCl obrazok z ktoreho sa bude kopyrovat
@param origin poloha zacaitku odkial sa bude kopirovat
@param region velkost oblasti ktora sa bude kopirovat
@param block ak je true funkcia vrati NULL. inak vracia event
@return udalost ktora posluzi na synchronizaciu kopirovania. ak je blokove vrati NULL
*/
cl_event Machine::copyImage(cl_mem img, void *data, size_t origin[3], size_t region[3], bool block)
{
    cl_int err_code;
    cl_event event = NULL;
    if(block)err_code = clEnqueueReadImage(queues[0], img, block, origin, region, 0, 0, data, 0, NULL, NULL);
    else err_code = clEnqueueReadImage(queues[0], img, block, origin, region, 0, 0, data, 0, NULL, &event);
    if(err_code){ cerr << "Chyba pri kopirovani z obrazka do pamete "; clGetError(err_code); }
    return event;
}

/**
Vytvory memory buffer na kontexte aktualnej Machine
@param flags standartne priznaky pre CLbuffer. CL_MEM_READ_ONLY,CL_MEM_WRITE_ONLY,CL_MEM_*_HOST_PTR
@param size velkost buffera v bajtoch
@param ptr ukazovatel na pouzivatelske data ak je pouzity CL_MEM_*_HOST_PTR priznak
*/
cl_mem Machine::createBuffer(cl_mem_flags flags, size_t size, void *ptr)
{
    cl_int err_code;
    cl_mem ret = clCreateBuffer(context, flags, size, ptr, &err_code);
    if(err_code){ cerr << "Nepodarilo sa vytvorit buffer "; clGetError(err_code); }
    return ret;
}

/**
Vytvory z OpenGL textury OpenCL obrazok.
@param flags flagy s ktorymi sa vytvory obrazok. CL_MEM_READ_ONLY,CL_MEM_WRITE_ONLY
@param texture OpenGL textura z ktorej sa vytvory OpenCL image2d_t
@return OpenCL textura
*/
cl_mem Machine::createFromGLTexture(cl_mem_flags flags, GLuint texture)
{
    cl_int err_code;
    cl_mem tex = clCreateFromGLTexture2D(context, CL_MEM_WRITE_ONLY, GL_TEXTURE_2D, 0, texture, &err_code);
    if(err_code){ cerr << "Nepodarilo sa vytvorit CL objekt z GL textury "; clGetError(err_code); }
    return tex;
}

/**
Vytvory OpenCL obrazok
@param flags flagy s ktorymi sa vytvory obrazok. CL_MEM_READ_ONLY,CL_MEM_WRITE_ONLY
@param w sirka vytvorenej textury
@param h vyska vytvorenej textury
@param ptr ukazovatel na data ktore sa nahraju do textury
*/
cl_mem Machine::createTexture(cl_mem_flags flags, size_t w, size_t h, void *ptr)
{
    cl_int err_code;
    cl_mem tex;
    cl_image_format format ={ CL_RGBA, CL_UNORM_INT8 };
    tex = clCreateImage2D(context, flags, &format, w, h, 0, ptr, &err_code);
    if(err_code){ cerr << "Nepodarilo sa vytvorit CL image "; clGetError(err_code); }
    return tex;
}

/**
Vytvory novy Cl program.
@param source zdrojovy kod programu
@return novy OpenCL program
*/
cl_program Machine::createProgram(const string &source)
{
    size_t len[] = { source.length() };
    cl_int err_code;
    const char *s = source.c_str();
    cl_program program = clCreateProgramWithSource(context, 1, &s, len, &err_code);
    if(err_code){ cerr << "Nepodarilo sa vytvorit program "; clGetError(err_code); }
    return program;
}

/**
Nastavy lokalny worksize pre dane zariadenie.
@param index cislo zariadenia
@param size velkost worksize
*/
void Machine::setLocalworkSize(int index, size_t size)
{
    if(index<(int)local_work_sizes.size())local_work_sizes[index] = size;
}
