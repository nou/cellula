/**************************************************************************
*   Cellula, is cellular automata simulator accelerated on GPU.           *
*   Copyright (C) 2011  Dušan Poizl                                       *
*                                                                         *
*   This program is free software: you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation, either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
***************************************************************************/

#include "options.h"
#include <QCheckBox>
#include <QBitArray>
#include <QLabel>


Options::Options(QWidget *parent) : QDialog(parent)
{
    vertical_layout = new QVBoxLayout(this);
    //resize(400, 300);
    setWindowTitle(trUtf8("Nastavenia"));
    tabs = new QTabWidget(this);

    ok_cancel = new QDialogButtonBox(this);
    ok_cancel->setStandardButtons(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);

    QPushButton *o = ok_cancel->button(QDialogButtonBox::Ok);
    connect((QObject*)o, SIGNAL(pressed()), this, SLOT(accept()));
    o = ok_cancel->button(QDialogButtonBox::Cancel);
    connect((QObject*)o, SIGNAL(pressed()), this, SLOT(reject()));

    vertical_layout->addWidget(tabs);
    vertical_layout->addWidget(ok_cancel);
    createOpenCL();
    reject();//v reject() je nacitanie povodnych hodnout
}

Options::~Options()
{
    //dtor
}

void Options::accept()
{
    //TODO dorobit vsetky ostatne nastavenia
    settings.setValue("opencl/platform", platforms->currentIndex());

    QLayout *layout = platform_stack->currentWidget()->layout();
    int count = layout->count();
    QBitArray devices(count);
    QList<QVariant> local_sizes;
    for(int i=0;i<count;i++)
    {
        QCheckBox *box = dynamic_cast<QCheckBox*>(layout->itemAt(i)->layout()->itemAt(0)->widget());
        if(box && box->isChecked())devices.setBit(i);
        /*QSpinBox *spin = dynamic_cast<QSpinBox*>(layout->itemAt(i)->layout()->itemAt(1)->widget());
        if(spin)local_sizes.append(spin->value());*/
    }
    settings.setValue("opencl/devices", devices);
    //settings.setValue("opencl/device_local_sizes", local_sizes);
    settings.setValue("opencl/cl_gl_interoperability", cl_gl_interoperablity->isChecked());

    QDialog::accept();
}

void Options::reject()
{
    int platform_id = settings.value("opencl/platform", 0).toInt();
    if(platform_id > platforms->count())platform_id=0;
    platforms->setCurrentIndex(platform_id);

    QLayout *layout = platform_stack->widget(platform_id)->layout();
    int count = layout->count();
    QBitArray devices(count);
    QList<QVariant> local_work_sizes,default_local_sizes;
    for(int i=0;i<count;i++)default_local_sizes.append(64);
    devices = settings.value("opencl/devices", QBitArray(count)).toBitArray();
    devices.resize(count);
    //local_work_sizes = settings.value("opencl/device_local_sizes", default_local_sizes).toList();
    QVector<QVariant> vect = local_work_sizes.toVector();
    for(int i=0;i<count;i++)
    {
        QCheckBox *box = dynamic_cast<QCheckBox*>(layout->itemAt(i)->layout()->itemAt(0)->widget());
        if(box)box->setChecked(devices[i]);
        /*QSpinBox *spin = dynamic_cast<QSpinBox*>(layout->itemAt(i)->layout()->itemAt(1)->widget());
        if(spin)spin->setValue(vect[i].toInt());*/
    }
    cl_gl_interoperablity->setChecked(settings.value("opencl/cl_gl_interoperability", false).toBool());
    QDialog::reject();
}

void Options::createOpenCL()
{
    opencl = new QWidget(this);
    tabs->addTab(opencl, trUtf8("OpenCL"));
    platform_stack = new QStackedWidget(this);

    platforms = new QComboBox(this);
    cl_platform_v platform_ids;
    platform_ids = Machine::getPlatforms();

    QWidget *platform_widget;
    for(cl_platform_v::iterator i = platform_ids.begin(); i != platform_ids.end();i++)
    {
        platforms->addItem(getPlatformInfo(*i).c_str());//pridame platformu do comboboxu
        platform_widget = new QWidget(platform_stack);//widget obsahujuci zoznam zaraideni v platforme
        QVBoxLayout *vbox = new QVBoxLayout(platform_widget);
        platform_widget->setLayout(vbox);//vertikalne usporiadanie zariadeni
        cl_device_v devices = Machine::getDevices(*i);
        for(cl_device_v::iterator o = devices.begin(); o != devices.end(); o++)
        {
            QHBoxLayout *hbox = new QHBoxLayout();
            vbox->addLayout(hbox);
            hbox->addWidget(new QCheckBox(getDeviceInfoShort(*o).c_str()), 10, Qt::AlignLeft);
            /*QSpinBox *spin = new QSpinBox();
            spin->setMaximum(getLocalWorkSize(*o));
            hbox->addWidget(spin, 0, Qt::AlignRight);*/
        }
        platform_stack->addWidget(platform_widget);
    }

    QFrame *line = new QFrame();
    line->setFrameShape(QFrame::HLine);
    line->setFrameShadow(QFrame::Sunken);
    cl_gl_interoperablity = new QCheckBox(trUtf8("Použiť OpenCL/OpenGL interoperabilitu"));

    opencl->setLayout(new QVBoxLayout(opencl));
    opencl->layout()->addWidget(platforms);
    opencl->layout()->addWidget(platform_stack);
    opencl->layout()->addWidget(line);
    opencl->layout()->addWidget(cl_gl_interoperablity);
    connect(platforms, SIGNAL(activated(int)), platform_stack, SLOT(setCurrentIndex(int)));
}

cl_device_v Options::getDevices()
{
    int platform_index = platforms->currentIndex();
    cl_device_v devices = Machine::getDevices(Machine::getPlatforms()[platform_index]);
    cl_device_v ret;
    QBitArray selected_devices = settings.value("opencl/devices").toBitArray();

    for(int i=0; i<selected_devices.size();i++)
        if(selected_devices[i])ret.push_back(devices[i]);
    return ret;
}

bool Options::useCLGLInteroperability()
{
    return settings.value("opencl/cl_gl_interoperability", false).toBool();
}
