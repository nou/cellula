/************************************************************************** 
*   Cellula, is cellular automata simulator accelerated on GPU.           *
*   Copyright (C) 2011  Dušan Poizl                                       *
*                                                                         *
*   This program is free software: you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation, either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
***************************************************************************/

#include "clutility.h"
#include <CL/cl_gl.h>

void getDeviceInfo(cl_device_id device)
{
    cl_uint uint;
    cl_ulong ulong;
    size_t size;
    cl_bool b;
    cl_device_fp_config fp_conf;
    cl_device_mem_cache_type mem_cache_type;
    cl_device_type type;
    char info[10000];
    clGetDeviceInfo(device, CL_DEVICE_VENDOR_ID, sizeof(cl_uint), &uint, NULL);
    cout << "Device vendor ID: " << uint << endl;
    clGetDeviceInfo(device, CL_DEVICE_TYPE, sizeof(cl_device_type), &type, NULL);
    cout << "Device type: ";
    if(type & CL_DEVICE_TYPE_DEFAULT)cout << "DEFAULT ";
    if(type & CL_DEVICE_TYPE_CPU)cout << "CPU ";
    if(type & CL_DEVICE_TYPE_GPU)cout << "GPU ";
    if(type & CL_DEVICE_TYPE_ACCELERATOR)cout << "ACCELERATOR ";
    cout << endl;
    clGetDeviceInfo(device, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(cl_uint), &uint, NULL);
    cout << "Max compute units: " << uint << endl;
    clGetDeviceInfo(device, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof(cl_uint), &uint, NULL);
    cout << "Max work item dimensions: " << uint << endl;
    clGetDeviceInfo(device, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(size_t), &size, NULL);
    cout << "Max work group size: " << size << endl;
    clGetDeviceInfo(device, CL_DEVICE_MAX_WORK_ITEM_SIZES, sizeof(size_t), &size, NULL);
    cout << "Max work item sizes: " << size << endl;
    clGetDeviceInfo(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR, sizeof(cl_uint), &uint, NULL);
    cout << "Prefered vector width char: " << uint << endl;
    clGetDeviceInfo(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT, sizeof(cl_uint), &uint, NULL);
    cout << "Prefered vector width short: " << uint << endl;
    clGetDeviceInfo(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT, sizeof(cl_uint), &uint, NULL);
    cout << "Prefered vector width int: " << uint << endl;
    clGetDeviceInfo(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG, sizeof(cl_uint), &uint, NULL);
    cout << "Prefered vector width long: " << uint << endl;
    clGetDeviceInfo(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT, sizeof(cl_uint), &uint, NULL);
    cout << "Prefered vector width float: " << uint << endl;
    clGetDeviceInfo(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE, sizeof(cl_uint), &uint, NULL);
    cout << "Prefered vector width double: " << uint << endl;
    clGetDeviceInfo(device, CL_DEVICE_MAX_CLOCK_FREQUENCY, sizeof(cl_uint), &uint, NULL);
    cout << "Max clock frequency: " << uint << endl;
    clGetDeviceInfo(device, CL_DEVICE_ADDRESS_BITS, sizeof(cl_uint), &uint, NULL);
    cout << "Address bits: " << uint << endl;
    /*clGetDeviceInfo(device, CL_DEVICE_MAX_READ_IMAGE_ARGS, sizeof(), &, NULL);
    cout << ": " << uint << endl;
    clGetDeviceInfo(device, CL_DEVICE_MAX_WRITE_IMAGE_ARGS, sizeof(), &, NULL);
    cout << ": " << uint << endl;*/
    clGetDeviceInfo(device, CL_DEVICE_MAX_MEM_ALLOC_SIZE, sizeof(cl_ulong), &ulong, NULL);
    cout << "Max mem alloc size: " << ulong/1024/1024 << " MiB" << endl;
    /*clGetDeviceInfo(device, CL_DEVICE_IMAGE2D_MAX_WIDTH, sizeof(), &, NULL);
    cout << ": " << uint << endl;
    clGetDeviceInfo(device, CL_DEVICE_IMAGE2D_MAX_HEIGHT, sizeof(), &, NULL);
    cout << ": " << uint << endl;
    clGetDeviceInfo(device, CL_DEVICE_IMAGE3D_MAX_WIDTH, sizeof(), &, NULL);
    cout << ": " << uint << endl;
    clGetDeviceInfo(device, CL_DEVICE_IMAGE3D_MAX_HEIGHT, sizeof(), &, NULL);
    cout << ": " << uint << endl;
    clGetDeviceInfo(device, CL_DEVICE_IMAGE3D_MAX_DEPTH, sizeof(), &, NULL);
    cout << ": " << uint << endl;*/
    clGetDeviceInfo(device, CL_DEVICE_IMAGE_SUPPORT, sizeof(cl_bool), &b, NULL);
    cout << "Image support: " << b << endl;
    clGetDeviceInfo(device, CL_DEVICE_MAX_PARAMETER_SIZE, sizeof(size_t), &size, NULL);
    cout << "Max parameter size: " << size << endl;
    /*clGetDeviceInfo(device, CL_DEVICE_MAX_SAMPLERS, sizeof(), &, NULL);
    cout << ": " << uint << endl;*/
    clGetDeviceInfo(device, CL_DEVICE_MEM_BASE_ADDR_ALIGN, sizeof(cl_uint), &uint, NULL);
    cout << "Mem base address align: " << uint << endl;
    clGetDeviceInfo(device, CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE, sizeof(cl_uint), &uint, NULL);
    cout << "Min data type align size: " << uint << endl;
    clGetDeviceInfo(device, CL_DEVICE_SINGLE_FP_CONFIG, sizeof(cl_device_fp_config), &fp_conf, NULL);
    cout << "Single FP config: ";
    if(fp_conf & CL_FP_DENORM)cout << "DENORM ";
    if(fp_conf & CL_FP_INF_NAN)cout << "INF_NAN ";
    if(fp_conf & CL_FP_ROUND_TO_NEAREST)cout << "ROUND_TO_NEAREST ";
    if(fp_conf & CL_FP_ROUND_TO_ZERO)cout << "ROUND_TO_ZERO ";
    if(fp_conf & CL_FP_ROUND_TO_INF)cout << "ROUND_TO_INF ";
    if(fp_conf & CL_FP_FMA)cout << "FMA ";
    cout << endl;
    clGetDeviceInfo(device, CL_DEVICE_GLOBAL_MEM_CACHE_TYPE, sizeof(cl_device_mem_cache_type), &mem_cache_type, NULL);
    cout << "Global mem cache type: ";
    if(mem_cache_type == CL_NONE)cout << "NONE ";
    if(mem_cache_type == CL_READ_ONLY_CACHE)cout << "READ_ONLY_CACHE ";
    if(mem_cache_type == CL_READ_WRITE_CACHE)cout << "READ_WRITE_CACHE ";
    cout << endl;
    clGetDeviceInfo(device, CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE, sizeof(cl_uint), &uint, NULL);
    cout << "Global mem cacheline size: " << uint << endl;
    clGetDeviceInfo(device, CL_DEVICE_GLOBAL_MEM_CACHE_SIZE, sizeof(cl_ulong), &ulong, NULL);
    cout << "Global mem cache size: " << ulong/1024 << " kiB" << endl;
    clGetDeviceInfo(device, CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(cl_ulong), &ulong, NULL);
    cout << "Global mem size: " << ulong/1024/1024 << " MiB" << endl;
    clGetDeviceInfo(device, CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE, sizeof(cl_ulong), &ulong, NULL);
    cout << "Max constant buffer size: " << ulong/1024 << " kiB" << endl;
    clGetDeviceInfo(device, CL_DEVICE_MAX_CONSTANT_ARGS, sizeof(cl_ulong), &ulong, NULL);
    cout << "Max constant args: " << ulong << endl;
    clGetDeviceInfo(device, CL_DEVICE_LOCAL_MEM_TYPE, sizeof(cl_device_local_mem_type), &mem_cache_type, NULL);
    cout << "Local mem type: ";
    if(mem_cache_type == CL_LOCAL)cout << "LOCAL ";
    if(mem_cache_type == CL_GLOBAL)cout << "GLOBAL ";
    cout << endl;
    clGetDeviceInfo(device, CL_DEVICE_LOCAL_MEM_SIZE, sizeof(cl_ulong), &ulong, NULL);
    cout << "Local mem size: " << ulong/1024 << " kiB" << endl;
    /*clGetDeviceInfo(device, CL_DEVICE_ERROR_CORRECTION_SUPPORT, sizeof(), &, NULL);
    cout << ": " << uint << endl;
    clGetDeviceInfo(device, CL_DEVICE_PROFILING_TIMER_RESOLUTION, sizeof(), &, NULL);
    cout << ": " << uint << endl;
    clGetDeviceInfo(device, CL_DEVICE_ENDIAN_LITTLE, sizeof(), &, NULL);
    cout << ": " << uint << endl;
    clGetDeviceInfo(device, CL_DEVICE_AVAILABLE, sizeof(), &, NULL);
    cout << ": " << uint << endl;
    clGetDeviceInfo(device, CL_DEVICE_COMPILER_AVAILABLE, sizeof(), &, NULL);
    cout << ": " << uint << endl;
    clGetDeviceInfo(device, CL_DEVICE_EXECUTION_CAPABILITIES, sizeof(), &, NULL);
    cout << ": " << uint << endl;
    clGetDeviceInfo(device, CL_DEVICE_QUEUE_PROPERTIES, sizeof(), &, NULL);
    cout << ": " << uint << endl;*/
    clGetDeviceInfo(device, CL_DEVICE_NAME, 10000, &info, NULL);
    cout << "Name: " << info << endl;
    clGetDeviceInfo(device, CL_DEVICE_VENDOR, 10000, &info, NULL);
    cout << "Vendor: " << info << endl;
    clGetDeviceInfo(device, CL_DRIVER_VERSION, 10000, &info, NULL);
    cout << "Driver version: " << info << endl;
    clGetDeviceInfo(device, CL_DEVICE_PROFILE, 10000, &info, NULL);
    cout << "Profile: " << info << endl;
    clGetDeviceInfo(device, CL_DEVICE_VERSION, 10000, &info, NULL);
    cout << "Version: " << info << endl;
    clGetDeviceInfo(device, CL_DEVICE_EXTENSIONS, 10000, &info, NULL);
    cout << "Extensions: " << info << endl;
}

void clGetError(cl_int err_code)
{
    switch(err_code)
    {
        case CL_DEVICE_NOT_FOUND: cerr << "DEVICE_NOT_FOUND" << endl; break;
        case CL_DEVICE_NOT_AVAILABLE: cerr << "DEVICE_NOT_AVAILABLE" << endl; break;
        case CL_COMPILER_NOT_AVAILABLE: cerr << "COMPILER_NOT_AVAILABLE" << endl; break;
        case CL_MEM_OBJECT_ALLOCATION_FAILURE: cerr << "MEM_OBJECT_ALLOCATION_FAILURE" << endl; break;
        case CL_OUT_OF_RESOURCES: cerr << "OUT_OF_RESOURCES" << endl; break;
        case CL_OUT_OF_HOST_MEMORY: cerr << "OUT_OF_HOST_MEMORY" << endl; break;
        case CL_PROFILING_INFO_NOT_AVAILABLE: cerr << "PROFILING_INFO_NOT_AVAILABLE" << endl; break;
        case CL_MEM_COPY_OVERLAP: cerr << "MEM_COPY_OVERLAP" << endl; break;
        case CL_IMAGE_FORMAT_MISMATCH: cerr << "IMAGE_FORMAT_MISMATCH" << endl; break;
        case CL_IMAGE_FORMAT_NOT_SUPPORTED: cerr << "IMAGE_FORMAT_NOT_SUPPORTED" << endl; break;
        case CL_BUILD_PROGRAM_FAILURE: cerr << "BUILD_PROGRAM_FAILURE" << endl; break;
        case CL_MAP_FAILURE: cerr << "MAP_FAILURE" << endl; break;
        case CL_INVALID_VALUE: cerr << "INVALID_VALUE" << endl; break;
        case CL_INVALID_DEVICE_TYPE: cerr << "INVALID_DEVICE_TYPE" << endl; break;
        case CL_INVALID_PLATFORM: cerr << "INVALID_PLATFORM" << endl; break;
        case CL_INVALID_DEVICE: cerr << "INVALID_DEVICE" << endl; break;
        case CL_INVALID_CONTEXT: cerr << "INVALID_CONTEXT" << endl; break;
        case CL_INVALID_QUEUE_PROPERTIES: cerr << "INVALID_QUEUE_PROPERTIES" << endl; break;
        case CL_INVALID_COMMAND_QUEUE: cerr << "INVALID_COMMAND_QUEUE" << endl; break;
        case CL_INVALID_HOST_PTR: cerr << "INVALID_HOST_PTR" << endl; break;
        case CL_INVALID_MEM_OBJECT: cerr << "INVALID_MEM_OBJECT" << endl; break;
        case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR: cerr << "INVALID_IMAGE_FORMAT_DESCRIPTOR" << endl; break;
        case CL_INVALID_IMAGE_SIZE: cerr << "INVALID_IMAGE_SIZE" << endl; break;
        case CL_INVALID_SAMPLER: cerr << "INVALID_SAMPLER" << endl; break;
        case CL_INVALID_BINARY: cerr << "INVALID_BINARY" << endl; break;
        case CL_INVALID_BUILD_OPTIONS: cerr << "INVALID_BUILD_OPTIONS" << endl; break;
        case CL_INVALID_PROGRAM: cerr << "INVALID_PROGRAM" << endl; break;
        case CL_INVALID_PROGRAM_EXECUTABLE: cerr << "INVALID_PROGRAM_EXECUTABLE" << endl; break;
        case CL_INVALID_KERNEL_NAME: cerr << "INVALID_KERNEL_NAME" << endl; break;
        case CL_INVALID_KERNEL_DEFINITION: cerr << "INVALID_KERNEL_DEFINITION" << endl; break;
        case CL_INVALID_KERNEL: cerr << "INVALID_KERNEL" << endl; break;
        case CL_INVALID_ARG_INDEX: cerr << "INVALID_ARG_INDEX" << endl; break;
        case CL_INVALID_ARG_VALUE: cerr << "INVALID_ARG_VALUE" << endl; break;
        case CL_INVALID_ARG_SIZE: cerr << "INVALID_ARG_SIZE" << endl; break;
        case CL_INVALID_KERNEL_ARGS: cerr << "INVALID_KERNEL_ARGS" << endl; break;
        case CL_INVALID_WORK_DIMENSION: cerr << "INVALID_WORK_DIMENSION" << endl; break;
        case CL_INVALID_WORK_GROUP_SIZE: cerr << "INVALID_WORK_GROUP_SIZE" << endl; break;
        case CL_INVALID_WORK_ITEM_SIZE: cerr << "INVALID_WORK_ITEM_SIZE" << endl; break;
        case CL_INVALID_GLOBAL_OFFSET: cerr << "INVALID_GLOBAL_OFFSET" << endl; break;
        case CL_INVALID_EVENT_WAIT_LIST: cerr << "INVALID_EVENT_WAIT_LIST" << endl; break;
        case CL_INVALID_EVENT: cerr << "INVALID_EVENT" << endl; break;
        case CL_INVALID_OPERATION: cerr << "INVALID_OPERATION" << endl; break;
        case CL_INVALID_GL_OBJECT: cerr << "INVALID_GL_OBJECT" << endl; break;
        case CL_INVALID_BUFFER_SIZE: cerr << "INVALID_BUFFER_SIZE" << endl; break;
        case CL_INVALID_MIP_LEVEL: cerr << "INVALID_MIP_LEVEL" << endl; break;
        case CL_INVALID_GL_SHAREGROUP_REFERENCE_KHR: cerr << "INVALID_GL_SHAREGROUP_REFERENCE_KHR" << endl; break;
        default: cerr << "Neznama chyba " << err_code << endl; break;
    }
}

string getPlatformInfo(cl_platform_id platform_id)
{
    string ret;
    char info[1000];
    clGetPlatformInfo(platform_id, CL_PLATFORM_NAME, 1000, info, NULL);
    ret = info;
    clGetPlatformInfo(platform_id, CL_PLATFORM_VENDOR, 1000, info, NULL);
    ret += ", ";
    ret += info;
    return ret;
}

string getDeviceInfoShort(cl_device_id device)
{
    string ret;
    char info[1000];
    clGetDeviceInfo(device, CL_DEVICE_NAME, 1000, info, NULL);
    ret = info;
    cl_device_type type;
    clGetDeviceInfo(device, CL_DEVICE_TYPE, sizeof(cl_device_type), &type, NULL);
    switch(type)
    {
        case CL_DEVICE_TYPE_CPU:
        ret += " - CPU";
        break;
        case CL_DEVICE_TYPE_GPU:
        ret += " - GPU";
        break;
        case CL_DEVICE_TYPE_ACCELERATOR:
        ret += " - Accelerator";
        break;
    }
    return ret;
}

string getBuildLog(cl_program program)
{
    size_t num_dev;
    string ret = "BUILD LOG:\n====================\n";
    clGetProgramInfo(program, CL_PROGRAM_DEVICES, 0, NULL, &num_dev);
    cl_device_id devices[num_dev];
    clGetProgramInfo(program, CL_PROGRAM_DEVICES, num_dev, devices, NULL);

    num_dev /= sizeof(size_t);
    for(size_t i=0;i<num_dev;i++)
    {
        size_t len;
        clGetProgramBuildInfo(program, devices[i], CL_PROGRAM_BUILD_LOG, 0, NULL, &len);
        char build_log[len];
        clGetProgramBuildInfo(program, devices[i], CL_PROGRAM_BUILD_LOG, len, build_log, NULL);
        ret += build_log;
    }
    return ret;
}

size_t getLocalWorkSize(cl_device_id device)
{
    size_t s;
    clGetDeviceInfo(device, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof(size_t), &s, NULL);
    size_t size[s];
    if(clGetDeviceInfo(device, CL_DEVICE_MAX_WORK_ITEM_SIZES, sizeof(size_t)*s, size, NULL) == CL_SUCCESS)return size[0];
    else return 0;
}
