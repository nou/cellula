/**************************************************************************
*   Cellula, is cellular automata simulator accelerated on GPU.           *
*   Copyright (C) 2011  Dušan Poizl                                       *
*                                                                         *
*   This program is free software: you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation, either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
***************************************************************************/

#ifndef LUACONSOLE_H
#define LUACONSOLE_H

#include <QTextEdit>
#include <iostream>
#include <vector>
#include <sstream>
#include <string>
extern "C"
{
    #include <lua5.1/lualib.h>
    #include <lua5.1/lauxlib.h>
}
#include "lua/luawrapper.h"

using namespace std;

class LuaConsole : public QTextEdit
{
    public:
    LuaConsole(CellulaMainWindow *mainw, QWidget * parent = 0);
    LuaConsole(const QString & text, CellulaMainWindow *mainw, QWidget * parent = 0);
    ~LuaConsole();
    protected:
    void keyPressEvent(QKeyEvent *e);
    void execute();
    void historyKey(int key);
    static int luaPrint(lua_State *L);
    void luaPrint(string s);
    private:
    void init();
    lua_State *Lstate;
    vector<QString> history;
    int history_index;
    LuaMain *luamain;
    CellulaMainWindow *mainw;
    //protected slot:
    //void moveCursorToEnd();
};

#endif // LUACONSOLE_H
