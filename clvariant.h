/************************************************************************** 
*   Cellula, is cellular automata simulator accelerated on GPU.           *
*   Copyright (C) 2011  Dušan Poizl                                       *
*                                                                         *
*   This program is free software: you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation, either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
***************************************************************************/

#ifndef _CLVARIANT_H_NOU_
#define _CLVARIANT_H_NOU_

#include <CL/cl.h>

class CLVariant
{
    enum type
    {
        CL_INT1,
        CL_INT2,
        CL_INT4,
        CL_INT8,
        CL_INT16,
        CL_FLOAT1,
        CL_FLOAT2,
        CL_FLOAT4,
        CL_FLOAT8,
        CL_FLOAT16,
        CL_IMAGE,
        CL_BUFFER
    }type;
    union data
    {
        cl_int int1;
        cl_int2 int2;
        cl_int4 int4;
        cl_int8 int8;
        cl_int16 int16;
        cl_float float1;
        cl_float2 float2;
        cl_float4 float4;
        cl_float8 float8;
        cl_float16 float16;
        cl_mem mem;
    }data;
    public:
    CLVariant(cl_int i);
    CLVariant(cl_int2 i);
    CLVariant(cl_int4 i);
    CLVariant(cl_int8 i);
    CLVariant(cl_int16 i);
    CLVariant(cl_float i);
    CLVariant(cl_float2 i);
    CLVariant(cl_float4 i);
    CLVariant(cl_float8 i);
    CLVariant(cl_float16 i);
    CLVariant(cl_mem m);
};

#endif // _CLVARIANT_H_NOU_
