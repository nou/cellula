/************************************************************************** 
*   Cellula, is cellular automata simulator accelerated on GPU.           *
*   Copyright (C) 2011  Dušan Poizl                                       *
*                                                                         *
*   This program is free software: you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation, either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
***************************************************************************/

#ifndef _CDPARSER_H_NOU_
#define _CDPARSER_H_NOU_

#include "parser.tab.hh"
#include "scanner.hh"
#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <fstream>
#include <sstream>

using namespace std;

namespace DEVS
{
    class Param
    {
        public:
        string name, value;
        int startx, starty, endx, endy;
        Param(string n, string v)
        {
            name = n;
            value = v;
            transform(name.begin(), name.end(), name.begin(), ::tolower);
        }
        Param(string n, string v, int x1, int y1, int x2, int y2)
        {
            name = n;
            value = v;
            transform(name.begin(), name.end(), name.begin(), ::tolower);
            startx = x1<x2 ? x1 : x2;
            starty = y1<y2 ? y1 : y2;
            endx = x1>x2 ? x1 : x2;
            endy = y1>y2 ? y1 : y2;
        }
        Param(string n, int a, int b)
        {
            name = n;
            startx = a; starty = b;
        }
    };
    class Section
    {
        public:
        bool wrapped_border;
        int width, height;
        set<string> neighbors;
        map<string, int> neighbors_index;
        double initialvalue;
        string local_transition;
        double timewindow;
        Section();
    };
    class ModelDefinition
    {
        public:
        map<string, string> rules;
        Section section;
        bool section_set;
        string zones;
        public:
        ModelDefinition(){ section_set = false; }
        int addRule(string &name, string &rule);
        int setSection(vector<Param> &params);
        string transfer_func_declaration;
    };
    class CDparser
    {
        public:
        Parser *parser;
        Scanner *scanner;
        ModelDefinition model;
        vector<Param> params;
        istringstream stream;
        stringstream error;
        public:
        CDparser(std::string &source)
        {
            parser = new Parser(this);
            stream.str(source);
            scanner = new Scanner(&stream, &std::cout);
        }
        ~CDparser()
        {
            delete parser;
            delete scanner;
        }
        int parse()
        {
            return parser->parse();
        }
        string compiledSource()
        {
            stringstream ss;
            ss << model.transfer_func_declaration << endl;
            for(map<string, string>::iterator i = model.rules.begin();i != model.rules.end(); i++)
            {
                ss << i->second << endl;
            }

            ss << "_event transfer_func(_state current_state, _time current_time, _event *event_vector, _seed *seed){" << endl;
            ss << "size_t x,y;\nx = get_global_id(0);\ny = get_global_id(1);" << endl;
            ss << model.zones;
            ss << "else return " << model.section.local_transition <<  "(current_state, current_time, event_vector, seed);}" << endl;
            ss << endl;

            return ss.str();
        }
        string neighbors()
        {
            vector<string> n;
            n.resize(model.section.neighbors_index.size());
            for(map<string, int>::iterator i = model.section.neighbors_index.begin(); i != model.section.neighbors_index.end();i++)
            {
                n[i->second] = i->first;
            }
            stringstream ss;
            ss << "__constant int neighbors[] = {";
            for(unsigned int i=0;i<n.size();i++)
            {
                string s;
                s = n[i].substr(1, n[i].length()-2);
                ss << s << ", ";
            }
            ss << "};\n";
            return ss.str();
        }
        static string toString(double d)
        {
            stringstream s;
            s.precision(20);
            s << d;
            return s.str();
        }
        static string toString(int i)
        {
            stringstream s;
            s << i;
            return s.str();
        }
        static double constFunc(const std::string *name);
    };
};

#endif
