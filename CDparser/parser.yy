%{
    #include <string>
    #include <math.h>
    #include <limits>
    #include <map>
    #include "CDparser.h"
%}

%require "2.3"
%defines
%skeleton "lalr1.cc"
%name-prefix="DEVS"
%define "parser_class_name" "Parser"
%parse-param { class CDparser *driver }
%locations
%error-verbose

%union
{
    int intVal;
    double doubleVal;
    std::string *stringVal;
    struct { int x,y,z; } intVec;
};

%token END 0 "end of file"
%token LBRACKET "(" RBRACKET ")" LCBRACKET "{" RCBRACKET "}" LSBRACKET "[" RSBRACKET "]" // ( ) { } [ ]
%token AND "and" OR "or" XOR "xor" IMP "imp" EQV "eqv" NOT "not"// and or xor imp eqv not
%token TRUE "t" FALSE "f" UNDEFB "?b" UNDEFC "?c" // t f ?
%token MUL "*" DIV "/" // + - * /
%token EXCEQV "!=" EQVSIGN "=" GT ">" LT "<" GTEQV ">=" LTEQV "<=" // != = > <  >= <=
%token <stringVal> COND_REAL_FUNC // even odd isInt isPrime isUndefined
%token PORTVALUE "portValue" SEND "send" CELLPOS "cellPos" // portValue send cellPos
%token THISPORT "thisPort" // thisPort
%token IF "if" IFU "ifu" // if ifu
%token <stringVal> CONST_FUNC "const function"
%token <stringVal> WITHOUT_PARAM_FUNC "without parameter function"
%token <stringVal> UNARY_FUNC "unary function"
%token <stringVal> BINARY_FUNC "binary function"
%token COMA "," DOTS ".." // , .
%token RULETOKEN "rule"
%token COLON ":"
%token TOP "top" ELSE "else"
%token ZONE "zone" DIM "dim"

%token <intVal> UINT PLUS "+" MINUS "-"
%token <doubleVal> UREAL
%token <stringVal> STRING

%left AND OR XOR IMP EQV
%right NOT
%nonassoc EXCEQV EQVSIGN GT LT GTEQV LTEQV
%left PLUS MINUS
%left MUL DIV
%left LBRACKET RBRACKET

%type <intVal> SIGN INT
%type <doubleVal> CONSTANT BOOL REAL
%type <stringVal> SECNAME
%type <stringVal> RULELIST RULE ELSERULE RESULT BOOLEXP REALRELEXP REALEXP IDREF FUNCTION CELLREF PORTNAME


%destructor { delete $$; } <stringVal>
//%destructor { std::cerr << "destruktor" << std::endl;delete $$; } CONST_FUNC WITHOUT_PARAM_FUNC UNARY_FUNC BINARY_FUNC

%{
    #undef yylex
    #define yylex driver->scanner->lex
    #define NEWSTR new std::string;
%}

%% // pravidla

START :         TOPSECTION SECTIONTUPLA

TOPSECTION :    LSBRACKET TOP RSBRACKET PARAMTUPLA

SECTIONTUPLA :
                | SECTION SECTIONTUPLA

SECTION :       SECNAME PARAMTUPLA { if(driver->model.setSection(driver->params))driver->error << "Duplicated model definition section or wrong parameter.\n"; driver->params.clear(); delete $1; }
                | SECNAME RULELIST { if(driver->model.addRule(*$1, *$2))driver->error << "Duplicated rule section." << *$1 << endl; delete $1; delete $2; }

SECNAME :       LSBRACKET STRING RSBRACKET { $$ = $2; }

PARAMTUPLA :
                | PARAM PARAMTUPLA

PARAM :         ZONE COLON STRING LCBRACKET LBRACKET INT COMA INT RBRACKET RCBRACKET { driver->params.push_back(Param("zone", *$3, $6, $8, $6, $8)); delete $3; }
                | ZONE COLON STRING LCBRACKET LBRACKET INT COMA INT RBRACKET DOTS LBRACKET INT COMA INT RBRACKET RCBRACKET { driver->params.push_back(Param("zone", *$3, $6, $8, $12, $14)); delete $3; }
                | STRING COLON STRING { driver->params.push_back(Param(*$1, *$3)); delete $1; delete $3; }
                | STRING COLON CONSTANT { driver->params.push_back(Param(*$1, driver->toString($3))); delete $1; }
                | STRING COLON STRING CELLREF { driver->params.push_back(Param(*$1, *$4)); delete $1; delete $3; delete $4; }
                | DIM COLON LBRACKET INT COMA INT RBRACKET { driver->params.push_back(Param("dim", $4, $6)); }

RULELIST :      RULE { $$ = $1; }
                | RULE RULELIST { $$=NEWSTR; *$$ = *$1 + "\n" + *$2; delete $1; delete $2; }
                | ELSERULE { $$=$1; }
                | error RULELIST { $$ = $2; }

RULE :          RULETOKEN COLON RESULT RESULT LCBRACKET BOOLEXP RCBRACKET { $$=NEWSTR; *$$ = "if("+*$6+"){ new_event.state="+*$3+"; new_event.time="+*$4+"; return new_event; }"; delete $6; delete $3; delete $4; }
                | RULETOKEN COLON RESULT RESULT LCBRACKET error RCBRACKET { $$ = NEWSTR; delete $3; delete $4; }

ELSERULE :      STRING { $$=NEWSTR; *$$="return "+*$1+"(current_state, current_time, event_vector, seed);\n"; delete $1; }

RESULT :        CONSTANT { $$=NEWSTR; if(isnan($1))*$$ = " NAN "; else *$$ = driver->toString($1); }
                | LCBRACKET REALEXP RCBRACKET { $$=NEWSTR; *$$ = "("+*$2+")"; delete $2; }

BOOLEXP :       BOOL { $$=NEWSTR; if(isnan($1))*$$ = " NAN "; else *$$ = driver->toString($1); }
                | LBRACKET BOOLEXP RBRACKET { $$=NEWSTR; *$$ = "("+*$2+")"; delete $2; }
                | REALRELEXP { $$=NEWSTR; *$$ = *$1; delete $1; }
                | NOT BOOLEXP { $$=NEWSTR; *$$ = "!("+*$2+")"; delete $2; }
                | BOOLEXP AND BOOLEXP { $$=NEWSTR; *$$ = "DEVS_and("+*$1+","+*$3+") "; delete $1; delete $3; }
                | BOOLEXP OR BOOLEXP { $$=NEWSTR; *$$ = "DEVS_or("+*$1+","+*$3+") "; delete $1; delete $3; }
                | BOOLEXP XOR BOOLEXP { $$=NEWSTR; *$$ = "DEVS_xor("+*$1+", "+*$3+") "; delete $1; delete $3; }
                | BOOLEXP IMP BOOLEXP { $$=NEWSTR; *$$ = "DEVS_imp("+*$1+", "+*$3+") "; delete $1; delete $3; }
                | BOOLEXP EQV BOOLEXP { $$=NEWSTR; *$$ = "DEVS_eqv("+*$1+", "+*$3+") "; delete $1; delete $3; }

REALRELEXP :    REALEXP EXCEQV REALEXP { $$=NEWSTR; *$$ = *$1+"!="+*$3; delete $1; delete $3; }
                | REALEXP EQVSIGN REALEXP { $$=NEWSTR; *$$ = *$1+"=="+*$3; delete $1; delete $3; }
                | REALEXP GT REALEXP { $$=NEWSTR; *$$ = *$1+">"+*$3; delete $1; delete $3; }
                | REALEXP LT REALEXP { $$=NEWSTR; *$$ = *$1+"<"+*$3; delete $1; delete $3; }
                | REALEXP GTEQV REALEXP { $$=NEWSTR; *$$ = *$1+">="+*$3; delete $1; delete $3; }
                | REALEXP LTEQV REALEXP { $$=NEWSTR; *$$ = *$1+"<="+*$3; delete $1; delete $3; }
                | COND_REAL_FUNC LBRACKET REALEXP RBRACKET { $$=NEWSTR; *$$ = *$1+"("+*$3+")"; delete $1; delete $3; }

REALEXP :       IDREF { $$=NEWSTR; *$$ = *$1; delete $1; }
                | LBRACKET REALEXP RBRACKET { $$=NEWSTR; *$$ = "("+*$2+")"; delete $2; }
                | REALEXP PLUS REALEXP { $$=NEWSTR; *$$ = *$1+"+"+*$3; delete $1; delete $3; }
                | REALEXP MINUS REALEXP { $$=NEWSTR; *$$ = *$1+"-"+*$3; delete $1; delete $3; }
                | REALEXP MUL REALEXP { $$=NEWSTR; *$$ = *$1+"*"+*$3; delete $1; delete $3; }
                | REALEXP DIV REALEXP { $$=NEWSTR; *$$ = *$1+"/"+*$3; delete $1; delete $3; }

IDREF :         CELLREF { $$ = NEWSTR; if(*$1=="(0,0)")*$$ = "current_state"; else *$$ = "event_vector["+driver->toString(driver->model.section.neighbors_index[*$1])+"].state"; delete $1; }
                | CONSTANT { $$=NEWSTR; *$$ = driver->toString($1); }
                | FUNCTION { $$ = $1; }
                | PORTVALUE LBRACKET PORTNAME RBRACKET { $$=NEWSTR; *$$ = "portValue("+*$3+")"; delete $3; }
                | SEND LBRACKET PORTNAME COMA REALEXP RBRACKET { $$=NEWSTR; *$$ = "send("+*$3+","+*$5+")"; delete $3; delete $5; }
                | CELLPOS LBRACKET REALEXP RBRACKET { $$=NEWSTR; *$$ = "DEVS_cellPos("+*$3+")"; delete $3; }

CONSTANT :      INT { $$ = $1; }
                | REAL { $$ = $1; }
                | CONST_FUNC { $$ = driver->constFunc($1); delete $1; }
                | UNDEFC { $$ = NAN; }

INT :           UINT { $$ = $1; }
                | SIGN UINT { $$ = $1*$2; }

REAL :          UREAL { $$ = $1; }
                | SIGN UREAL { $$ = $1*$2; }

SIGN :          PLUS { $$ = $1; }
                | MINUS { $$ = $1; }

FUNCTION :      UNARY_FUNC LBRACKET REALEXP RBRACKET { $$=NEWSTR; *$$ = "DEVS_"+*$1+"("+*$3+")"; delete $1; delete $3; }
                | WITHOUT_PARAM_FUNC { $$=NEWSTR; *$$ = "DEVS_"+*$1+"()"; delete $1; }
                | BINARY_FUNC LBRACKET REALEXP COMA REALEXP RBRACKET { $$=NEWSTR; *$$ = "DEVS_"+*$1+"("+*$3+","+*$5+")"; delete $1; delete $3; delete $5; }
                | IF LBRACKET BOOLEXP COMA REALEXP COMA REALEXP RBRACKET { $$=NEWSTR; *$$ = "DEVS_if("+*$3+","+*$5+")"; delete $3; delete $5; delete $7; }
                | IFU LBRACKET BOOLEXP COMA REALEXP COMA REALEXP COMA REALEXP RBRACKET { $$=NEWSTR; *$$ = "DEVS_ifu("+*$3+","+*$5+","+*$7+","+*$9+")"; delete $3; delete $5; delete $7; delete $9; }

CELLREF :       LBRACKET INT COMA INT RBRACKET { $$=NEWSTR; *$$ = "("+driver->toString($2)+","+driver->toString($4)+")"; }

/*REST_TUPLA :    COMA INT REST_TUPLA { std::cout << "ZLE" << std::endl; }
                | RBRACKET { std::cout << "ZLE" << std::endl; }*/

BOOL :          TRUE { $$ = 1.0; }
                | FALSE { $$ = 0.0; }
                | UNDEFB { $$ = NAN; }

PORTNAME :      THISPORT { $$=NEWSTR; *$$ = "thisPort"; }
                | STRING { $$=NEWSTR; *$$ = *$1; delete $1; }

%% // dodatocny kod

void DEVS::Parser::error(const Parser::location_type& l, const std::string& m)
{
    driver->error << "Error " << l.begin.line << ":" << l.begin.column << "-" << l.end.column << " " << m << std::endl;
}
