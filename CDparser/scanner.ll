%{
#include <string>
#include <stdlib.h>
#include "scanner.hh"


typedef DEVS::Parser::token token;
typedef DEVS::Parser::token_type token_type;

//#define yyterminate() return token::END
#define YY_NO_UNISTD_H

#define yyterminate() return token::END

%}

%option c++
%option prefix="Scanner"
%option batch
%option yywrap nounput
%option stack

/* sledovnie polohy parsovania */
%{
#define YY_USER_ACTION  yylloc->columns(yyleng);
%}

%% /* regularne vyrazy */

%{
    yylloc->step();
%}

\%[^\n]*/\n             { }
\n                      { yylloc->lines(); }
<<EOF>>                 { return token::END; }
\{                      { return token::LCBRACKET; }
\}                      { return token::RCBRACKET; }
\(                      { return token::LBRACKET; }
\)                      { return token::RBRACKET; }
\[                      { return token::LSBRACKET; }
\]                      { return token::RSBRACKET; }
[0-9]+                  { yylval->intVal = atoi(YYText()); return token::UINT; }
[0-9]+\.[0-9]+          { yylval->doubleVal = atof(YYText()); return token::UREAL; }
t                       { return token::TRUE; }
f                       { return token::FALSE; }
\?b                     { return token::UNDEFB; }
\?c                     { return token::UNDEFC; }
\+                      { yylval->intVal = 1; return token::PLUS; }
\-                      { yylval->intVal =-1; return token::MINUS; }
\*                      { return token::MUL; }
\/                      { return token::DIV; }
\:                      { return token::COLON; }
"!="                    { return token::EXCEQV; }
"="                     { return token::EQVSIGN; }
">"                     { return token::GT; }
"<"                     { return token::LT; }
">="                    { return token::GTEQV; }
"<="                    { return token::LTEQV; }
","                     { return token::COMA; }
"\.\."                  { return token::DOTS; }
(?i:and)                { return token::AND; }
(?i:or)                 { return token::OR; }
(?i:xor)                { return token::XOR; }
(?i:imp)                { return token::IMP; }
(?i:eqv)                { return token::EQV; }
(?i:not)                { return token::NOT; }
(?i:top)                { return token::TOP; }
(?i:else)               { return token::ELSE; }
(?i:(even|odd|isInt|isPrime|isUndefined)) {
                        yylval->stringVal = new std::string(YYText()); return token::COND_REAL_FUNC; }
(?i:(pi|e|inf|grav|accel|light|planck|avogadro|faraday|rydberg|euler_gamma|bohr_radius|boltzmann|bohr_magneton|golden|catalan|amu|electron_charge|ideal_gas|stefan_boltzmann|proton_mass|electron_mass|neutron_mass|pem)) {
                        yylval->stringVal = new std::string(YYText()); return token::CONST_FUNC; }
(?i:(truecount|falsecount|undefcount|time|random|randomSign)) {
                        yylval->stringVal = new std::string(YYText()); return token::WITHOUT_PARAM_FUNC; }
(?i:(abs|acos|acosh|asin|asinh|atan|atanh|cos|sec|sech|exp|cosh|fact|fractional|ln|log|round|cotan|cosec|cosech|sign|sin|sinh|statecount|sqrt|tan|tanh|trunc|truncUpper|poisson|exponential|randInt|chi|asec|acotan|asech|acosech|nextPrime|radToDeg|degToRad|nth_prime|acotanh|CtoF|CtoK|KtoC|KtoF|FtoC|FtoK)) {
                        yylval->stringVal = new std::string(YYText()); return token::UNARY_FUNC; }
(?i:(comb|logn|max|min|power|remainder|root|beta|gamma|lcm|gcd|normal|f|uniform|binomial|rectToPolar_r|rectToPolar_angle|polarToRect_x|hip|polarToRect_y)) {
                        yylval->stringVal = new std::string(YYText()); return token::BINARY_FUNC; }
(?i:portValue)          { return token::PORTVALUE; }
(?i:send)               { return token::SEND; }
(?i:cellPos)            { return token::CELLPOS; }
(?i:thisPort)           { return token::THISPORT; }
(?i:ifu)                { return token::IFU; }
(?i:if)                 { return token::IF; }
(?i:rule)               { return token::RULETOKEN; }
(?i:zone)               { return token::ZONE; }
(?i:dim)                { return token::DIM; }
[A-Za-z]+               { yylval->stringVal = new std::string(YYText()); return token::STRING; }
.                       { }

%% /* dodatocny kod */

int ScannerFlexLexer::yylex()
{
    std::cerr << "in ExampleFlexLexer::yylex() !" << std::endl;
    return 0;
}

int ScannerFlexLexer::yywrap()
{
    return 1;
}
