
/* A Bison parser, made by GNU Bison 2.4.1.  */

/* Skeleton implementation for Bison LALR(1) parsers in C++
   
      Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 Free Software
   Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

// Take the name prefix into account.
#define yylex   DEVSlex

/* First part of user declarations.  */

/* Line 311 of lalr1.cc  */
#line 1 "parser.yy"

    #include <string>
    #include <math.h>
    #include <limits>
    #include <map>
    #include "CDparser.h"


/* Line 311 of lalr1.cc  */
#line 52 "parser.tab.cc"


#include "parser.tab.hh"

/* User implementation prologue.  */

/* Line 317 of lalr1.cc  */
#line 66 "parser.yy"

    #undef yylex
    #define yylex driver->scanner->lex
    #define NEWSTR new std::string;


/* Line 317 of lalr1.cc  */
#line 68 "parser.tab.cc"

#ifndef YY_
# if YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* FIXME: INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#define YYUSE(e) ((void) (e))

/* Enable debugging if requested.  */
#if YYDEBUG

/* A pseudo ostream that takes yydebug_ into account.  */
# define YYCDEBUG if (yydebug_) (*yycdebug_)

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)	\
do {							\
  if (yydebug_)						\
    {							\
      *yycdebug_ << Title << ' ';			\
      yy_symbol_print_ ((Type), (Value), (Location));	\
      *yycdebug_ << std::endl;				\
    }							\
} while (false)

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug_)				\
    yy_reduce_print_ (Rule);		\
} while (false)

# define YY_STACK_PRINT()		\
do {					\
  if (yydebug_)				\
    yystack_print_ ();			\
} while (false)

#else /* !YYDEBUG */

# define YYCDEBUG if (false) std::cerr
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_REDUCE_PRINT(Rule)
# define YY_STACK_PRINT()

#endif /* !YYDEBUG */

#define yyerrok		(yyerrstatus_ = 0)
#define yyclearin	(yychar = yyempty_)

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab
#define YYRECOVERING()  (!!yyerrstatus_)


/* Line 380 of lalr1.cc  */
#line 1 "[Bison:b4_percent_define_default]"

namespace DEVS {

/* Line 380 of lalr1.cc  */
#line 137 "parser.tab.cc"
#if YYERROR_VERBOSE

  /* Return YYSTR after stripping away unnecessary quotes and
     backslashes, so that it's suitable for yyerror.  The heuristic is
     that double-quoting is unnecessary unless the string contains an
     apostrophe, a comma, or backslash (other than backslash-backslash).
     YYSTR is taken from yytname.  */
  std::string
  Parser::yytnamerr_ (const char *yystr)
  {
    if (*yystr == '"')
      {
        std::string yyr = "";
        char const *yyp = yystr;

        for (;;)
          switch (*++yyp)
            {
            case '\'':
            case ',':
              goto do_not_strip_quotes;

            case '\\':
              if (*++yyp != '\\')
                goto do_not_strip_quotes;
              /* Fall through.  */
            default:
              yyr += *yyp;
              break;

            case '"':
              return yyr;
            }
      do_not_strip_quotes: ;
      }

    return yystr;
  }

#endif

  /// Build a parser object.
  Parser::Parser (class CDparser *driver_yyarg)
    :
#if YYDEBUG
      yydebug_ (false),
      yycdebug_ (&std::cerr),
#endif
      driver (driver_yyarg)
  {
  }

  Parser::~Parser ()
  {
  }

#if YYDEBUG
  /*--------------------------------.
  | Print this symbol on YYOUTPUT.  |
  `--------------------------------*/

  inline void
  Parser::yy_symbol_value_print_ (int yytype,
			   const semantic_type* yyvaluep, const location_type* yylocationp)
  {
    YYUSE (yylocationp);
    YYUSE (yyvaluep);
    switch (yytype)
      {
         default:
	  break;
      }
  }


  void
  Parser::yy_symbol_print_ (int yytype,
			   const semantic_type* yyvaluep, const location_type* yylocationp)
  {
    *yycdebug_ << (yytype < yyntokens_ ? "token" : "nterm")
	       << ' ' << yytname_[yytype] << " ("
	       << *yylocationp << ": ";
    yy_symbol_value_print_ (yytype, yyvaluep, yylocationp);
    *yycdebug_ << ')';
  }
#endif

  void
  Parser::yydestruct_ (const char* yymsg,
			   int yytype, semantic_type* yyvaluep, location_type* yylocationp)
  {
    YYUSE (yylocationp);
    YYUSE (yymsg);
    YYUSE (yyvaluep);

    YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

    switch (yytype)
      {
        case 27: /* "COND_REAL_FUNC" */

/* Line 480 of lalr1.cc  */
#line 63 "parser.yy"
	{ delete (yyvaluep->stringVal); };

/* Line 480 of lalr1.cc  */
#line 244 "parser.tab.cc"
	break;
      case 34: /* "\"const function\"" */

/* Line 480 of lalr1.cc  */
#line 63 "parser.yy"
	{ delete (yyvaluep->stringVal); };

/* Line 480 of lalr1.cc  */
#line 253 "parser.tab.cc"
	break;
      case 35: /* "\"without parameter function\"" */

/* Line 480 of lalr1.cc  */
#line 63 "parser.yy"
	{ delete (yyvaluep->stringVal); };

/* Line 480 of lalr1.cc  */
#line 262 "parser.tab.cc"
	break;
      case 36: /* "\"unary function\"" */

/* Line 480 of lalr1.cc  */
#line 63 "parser.yy"
	{ delete (yyvaluep->stringVal); };

/* Line 480 of lalr1.cc  */
#line 271 "parser.tab.cc"
	break;
      case 37: /* "\"binary function\"" */

/* Line 480 of lalr1.cc  */
#line 63 "parser.yy"
	{ delete (yyvaluep->stringVal); };

/* Line 480 of lalr1.cc  */
#line 280 "parser.tab.cc"
	break;
      case 50: /* "STRING" */

/* Line 480 of lalr1.cc  */
#line 63 "parser.yy"
	{ delete (yyvaluep->stringVal); };

/* Line 480 of lalr1.cc  */
#line 289 "parser.tab.cc"
	break;
      case 56: /* "SECNAME" */

/* Line 480 of lalr1.cc  */
#line 63 "parser.yy"
	{ delete (yyvaluep->stringVal); };

/* Line 480 of lalr1.cc  */
#line 298 "parser.tab.cc"
	break;
      case 59: /* "RULELIST" */

/* Line 480 of lalr1.cc  */
#line 63 "parser.yy"
	{ delete (yyvaluep->stringVal); };

/* Line 480 of lalr1.cc  */
#line 307 "parser.tab.cc"
	break;
      case 60: /* "RULE" */

/* Line 480 of lalr1.cc  */
#line 63 "parser.yy"
	{ delete (yyvaluep->stringVal); };

/* Line 480 of lalr1.cc  */
#line 316 "parser.tab.cc"
	break;
      case 61: /* "ELSERULE" */

/* Line 480 of lalr1.cc  */
#line 63 "parser.yy"
	{ delete (yyvaluep->stringVal); };

/* Line 480 of lalr1.cc  */
#line 325 "parser.tab.cc"
	break;
      case 62: /* "RESULT" */

/* Line 480 of lalr1.cc  */
#line 63 "parser.yy"
	{ delete (yyvaluep->stringVal); };

/* Line 480 of lalr1.cc  */
#line 334 "parser.tab.cc"
	break;
      case 63: /* "BOOLEXP" */

/* Line 480 of lalr1.cc  */
#line 63 "parser.yy"
	{ delete (yyvaluep->stringVal); };

/* Line 480 of lalr1.cc  */
#line 343 "parser.tab.cc"
	break;
      case 64: /* "REALRELEXP" */

/* Line 480 of lalr1.cc  */
#line 63 "parser.yy"
	{ delete (yyvaluep->stringVal); };

/* Line 480 of lalr1.cc  */
#line 352 "parser.tab.cc"
	break;
      case 65: /* "REALEXP" */

/* Line 480 of lalr1.cc  */
#line 63 "parser.yy"
	{ delete (yyvaluep->stringVal); };

/* Line 480 of lalr1.cc  */
#line 361 "parser.tab.cc"
	break;
      case 66: /* "IDREF" */

/* Line 480 of lalr1.cc  */
#line 63 "parser.yy"
	{ delete (yyvaluep->stringVal); };

/* Line 480 of lalr1.cc  */
#line 370 "parser.tab.cc"
	break;
      case 71: /* "FUNCTION" */

/* Line 480 of lalr1.cc  */
#line 63 "parser.yy"
	{ delete (yyvaluep->stringVal); };

/* Line 480 of lalr1.cc  */
#line 379 "parser.tab.cc"
	break;
      case 72: /* "CELLREF" */

/* Line 480 of lalr1.cc  */
#line 63 "parser.yy"
	{ delete (yyvaluep->stringVal); };

/* Line 480 of lalr1.cc  */
#line 388 "parser.tab.cc"
	break;
      case 74: /* "PORTNAME" */

/* Line 480 of lalr1.cc  */
#line 63 "parser.yy"
	{ delete (yyvaluep->stringVal); };

/* Line 480 of lalr1.cc  */
#line 397 "parser.tab.cc"
	break;

	default:
	  break;
      }
  }

  void
  Parser::yypop_ (unsigned int n)
  {
    yystate_stack_.pop (n);
    yysemantic_stack_.pop (n);
    yylocation_stack_.pop (n);
  }

#if YYDEBUG
  std::ostream&
  Parser::debug_stream () const
  {
    return *yycdebug_;
  }

  void
  Parser::set_debug_stream (std::ostream& o)
  {
    yycdebug_ = &o;
  }


  Parser::debug_level_type
  Parser::debug_level () const
  {
    return yydebug_;
  }

  void
  Parser::set_debug_level (debug_level_type l)
  {
    yydebug_ = l;
  }
#endif

  int
  Parser::parse ()
  {
    /// Lookahead and lookahead in internal form.
    int yychar = yyempty_;
    int yytoken = 0;

    /* State.  */
    int yyn;
    int yylen = 0;
    int yystate = 0;

    /* Error handling.  */
    int yynerrs_ = 0;
    int yyerrstatus_ = 0;

    /// Semantic value of the lookahead.
    semantic_type yylval;
    /// Location of the lookahead.
    location_type yylloc;
    /// The locations where the error started and ended.
    location_type yyerror_range[2];

    /// $$.
    semantic_type yyval;
    /// @$.
    location_type yyloc;

    int yyresult;

    YYCDEBUG << "Starting parse" << std::endl;


    /* Initialize the stacks.  The initial state will be pushed in
       yynewstate, since the latter expects the semantical and the
       location values to have been already stored, initialize these
       stacks with a primary value.  */
    yystate_stack_ = state_stack_type (0);
    yysemantic_stack_ = semantic_stack_type (0);
    yylocation_stack_ = location_stack_type (0);
    yysemantic_stack_.push (yylval);
    yylocation_stack_.push (yylloc);

    /* New state.  */
  yynewstate:
    yystate_stack_.push (yystate);
    YYCDEBUG << "Entering state " << yystate << std::endl;

    /* Accept?  */
    if (yystate == yyfinal_)
      goto yyacceptlab;

    goto yybackup;

    /* Backup.  */
  yybackup:

    /* Try to take a decision without lookahead.  */
    yyn = yypact_[yystate];
    if (yyn == yypact_ninf_)
      goto yydefault;

    /* Read a lookahead token.  */
    if (yychar == yyempty_)
      {
	YYCDEBUG << "Reading a token: ";
	yychar = yylex (&yylval, &yylloc);
      }


    /* Convert token to internal form.  */
    if (yychar <= yyeof_)
      {
	yychar = yytoken = yyeof_;
	YYCDEBUG << "Now at end of input." << std::endl;
      }
    else
      {
	yytoken = yytranslate_ (yychar);
	YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
      }

    /* If the proper action on seeing token YYTOKEN is to reduce or to
       detect an error, take that action.  */
    yyn += yytoken;
    if (yyn < 0 || yylast_ < yyn || yycheck_[yyn] != yytoken)
      goto yydefault;

    /* Reduce or error.  */
    yyn = yytable_[yyn];
    if (yyn <= 0)
      {
	if (yyn == 0 || yyn == yytable_ninf_)
	goto yyerrlab;
	yyn = -yyn;
	goto yyreduce;
      }

    /* Shift the lookahead token.  */
    YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

    /* Discard the token being shifted.  */
    yychar = yyempty_;

    yysemantic_stack_.push (yylval);
    yylocation_stack_.push (yylloc);

    /* Count tokens shifted since error; after three, turn off error
       status.  */
    if (yyerrstatus_)
      --yyerrstatus_;

    yystate = yyn;
    goto yynewstate;

  /*-----------------------------------------------------------.
  | yydefault -- do the default action for the current state.  |
  `-----------------------------------------------------------*/
  yydefault:
    yyn = yydefact_[yystate];
    if (yyn == 0)
      goto yyerrlab;
    goto yyreduce;

  /*-----------------------------.
  | yyreduce -- Do a reduction.  |
  `-----------------------------*/
  yyreduce:
    yylen = yyr2_[yyn];
    /* If YYLEN is nonzero, implement the default value of the action:
       `$$ = $1'.  Otherwise, use the top of the stack.

       Otherwise, the following line sets YYVAL to garbage.
       This behavior is undocumented and Bison
       users should not rely upon it.  */
    if (yylen)
      yyval = yysemantic_stack_[yylen - 1];
    else
      yyval = yysemantic_stack_[0];

    {
      slice<location_type, location_stack_type> slice (yylocation_stack_, yylen);
      YYLLOC_DEFAULT (yyloc, slice, yylen);
    }
    YY_REDUCE_PRINT (yyn);
    switch (yyn)
      {
	  case 6:

/* Line 678 of lalr1.cc  */
#line 81 "parser.yy"
    { if(driver->model.setSection(driver->params))driver->error << "Duplicated model definition section or wrong parameter.\n"; driver->params.clear(); delete (yysemantic_stack_[(2) - (1)].stringVal); }
    break;

  case 7:

/* Line 678 of lalr1.cc  */
#line 82 "parser.yy"
    { if(driver->model.addRule(*(yysemantic_stack_[(2) - (1)].stringVal), *(yysemantic_stack_[(2) - (2)].stringVal)))driver->error << "Duplicated rule section." << *(yysemantic_stack_[(2) - (1)].stringVal) << endl; delete (yysemantic_stack_[(2) - (1)].stringVal); delete (yysemantic_stack_[(2) - (2)].stringVal); }
    break;

  case 8:

/* Line 678 of lalr1.cc  */
#line 84 "parser.yy"
    { (yyval.stringVal) = (yysemantic_stack_[(3) - (2)].stringVal); }
    break;

  case 11:

/* Line 678 of lalr1.cc  */
#line 89 "parser.yy"
    { driver->params.push_back(Param("zone", *(yysemantic_stack_[(10) - (3)].stringVal), (yysemantic_stack_[(10) - (6)].intVal), (yysemantic_stack_[(10) - (8)].intVal), (yysemantic_stack_[(10) - (6)].intVal), (yysemantic_stack_[(10) - (8)].intVal))); delete (yysemantic_stack_[(10) - (3)].stringVal); }
    break;

  case 12:

/* Line 678 of lalr1.cc  */
#line 90 "parser.yy"
    { driver->params.push_back(Param("zone", *(yysemantic_stack_[(16) - (3)].stringVal), (yysemantic_stack_[(16) - (6)].intVal), (yysemantic_stack_[(16) - (8)].intVal), (yysemantic_stack_[(16) - (12)].intVal), (yysemantic_stack_[(16) - (14)].intVal))); delete (yysemantic_stack_[(16) - (3)].stringVal); }
    break;

  case 13:

/* Line 678 of lalr1.cc  */
#line 91 "parser.yy"
    { driver->params.push_back(Param(*(yysemantic_stack_[(3) - (1)].stringVal), *(yysemantic_stack_[(3) - (3)].stringVal))); delete (yysemantic_stack_[(3) - (1)].stringVal); delete (yysemantic_stack_[(3) - (3)].stringVal); }
    break;

  case 14:

/* Line 678 of lalr1.cc  */
#line 92 "parser.yy"
    { driver->params.push_back(Param(*(yysemantic_stack_[(3) - (1)].stringVal), driver->toString((yysemantic_stack_[(3) - (3)].doubleVal)))); delete (yysemantic_stack_[(3) - (1)].stringVal); }
    break;

  case 15:

/* Line 678 of lalr1.cc  */
#line 93 "parser.yy"
    { driver->params.push_back(Param(*(yysemantic_stack_[(4) - (1)].stringVal), *(yysemantic_stack_[(4) - (4)].stringVal))); delete (yysemantic_stack_[(4) - (1)].stringVal); delete (yysemantic_stack_[(4) - (3)].stringVal); delete (yysemantic_stack_[(4) - (4)].stringVal); }
    break;

  case 16:

/* Line 678 of lalr1.cc  */
#line 94 "parser.yy"
    { driver->params.push_back(Param("dim", (yysemantic_stack_[(7) - (4)].intVal), (yysemantic_stack_[(7) - (6)].intVal))); }
    break;

  case 17:

/* Line 678 of lalr1.cc  */
#line 96 "parser.yy"
    { (yyval.stringVal) = (yysemantic_stack_[(1) - (1)].stringVal); }
    break;

  case 18:

/* Line 678 of lalr1.cc  */
#line 97 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = *(yysemantic_stack_[(2) - (1)].stringVal) + "\n" + *(yysemantic_stack_[(2) - (2)].stringVal); delete (yysemantic_stack_[(2) - (1)].stringVal); delete (yysemantic_stack_[(2) - (2)].stringVal); }
    break;

  case 19:

/* Line 678 of lalr1.cc  */
#line 98 "parser.yy"
    { (yyval.stringVal)=(yysemantic_stack_[(1) - (1)].stringVal); }
    break;

  case 20:

/* Line 678 of lalr1.cc  */
#line 99 "parser.yy"
    { (yyval.stringVal) = (yysemantic_stack_[(2) - (2)].stringVal); }
    break;

  case 21:

/* Line 678 of lalr1.cc  */
#line 101 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = "if("+*(yysemantic_stack_[(7) - (6)].stringVal)+"){ new_event.state="+*(yysemantic_stack_[(7) - (3)].stringVal)+"; new_event.time="+*(yysemantic_stack_[(7) - (4)].stringVal)+"; return new_event; }"; delete (yysemantic_stack_[(7) - (6)].stringVal); delete (yysemantic_stack_[(7) - (3)].stringVal); delete (yysemantic_stack_[(7) - (4)].stringVal); }
    break;

  case 22:

/* Line 678 of lalr1.cc  */
#line 102 "parser.yy"
    { (yyval.stringVal) = NEWSTR; delete (yysemantic_stack_[(7) - (3)].stringVal); delete (yysemantic_stack_[(7) - (4)].stringVal); }
    break;

  case 23:

/* Line 678 of lalr1.cc  */
#line 104 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal)="return "+*(yysemantic_stack_[(1) - (1)].stringVal)+"(current_state, current_time, event_vector, seed);\n"; delete (yysemantic_stack_[(1) - (1)].stringVal); }
    break;

  case 24:

/* Line 678 of lalr1.cc  */
#line 106 "parser.yy"
    { (yyval.stringVal)=NEWSTR; if(isnan((yysemantic_stack_[(1) - (1)].doubleVal)))*(yyval.stringVal) = " NAN "; else *(yyval.stringVal) = driver->toString((yysemantic_stack_[(1) - (1)].doubleVal)); }
    break;

  case 25:

/* Line 678 of lalr1.cc  */
#line 107 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = "("+*(yysemantic_stack_[(3) - (2)].stringVal)+")"; delete (yysemantic_stack_[(3) - (2)].stringVal); }
    break;

  case 26:

/* Line 678 of lalr1.cc  */
#line 109 "parser.yy"
    { (yyval.stringVal)=NEWSTR; if(isnan((yysemantic_stack_[(1) - (1)].doubleVal)))*(yyval.stringVal) = " NAN "; else *(yyval.stringVal) = driver->toString((yysemantic_stack_[(1) - (1)].doubleVal)); }
    break;

  case 27:

/* Line 678 of lalr1.cc  */
#line 110 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = "("+*(yysemantic_stack_[(3) - (2)].stringVal)+")"; delete (yysemantic_stack_[(3) - (2)].stringVal); }
    break;

  case 28:

/* Line 678 of lalr1.cc  */
#line 111 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = *(yysemantic_stack_[(1) - (1)].stringVal); delete (yysemantic_stack_[(1) - (1)].stringVal); }
    break;

  case 29:

/* Line 678 of lalr1.cc  */
#line 112 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = "!("+*(yysemantic_stack_[(2) - (2)].stringVal)+")"; delete (yysemantic_stack_[(2) - (2)].stringVal); }
    break;

  case 30:

/* Line 678 of lalr1.cc  */
#line 113 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = "DEVS_and("+*(yysemantic_stack_[(3) - (1)].stringVal)+","+*(yysemantic_stack_[(3) - (3)].stringVal)+") "; delete (yysemantic_stack_[(3) - (1)].stringVal); delete (yysemantic_stack_[(3) - (3)].stringVal); }
    break;

  case 31:

/* Line 678 of lalr1.cc  */
#line 114 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = "DEVS_or("+*(yysemantic_stack_[(3) - (1)].stringVal)+","+*(yysemantic_stack_[(3) - (3)].stringVal)+") "; delete (yysemantic_stack_[(3) - (1)].stringVal); delete (yysemantic_stack_[(3) - (3)].stringVal); }
    break;

  case 32:

/* Line 678 of lalr1.cc  */
#line 115 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = "DEVS_xor("+*(yysemantic_stack_[(3) - (1)].stringVal)+", "+*(yysemantic_stack_[(3) - (3)].stringVal)+") "; delete (yysemantic_stack_[(3) - (1)].stringVal); delete (yysemantic_stack_[(3) - (3)].stringVal); }
    break;

  case 33:

/* Line 678 of lalr1.cc  */
#line 116 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = "DEVS_imp("+*(yysemantic_stack_[(3) - (1)].stringVal)+", "+*(yysemantic_stack_[(3) - (3)].stringVal)+") "; delete (yysemantic_stack_[(3) - (1)].stringVal); delete (yysemantic_stack_[(3) - (3)].stringVal); }
    break;

  case 34:

/* Line 678 of lalr1.cc  */
#line 117 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = "DEVS_eqv("+*(yysemantic_stack_[(3) - (1)].stringVal)+", "+*(yysemantic_stack_[(3) - (3)].stringVal)+") "; delete (yysemantic_stack_[(3) - (1)].stringVal); delete (yysemantic_stack_[(3) - (3)].stringVal); }
    break;

  case 35:

/* Line 678 of lalr1.cc  */
#line 119 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = *(yysemantic_stack_[(3) - (1)].stringVal)+"!="+*(yysemantic_stack_[(3) - (3)].stringVal); delete (yysemantic_stack_[(3) - (1)].stringVal); delete (yysemantic_stack_[(3) - (3)].stringVal); }
    break;

  case 36:

/* Line 678 of lalr1.cc  */
#line 120 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = *(yysemantic_stack_[(3) - (1)].stringVal)+"=="+*(yysemantic_stack_[(3) - (3)].stringVal); delete (yysemantic_stack_[(3) - (1)].stringVal); delete (yysemantic_stack_[(3) - (3)].stringVal); }
    break;

  case 37:

/* Line 678 of lalr1.cc  */
#line 121 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = *(yysemantic_stack_[(3) - (1)].stringVal)+">"+*(yysemantic_stack_[(3) - (3)].stringVal); delete (yysemantic_stack_[(3) - (1)].stringVal); delete (yysemantic_stack_[(3) - (3)].stringVal); }
    break;

  case 38:

/* Line 678 of lalr1.cc  */
#line 122 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = *(yysemantic_stack_[(3) - (1)].stringVal)+"<"+*(yysemantic_stack_[(3) - (3)].stringVal); delete (yysemantic_stack_[(3) - (1)].stringVal); delete (yysemantic_stack_[(3) - (3)].stringVal); }
    break;

  case 39:

/* Line 678 of lalr1.cc  */
#line 123 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = *(yysemantic_stack_[(3) - (1)].stringVal)+">="+*(yysemantic_stack_[(3) - (3)].stringVal); delete (yysemantic_stack_[(3) - (1)].stringVal); delete (yysemantic_stack_[(3) - (3)].stringVal); }
    break;

  case 40:

/* Line 678 of lalr1.cc  */
#line 124 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = *(yysemantic_stack_[(3) - (1)].stringVal)+"<="+*(yysemantic_stack_[(3) - (3)].stringVal); delete (yysemantic_stack_[(3) - (1)].stringVal); delete (yysemantic_stack_[(3) - (3)].stringVal); }
    break;

  case 41:

/* Line 678 of lalr1.cc  */
#line 125 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = *(yysemantic_stack_[(4) - (1)].stringVal)+"("+*(yysemantic_stack_[(4) - (3)].stringVal)+")"; delete (yysemantic_stack_[(4) - (1)].stringVal); delete (yysemantic_stack_[(4) - (3)].stringVal); }
    break;

  case 42:

/* Line 678 of lalr1.cc  */
#line 127 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = *(yysemantic_stack_[(1) - (1)].stringVal); delete (yysemantic_stack_[(1) - (1)].stringVal); }
    break;

  case 43:

/* Line 678 of lalr1.cc  */
#line 128 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = "("+*(yysemantic_stack_[(3) - (2)].stringVal)+")"; delete (yysemantic_stack_[(3) - (2)].stringVal); }
    break;

  case 44:

/* Line 678 of lalr1.cc  */
#line 129 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = *(yysemantic_stack_[(3) - (1)].stringVal)+"+"+*(yysemantic_stack_[(3) - (3)].stringVal); delete (yysemantic_stack_[(3) - (1)].stringVal); delete (yysemantic_stack_[(3) - (3)].stringVal); }
    break;

  case 45:

/* Line 678 of lalr1.cc  */
#line 130 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = *(yysemantic_stack_[(3) - (1)].stringVal)+"-"+*(yysemantic_stack_[(3) - (3)].stringVal); delete (yysemantic_stack_[(3) - (1)].stringVal); delete (yysemantic_stack_[(3) - (3)].stringVal); }
    break;

  case 46:

/* Line 678 of lalr1.cc  */
#line 131 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = *(yysemantic_stack_[(3) - (1)].stringVal)+"*"+*(yysemantic_stack_[(3) - (3)].stringVal); delete (yysemantic_stack_[(3) - (1)].stringVal); delete (yysemantic_stack_[(3) - (3)].stringVal); }
    break;

  case 47:

/* Line 678 of lalr1.cc  */
#line 132 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = *(yysemantic_stack_[(3) - (1)].stringVal)+"/"+*(yysemantic_stack_[(3) - (3)].stringVal); delete (yysemantic_stack_[(3) - (1)].stringVal); delete (yysemantic_stack_[(3) - (3)].stringVal); }
    break;

  case 48:

/* Line 678 of lalr1.cc  */
#line 134 "parser.yy"
    { (yyval.stringVal) = NEWSTR; if(*(yysemantic_stack_[(1) - (1)].stringVal)=="(0,0)")*(yyval.stringVal) = "current_state"; else *(yyval.stringVal) = "event_vector["+driver->toString(driver->model.section.neighbors_index[*(yysemantic_stack_[(1) - (1)].stringVal)])+"].state"; delete (yysemantic_stack_[(1) - (1)].stringVal); }
    break;

  case 49:

/* Line 678 of lalr1.cc  */
#line 135 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = driver->toString((yysemantic_stack_[(1) - (1)].doubleVal)); }
    break;

  case 50:

/* Line 678 of lalr1.cc  */
#line 136 "parser.yy"
    { (yyval.stringVal) = (yysemantic_stack_[(1) - (1)].stringVal); }
    break;

  case 51:

/* Line 678 of lalr1.cc  */
#line 137 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = "portValue("+*(yysemantic_stack_[(4) - (3)].stringVal)+")"; delete (yysemantic_stack_[(4) - (3)].stringVal); }
    break;

  case 52:

/* Line 678 of lalr1.cc  */
#line 138 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = "send("+*(yysemantic_stack_[(6) - (3)].stringVal)+","+*(yysemantic_stack_[(6) - (5)].stringVal)+")"; delete (yysemantic_stack_[(6) - (3)].stringVal); delete (yysemantic_stack_[(6) - (5)].stringVal); }
    break;

  case 53:

/* Line 678 of lalr1.cc  */
#line 139 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = "DEVS_cellPos("+*(yysemantic_stack_[(4) - (3)].stringVal)+")"; delete (yysemantic_stack_[(4) - (3)].stringVal); }
    break;

  case 54:

/* Line 678 of lalr1.cc  */
#line 141 "parser.yy"
    { (yyval.doubleVal) = (yysemantic_stack_[(1) - (1)].intVal); }
    break;

  case 55:

/* Line 678 of lalr1.cc  */
#line 142 "parser.yy"
    { (yyval.doubleVal) = (yysemantic_stack_[(1) - (1)].doubleVal); }
    break;

  case 56:

/* Line 678 of lalr1.cc  */
#line 143 "parser.yy"
    { (yyval.doubleVal) = driver->constFunc((yysemantic_stack_[(1) - (1)].stringVal)); delete (yysemantic_stack_[(1) - (1)].stringVal); }
    break;

  case 57:

/* Line 678 of lalr1.cc  */
#line 144 "parser.yy"
    { (yyval.doubleVal) = NAN; }
    break;

  case 58:

/* Line 678 of lalr1.cc  */
#line 146 "parser.yy"
    { (yyval.intVal) = (yysemantic_stack_[(1) - (1)].intVal); }
    break;

  case 59:

/* Line 678 of lalr1.cc  */
#line 147 "parser.yy"
    { (yyval.intVal) = (yysemantic_stack_[(2) - (1)].intVal)*(yysemantic_stack_[(2) - (2)].intVal); }
    break;

  case 60:

/* Line 678 of lalr1.cc  */
#line 149 "parser.yy"
    { (yyval.doubleVal) = (yysemantic_stack_[(1) - (1)].doubleVal); }
    break;

  case 61:

/* Line 678 of lalr1.cc  */
#line 150 "parser.yy"
    { (yyval.doubleVal) = (yysemantic_stack_[(2) - (1)].intVal)*(yysemantic_stack_[(2) - (2)].doubleVal); }
    break;

  case 62:

/* Line 678 of lalr1.cc  */
#line 152 "parser.yy"
    { (yyval.intVal) = (yysemantic_stack_[(1) - (1)].intVal); }
    break;

  case 63:

/* Line 678 of lalr1.cc  */
#line 153 "parser.yy"
    { (yyval.intVal) = (yysemantic_stack_[(1) - (1)].intVal); }
    break;

  case 64:

/* Line 678 of lalr1.cc  */
#line 155 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = "DEVS_"+*(yysemantic_stack_[(4) - (1)].stringVal)+"("+*(yysemantic_stack_[(4) - (3)].stringVal)+")"; delete (yysemantic_stack_[(4) - (1)].stringVal); delete (yysemantic_stack_[(4) - (3)].stringVal); }
    break;

  case 65:

/* Line 678 of lalr1.cc  */
#line 156 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = "DEVS_"+*(yysemantic_stack_[(1) - (1)].stringVal)+"()"; delete (yysemantic_stack_[(1) - (1)].stringVal); }
    break;

  case 66:

/* Line 678 of lalr1.cc  */
#line 157 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = "DEVS_"+*(yysemantic_stack_[(6) - (1)].stringVal)+"("+*(yysemantic_stack_[(6) - (3)].stringVal)+","+*(yysemantic_stack_[(6) - (5)].stringVal)+")"; delete (yysemantic_stack_[(6) - (1)].stringVal); delete (yysemantic_stack_[(6) - (3)].stringVal); delete (yysemantic_stack_[(6) - (5)].stringVal); }
    break;

  case 67:

/* Line 678 of lalr1.cc  */
#line 158 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = "DEVS_if("+*(yysemantic_stack_[(8) - (3)].stringVal)+","+*(yysemantic_stack_[(8) - (5)].stringVal)+")"; delete (yysemantic_stack_[(8) - (3)].stringVal); delete (yysemantic_stack_[(8) - (5)].stringVal); delete (yysemantic_stack_[(8) - (7)].stringVal); }
    break;

  case 68:

/* Line 678 of lalr1.cc  */
#line 159 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = "DEVS_ifu("+*(yysemantic_stack_[(10) - (3)].stringVal)+","+*(yysemantic_stack_[(10) - (5)].stringVal)+","+*(yysemantic_stack_[(10) - (7)].stringVal)+","+*(yysemantic_stack_[(10) - (9)].stringVal)+")"; delete (yysemantic_stack_[(10) - (3)].stringVal); delete (yysemantic_stack_[(10) - (5)].stringVal); delete (yysemantic_stack_[(10) - (7)].stringVal); delete (yysemantic_stack_[(10) - (9)].stringVal); }
    break;

  case 69:

/* Line 678 of lalr1.cc  */
#line 161 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = "("+driver->toString((yysemantic_stack_[(5) - (2)].intVal))+","+driver->toString((yysemantic_stack_[(5) - (4)].intVal))+")"; }
    break;

  case 70:

/* Line 678 of lalr1.cc  */
#line 166 "parser.yy"
    { (yyval.doubleVal) = 1.0; }
    break;

  case 71:

/* Line 678 of lalr1.cc  */
#line 167 "parser.yy"
    { (yyval.doubleVal) = 0.0; }
    break;

  case 72:

/* Line 678 of lalr1.cc  */
#line 168 "parser.yy"
    { (yyval.doubleVal) = NAN; }
    break;

  case 73:

/* Line 678 of lalr1.cc  */
#line 170 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = "thisPort"; }
    break;

  case 74:

/* Line 678 of lalr1.cc  */
#line 171 "parser.yy"
    { (yyval.stringVal)=NEWSTR; *(yyval.stringVal) = *(yysemantic_stack_[(1) - (1)].stringVal); delete (yysemantic_stack_[(1) - (1)].stringVal); }
    break;



/* Line 678 of lalr1.cc  */
#line 1059 "parser.tab.cc"
	default:
          break;
      }
    YY_SYMBOL_PRINT ("-> $$ =", yyr1_[yyn], &yyval, &yyloc);

    yypop_ (yylen);
    yylen = 0;
    YY_STACK_PRINT ();

    yysemantic_stack_.push (yyval);
    yylocation_stack_.push (yyloc);

    /* Shift the result of the reduction.  */
    yyn = yyr1_[yyn];
    yystate = yypgoto_[yyn - yyntokens_] + yystate_stack_[0];
    if (0 <= yystate && yystate <= yylast_
	&& yycheck_[yystate] == yystate_stack_[0])
      yystate = yytable_[yystate];
    else
      yystate = yydefgoto_[yyn - yyntokens_];
    goto yynewstate;

  /*------------------------------------.
  | yyerrlab -- here on detecting error |
  `------------------------------------*/
  yyerrlab:
    /* If not already recovering from an error, report this error.  */
    if (!yyerrstatus_)
      {
	++yynerrs_;
	error (yylloc, yysyntax_error_ (yystate, yytoken));
      }

    yyerror_range[0] = yylloc;
    if (yyerrstatus_ == 3)
      {
	/* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

	if (yychar <= yyeof_)
	  {
	  /* Return failure if at end of input.  */
	  if (yychar == yyeof_)
	    YYABORT;
	  }
	else
	  {
	    yydestruct_ ("Error: discarding", yytoken, &yylval, &yylloc);
	    yychar = yyempty_;
	  }
      }

    /* Else will try to reuse lookahead token after shifting the error
       token.  */
    goto yyerrlab1;


  /*---------------------------------------------------.
  | yyerrorlab -- error raised explicitly by YYERROR.  |
  `---------------------------------------------------*/
  yyerrorlab:

    /* Pacify compilers like GCC when the user code never invokes
       YYERROR and the label yyerrorlab therefore never appears in user
       code.  */
    if (false)
      goto yyerrorlab;

    yyerror_range[0] = yylocation_stack_[yylen - 1];
    /* Do not reclaim the symbols of the rule which action triggered
       this YYERROR.  */
    yypop_ (yylen);
    yylen = 0;
    yystate = yystate_stack_[0];
    goto yyerrlab1;

  /*-------------------------------------------------------------.
  | yyerrlab1 -- common code for both syntax error and YYERROR.  |
  `-------------------------------------------------------------*/
  yyerrlab1:
    yyerrstatus_ = 3;	/* Each real token shifted decrements this.  */

    for (;;)
      {
	yyn = yypact_[yystate];
	if (yyn != yypact_ninf_)
	{
	  yyn += yyterror_;
	  if (0 <= yyn && yyn <= yylast_ && yycheck_[yyn] == yyterror_)
	    {
	      yyn = yytable_[yyn];
	      if (0 < yyn)
		break;
	    }
	}

	/* Pop the current state because it cannot handle the error token.  */
	if (yystate_stack_.height () == 1)
	YYABORT;

	yyerror_range[0] = yylocation_stack_[0];
	yydestruct_ ("Error: popping",
		     yystos_[yystate],
		     &yysemantic_stack_[0], &yylocation_stack_[0]);
	yypop_ ();
	yystate = yystate_stack_[0];
	YY_STACK_PRINT ();
      }

    yyerror_range[1] = yylloc;
    // Using YYLLOC is tempting, but would change the location of
    // the lookahead.  YYLOC is available though.
    YYLLOC_DEFAULT (yyloc, (yyerror_range - 1), 2);
    yysemantic_stack_.push (yylval);
    yylocation_stack_.push (yyloc);

    /* Shift the error token.  */
    YY_SYMBOL_PRINT ("Shifting", yystos_[yyn],
		     &yysemantic_stack_[0], &yylocation_stack_[0]);

    yystate = yyn;
    goto yynewstate;

    /* Accept.  */
  yyacceptlab:
    yyresult = 0;
    goto yyreturn;

    /* Abort.  */
  yyabortlab:
    yyresult = 1;
    goto yyreturn;

  yyreturn:
    if (yychar != yyempty_)
      yydestruct_ ("Cleanup: discarding lookahead", yytoken, &yylval, &yylloc);

    /* Do not reclaim the symbols of the rule which action triggered
       this YYABORT or YYACCEPT.  */
    yypop_ (yylen);
    while (yystate_stack_.height () != 1)
      {
	yydestruct_ ("Cleanup: popping",
		   yystos_[yystate_stack_[0]],
		   &yysemantic_stack_[0],
		   &yylocation_stack_[0]);
	yypop_ ();
      }

    return yyresult;
  }

  // Generate an error message.
  std::string
  Parser::yysyntax_error_ (int yystate, int tok)
  {
    std::string res;
    YYUSE (yystate);
#if YYERROR_VERBOSE
    int yyn = yypact_[yystate];
    if (yypact_ninf_ < yyn && yyn <= yylast_)
      {
	/* Start YYX at -YYN if negative to avoid negative indexes in
	   YYCHECK.  */
	int yyxbegin = yyn < 0 ? -yyn : 0;

	/* Stay within bounds of both yycheck and yytname.  */
	int yychecklim = yylast_ - yyn + 1;
	int yyxend = yychecklim < yyntokens_ ? yychecklim : yyntokens_;
	int count = 0;
	for (int x = yyxbegin; x < yyxend; ++x)
	  if (yycheck_[x + yyn] == x && x != yyterror_)
	    ++count;

	// FIXME: This method of building the message is not compatible
	// with internationalization.  It should work like yacc.c does it.
	// That is, first build a string that looks like this:
	// "syntax error, unexpected %s or %s or %s"
	// Then, invoke YY_ on this string.
	// Finally, use the string as a format to output
	// yytname_[tok], etc.
	// Until this gets fixed, this message appears in English only.
	res = "syntax error, unexpected ";
	res += yytnamerr_ (yytname_[tok]);
	if (count < 5)
	  {
	    count = 0;
	    for (int x = yyxbegin; x < yyxend; ++x)
	      if (yycheck_[x + yyn] == x && x != yyterror_)
		{
		  res += (!count++) ? ", expecting " : " or ";
		  res += yytnamerr_ (yytname_[x]);
		}
	  }
      }
    else
#endif
      res = YY_("syntax error");
    return res;
  }


  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
  const signed char Parser::yypact_ninf_ = -60;
  const short int
  Parser::yypact_[] =
  {
         4,   -17,    36,    34,    44,   -60,     6,   -60,    34,    33,
      30,    58,   -60,     7,    35,    38,    40,    41,   -60,    30,
     -60,     3,   -60,    41,   -60,   -60,   -60,   -60,   222,    39,
      87,   117,   -60,   -60,    90,   -60,   -60,   -60,   -60,   -60,
     -60,   222,   -60,   -60,   -60,   -23,    89,   -18,    92,   -60,
      90,    93,   102,   104,   106,   108,   -60,   111,   118,    12,
     -60,   -60,   -60,   -60,   123,   -60,   -60,   126,    54,    84,
     -18,   -60,    68,    96,   -30,   -30,    90,   177,   177,    90,
      90,   -60,    90,    90,    90,    90,   141,   -18,   -18,    96,
     -60,   -18,   -60,   -60,   127,   107,   182,   177,   177,   -60,
     -60,   -60,   137,   280,   -60,   240,   -60,   285,   227,   281,
     -60,   -60,   -13,   -13,   140,   331,   110,   145,   148,   -60,
      90,   -60,   300,   196,   -60,    90,   177,   177,   177,   177,
     177,    90,    90,    90,    90,    90,    90,    90,    90,   -60,
      90,   -60,   -60,   -18,   -60,   -60,   229,   -60,   231,   -60,
     -60,   -60,   -60,   -60,   283,    65,    65,    65,    65,    65,
      65,   286,   233,   149,   -60,   -60,    90,    90,   -60,    -4,
     235,   288,   -60,   157,   -60,    90,   -18,   238,   124,   -60,
     -18,   168,   155,   -60
  };

  /* YYDEFACT[S] -- default rule to reduce with in state S when YYTABLE
     doesn't specify something else to do.  Zero means the default is an
     error.  */
  const unsigned char
  Parser::yydefact_[] =
  {
         0,     0,     0,     4,     0,     1,     0,     2,     4,     0,
       9,     0,     5,     0,     0,     0,     0,    23,     6,     9,
       7,     0,    19,     0,     3,     8,    23,    20,     0,     0,
       0,     0,    10,    18,     0,    57,    56,    58,    62,    63,
      60,     0,    24,    54,    55,     0,     0,     0,    13,    14,
       0,     0,     0,     0,     0,     0,    65,     0,     0,     0,
      42,    49,    50,    48,     0,    59,    61,     0,     0,     0,
       0,    15,     0,    54,     0,     0,     0,     0,     0,     0,
       0,    25,     0,     0,     0,     0,     0,     0,     0,     0,
      43,     0,    73,    74,     0,     0,     0,     0,     0,    70,
      71,    72,     0,     0,    28,     0,    26,     0,     0,     0,
      46,    47,    44,    45,     0,     0,     0,     0,     0,    51,
       0,    53,     0,     0,    29,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    64,
       0,    22,    21,     0,    16,    69,     0,    27,     0,    30,
      31,    32,    33,    34,     0,    35,    36,    37,    38,    39,
      40,     0,     0,     0,    52,    41,     0,     0,    66,     0,
       0,     0,    11,     0,    67,     0,     0,     0,     0,    68,
       0,     0,     0,    12
  };

  /* YYPGOTO[NTERM-NUM].  */
  const short int
  Parser::yypgoto_[] =
  {
       -60,   -60,   -60,   171,   -60,   -60,     5,   -60,    -8,   -60,
     -60,   142,   -59,   -60,   -34,   -60,   -19,   -33,   -60,   -26,
     -60,   133,   -60,   109
  };

  /* YYDEFGOTO[NTERM-NUM].  */
  const signed char
  Parser::yydefgoto_[] =
  {
        -1,     2,     3,     7,     8,     9,    18,    19,    20,    21,
      22,    41,   103,   104,   105,    60,    61,    43,    44,    45,
      62,    63,   106,    94
  };

  /* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule which
     number is the opposite.  If zero, do what YYDEFACT says.  */
  const signed char Parser::yytable_ninf_ = -18;
  const short int
  Parser::yytable_[] =
  {
        59,    92,   172,   -17,    13,    27,    82,    83,    13,    42,
     -17,     1,    49,    33,    68,    24,    72,    73,    81,   107,
      93,    69,    42,    65,    32,     4,    66,   115,    37,    38,
      39,    82,    83,    -9,    13,   173,     5,    89,   122,   124,
      -9,     6,    96,    14,    69,   108,   109,    14,   110,   111,
     112,   113,    10,    26,   116,   117,    11,    26,   118,    84,
      85,    69,    69,   123,    73,    69,    25,   149,   150,   151,
     152,   153,    90,    14,    15,    16,    28,    15,    16,    29,
      23,    30,    31,    17,    82,    83,   146,    82,    83,    46,
      47,   148,    88,    50,    67,    70,    74,   154,   155,   156,
     157,   158,   159,   160,   161,    75,   162,    76,    35,    77,
     163,    78,    84,    85,    79,    84,    85,    69,    51,    52,
      53,    80,    54,    55,    36,    56,    57,    58,    86,    87,
      65,   119,   170,   171,    91,    35,    37,    38,    39,    40,
     125,   177,   114,   178,    97,   120,   141,   181,   143,   144,
      69,    36,   145,   169,    69,    98,    99,   100,   101,    35,
     176,   183,   180,    37,    38,    39,    40,    48,   102,    51,
      52,    53,   182,    54,    55,    36,    56,    57,    58,    12,
      97,    71,     0,    64,    95,     0,   121,    37,    38,    39,
      40,    98,    99,   100,   101,    35,     0,     0,     0,     0,
      90,    82,    83,     0,   102,    51,    52,    53,     0,    54,
      55,    36,    56,    57,    58,    82,    83,   132,   133,   134,
     135,   136,   137,    37,    38,    39,    40,    34,     0,    84,
      85,   139,     0,   164,     0,   165,     0,   168,     0,   174,
      35,     0,   179,    84,    85,     0,    82,    83,    82,    83,
      82,    83,    82,    83,    82,    83,    36,    82,    83,    82,
      83,   132,   133,   134,   135,   136,   137,     0,    37,    38,
      39,    40,     0,     0,    84,    85,    84,    85,    84,    85,
      84,    85,    84,    85,     0,    84,    85,    84,    85,   126,
     127,   128,   129,   130,   126,   127,   128,   129,   130,     0,
      82,    83,    82,    83,   147,    82,    83,    82,    83,   126,
     127,   128,   129,   130,     0,     0,     0,     0,   131,   140,
       0,   166,     0,   138,   167,     0,   175,     0,    84,    85,
      84,    85,     0,    84,    85,    84,    85,   142,     0,     0,
     126,   127,   128,   129,   130
  };

  /* YYCHECK.  */
  const short int
  Parser::yycheck_[] =
  {
        34,    31,     6,     0,     1,    13,    19,    20,     1,    28,
       7,     7,    31,    21,    47,    10,    50,    50,     6,    78,
      50,    47,    41,    46,    19,    42,    49,    86,    46,    47,
      48,    19,    20,     0,     1,    39,     0,    70,    97,    98,
       7,     7,    76,    40,    70,    79,    80,    40,    82,    83,
      84,    85,     8,    50,    87,    88,    50,    50,    91,    47,
      48,    87,    88,    97,    97,    91,     8,   126,   127,   128,
     129,   130,     4,    40,    44,    45,    41,    44,    45,    41,
      50,    41,    41,    50,    19,    20,   120,    19,    20,    50,
       3,   125,    38,     3,     5,     3,     3,   131,   132,   133,
     134,   135,   136,   137,   138,     3,   140,     3,    18,     3,
     143,     3,    47,    48,     3,    47,    48,   143,    28,    29,
      30,     3,    32,    33,    34,    35,    36,    37,     5,     3,
      46,     4,   166,   167,    38,    18,    46,    47,    48,    49,
       3,   175,     1,   176,     3,    38,     6,   180,    38,     4,
     176,    34,     4,     4,   180,    14,    15,    16,    17,    18,
       3,     6,    38,    46,    47,    48,    49,    50,    27,    28,
      29,    30,     4,    32,    33,    34,    35,    36,    37,     8,
       3,    48,    -1,    41,    75,    -1,     4,    46,    47,    48,
      49,    14,    15,    16,    17,    18,    -1,    -1,    -1,    -1,
       4,    19,    20,    -1,    27,    28,    29,    30,    -1,    32,
      33,    34,    35,    36,    37,    19,    20,    21,    22,    23,
      24,    25,    26,    46,    47,    48,    49,     5,    -1,    47,
      48,     4,    -1,     4,    -1,     4,    -1,     4,    -1,     4,
      18,    -1,     4,    47,    48,    -1,    19,    20,    19,    20,
      19,    20,    19,    20,    19,    20,    34,    19,    20,    19,
      20,    21,    22,    23,    24,    25,    26,    -1,    46,    47,
      48,    49,    -1,    -1,    47,    48,    47,    48,    47,    48,
      47,    48,    47,    48,    -1,    47,    48,    47,    48,     9,
      10,    11,    12,    13,     9,    10,    11,    12,    13,    -1,
      19,    20,    19,    20,     4,    19,    20,    19,    20,     9,
      10,    11,    12,    13,    -1,    -1,    -1,    -1,    38,    38,
      -1,    38,    -1,    38,    38,    -1,    38,    -1,    47,    48,
      47,    48,    -1,    47,    48,    47,    48,     6,    -1,    -1,
       9,    10,    11,    12,    13
  };

  /* STOS_[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
  const unsigned char
  Parser::yystos_[] =
  {
         0,     7,    52,    53,    42,     0,     7,    54,    55,    56,
       8,    50,    54,     1,    40,    44,    45,    50,    57,    58,
      59,    60,    61,    50,    57,     8,    50,    59,    41,    41,
      41,    41,    57,    59,     5,    18,    34,    46,    47,    48,
      49,    62,    67,    68,    69,    70,    50,     3,    50,    67,
       3,    28,    29,    30,    32,    33,    35,    36,    37,    65,
      66,    67,    71,    72,    62,    46,    49,     5,    68,    70,
       3,    72,    65,    68,     3,     3,     3,     3,     3,     3,
       3,     6,    19,    20,    47,    48,     5,     3,    38,    68,
       4,    38,    31,    50,    74,    74,    65,     3,    14,    15,
      16,    17,    27,    63,    64,    65,    73,    63,    65,    65,
      65,    65,    65,    65,     1,    63,    68,    68,    68,     4,
      38,     4,    63,    65,    63,     3,     9,    10,    11,    12,
      13,    38,    21,    22,    23,    24,    25,    26,    38,     4,
      38,     6,     6,    38,     4,     4,    65,     4,    65,    63,
      63,    63,    63,    63,    65,    65,    65,    65,    65,    65,
      65,    65,    65,    68,     4,     4,    38,    38,     4,     4,
      65,    65,     6,    39,     4,    38,     3,    65,    68,     4,
      38,    68,     4,     6
  };

#if YYDEBUG
  /* TOKEN_NUMBER_[YYLEX-NUM] -- Internal symbol number corresponding
     to YYLEX-NUM.  */
  const unsigned short int
  Parser::yytoken_number_[] =
  {
         0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305
  };
#endif

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
  const unsigned char
  Parser::yyr1_[] =
  {
         0,    51,    52,    53,    54,    54,    55,    55,    56,    57,
      57,    58,    58,    58,    58,    58,    58,    59,    59,    59,
      59,    60,    60,    61,    62,    62,    63,    63,    63,    63,
      63,    63,    63,    63,    63,    64,    64,    64,    64,    64,
      64,    64,    65,    65,    65,    65,    65,    65,    66,    66,
      66,    66,    66,    66,    67,    67,    67,    67,    68,    68,
      69,    69,    70,    70,    71,    71,    71,    71,    71,    72,
      73,    73,    73,    74,    74
  };

  /* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
  const unsigned char
  Parser::yyr2_[] =
  {
         0,     2,     2,     4,     0,     2,     2,     2,     3,     0,
       2,    10,    16,     3,     3,     4,     7,     1,     2,     1,
       2,     7,     7,     1,     1,     3,     1,     3,     1,     2,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     4,     1,     3,     3,     3,     3,     3,     1,     1,
       1,     4,     6,     4,     1,     1,     1,     1,     1,     2,
       1,     2,     1,     1,     4,     1,     6,     8,    10,     5,
       1,     1,     1,     1,     1
  };

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
  /* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
     First, the terminals, then, starting at \a yyntokens_, nonterminals.  */
  const char*
  const Parser::yytname_[] =
  {
    "\"end of file\"", "error", "$undefined", "\"(\"", "\")\"", "\"{\"",
  "\"}\"", "\"[\"", "\"]\"", "\"and\"", "\"or\"", "\"xor\"", "\"imp\"",
  "\"eqv\"", "\"not\"", "\"t\"", "\"f\"", "\"?b\"", "\"?c\"", "\"*\"",
  "\"/\"", "\"!=\"", "\"=\"", "\">\"", "\"<\"", "\">=\"", "\"<=\"",
  "COND_REAL_FUNC", "\"portValue\"", "\"send\"", "\"cellPos\"",
  "\"thisPort\"", "\"if\"", "\"ifu\"", "\"const function\"",
  "\"without parameter function\"", "\"unary function\"",
  "\"binary function\"", "\",\"", "\"..\"", "\"rule\"", "\":\"", "\"top\"",
  "\"else\"", "\"zone\"", "\"dim\"", "UINT", "\"+\"", "\"-\"", "UREAL",
  "STRING", "$accept", "START", "TOPSECTION", "SECTIONTUPLA", "SECTION",
  "SECNAME", "PARAMTUPLA", "PARAM", "RULELIST", "RULE", "ELSERULE",
  "RESULT", "BOOLEXP", "REALRELEXP", "REALEXP", "IDREF", "CONSTANT", "INT",
  "REAL", "SIGN", "FUNCTION", "CELLREF", "BOOL", "PORTNAME", 0
  };
#endif

#if YYDEBUG
  /* YYRHS -- A `-1'-separated list of the rules' RHS.  */
  const Parser::rhs_number_type
  Parser::yyrhs_[] =
  {
        52,     0,    -1,    53,    54,    -1,     7,    42,     8,    57,
      -1,    -1,    55,    54,    -1,    56,    57,    -1,    56,    59,
      -1,     7,    50,     8,    -1,    -1,    58,    57,    -1,    44,
      41,    50,     5,     3,    68,    38,    68,     4,     6,    -1,
      44,    41,    50,     5,     3,    68,    38,    68,     4,    39,
       3,    68,    38,    68,     4,     6,    -1,    50,    41,    50,
      -1,    50,    41,    67,    -1,    50,    41,    50,    72,    -1,
      45,    41,     3,    68,    38,    68,     4,    -1,    60,    -1,
      60,    59,    -1,    61,    -1,     1,    59,    -1,    40,    41,
      62,    62,     5,    63,     6,    -1,    40,    41,    62,    62,
       5,     1,     6,    -1,    50,    -1,    67,    -1,     5,    65,
       6,    -1,    73,    -1,     3,    63,     4,    -1,    64,    -1,
      14,    63,    -1,    63,     9,    63,    -1,    63,    10,    63,
      -1,    63,    11,    63,    -1,    63,    12,    63,    -1,    63,
      13,    63,    -1,    65,    21,    65,    -1,    65,    22,    65,
      -1,    65,    23,    65,    -1,    65,    24,    65,    -1,    65,
      25,    65,    -1,    65,    26,    65,    -1,    27,     3,    65,
       4,    -1,    66,    -1,     3,    65,     4,    -1,    65,    47,
      65,    -1,    65,    48,    65,    -1,    65,    19,    65,    -1,
      65,    20,    65,    -1,    72,    -1,    67,    -1,    71,    -1,
      28,     3,    74,     4,    -1,    29,     3,    74,    38,    65,
       4,    -1,    30,     3,    65,     4,    -1,    68,    -1,    69,
      -1,    34,    -1,    18,    -1,    46,    -1,    70,    46,    -1,
      49,    -1,    70,    49,    -1,    47,    -1,    48,    -1,    36,
       3,    65,     4,    -1,    35,    -1,    37,     3,    65,    38,
      65,     4,    -1,    32,     3,    63,    38,    65,    38,    65,
       4,    -1,    33,     3,    63,    38,    65,    38,    65,    38,
      65,     4,    -1,     3,    68,    38,    68,     4,    -1,    15,
      -1,    16,    -1,    17,    -1,    31,    -1,    50,    -1
  };

  /* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
     YYRHS.  */
  const unsigned short int
  Parser::yyprhs_[] =
  {
         0,     0,     3,     6,    11,    12,    15,    18,    21,    25,
      26,    29,    40,    57,    61,    65,    70,    78,    80,    83,
      85,    88,    96,   104,   106,   108,   112,   114,   118,   120,
     123,   127,   131,   135,   139,   143,   147,   151,   155,   159,
     163,   167,   172,   174,   178,   182,   186,   190,   194,   196,
     198,   200,   205,   212,   217,   219,   221,   223,   225,   227,
     230,   232,   235,   237,   239,   244,   246,   253,   262,   273,
     279,   281,   283,   285,   287
  };

  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
  const unsigned char
  Parser::yyrline_[] =
  {
         0,    74,    74,    76,    78,    79,    81,    82,    84,    86,
      87,    89,    90,    91,    92,    93,    94,    96,    97,    98,
      99,   101,   102,   104,   106,   107,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   119,   120,   121,   122,   123,
     124,   125,   127,   128,   129,   130,   131,   132,   134,   135,
     136,   137,   138,   139,   141,   142,   143,   144,   146,   147,
     149,   150,   152,   153,   155,   156,   157,   158,   159,   161,
     166,   167,   168,   170,   171
  };

  // Print the state stack on the debug stream.
  void
  Parser::yystack_print_ ()
  {
    *yycdebug_ << "Stack now";
    for (state_stack_type::const_iterator i = yystate_stack_.begin ();
	 i != yystate_stack_.end (); ++i)
      *yycdebug_ << ' ' << *i;
    *yycdebug_ << std::endl;
  }

  // Report on the debug stream that the rule \a yyrule is going to be reduced.
  void
  Parser::yy_reduce_print_ (int yyrule)
  {
    unsigned int yylno = yyrline_[yyrule];
    int yynrhs = yyr2_[yyrule];
    /* Print the symbols being reduced, and their result.  */
    *yycdebug_ << "Reducing stack by rule " << yyrule - 1
	       << " (line " << yylno << "):" << std::endl;
    /* The symbols being reduced.  */
    for (int yyi = 0; yyi < yynrhs; yyi++)
      YY_SYMBOL_PRINT ("   $" << yyi + 1 << " =",
		       yyrhs_[yyprhs_[yyrule] + yyi],
		       &(yysemantic_stack_[(yynrhs) - (yyi + 1)]),
		       &(yylocation_stack_[(yynrhs) - (yyi + 1)]));
  }
#endif // YYDEBUG

  /* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
  Parser::token_number_type
  Parser::yytranslate_ (int t)
  {
    static
    const token_number_type
    translate_table[] =
    {
           0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50
    };
    if ((unsigned int) t <= yyuser_token_number_max_)
      return translate_table[t];
    else
      return yyundef_token_;
  }

  const int Parser::yyeof_ = 0;
  const int Parser::yylast_ = 344;
  const int Parser::yynnts_ = 24;
  const int Parser::yyempty_ = -2;
  const int Parser::yyfinal_ = 5;
  const int Parser::yyterror_ = 1;
  const int Parser::yyerrcode_ = 256;
  const int Parser::yyntokens_ = 51;

  const unsigned int Parser::yyuser_token_number_max_ = 305;
  const Parser::token_number_type Parser::yyundef_token_ = 2;


/* Line 1054 of lalr1.cc  */
#line 1 "[Bison:b4_percent_define_default]"

} // DEVS

/* Line 1054 of lalr1.cc  */
#line 1669 "parser.tab.cc"


/* Line 1056 of lalr1.cc  */
#line 173 "parser.yy"
 // dodatocny kod

void DEVS::Parser::error(const Parser::location_type& l, const std::string& m)
{
    driver->error << "Error " << l.begin.line << ":" << l.begin.column << "-" << l.end.column << " " << m << std::endl;
}

