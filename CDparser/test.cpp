/************************************************************************** 
*   Cellula, is cellular automata simulator accelerated on GPU.           *
*   Copyright (C) 2011  Dušan Poizl                                       *
*                                                                         *
*   This program is free software: you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation, either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
***************************************************************************/

#include "CDparser.h"
#include <iostream>

int main()
{
    ifstream fr;
    fr.open("vstup", ios::in);
    fr.seekg(0, ios::end);
    streamsize len = fr.tellg();
    char *s = new char[len+1];
    fr.seekg(0, ios::beg);
    fr.read(s, len);
    s[len]='\0';
    string source(s);
    cout << len << " " << source << endl;
    delete [] s;
    DEVS::CDparser parser(source);
    parser.parse();
    return 0;
}
