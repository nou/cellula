/**************************************************************************
*   Cellula, is cellular automata simulator accelerated on GPU.           *
*   Copyright (C) 2011  Dušan Poizl                                       *
*                                                                         *
*   This program is free software: you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation, either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
***************************************************************************/

#include <math.h>
#include "CDparser.h"

namespace DEVS
{
    double CDparser::constFunc(const std::string *name)
    {
        if(*name == "pi")return 3.14159265358979323846;
        else if(*name == "e")return 2.7182818284590452353;
        else if(*name == "inf")return INFINITY;
        else if(*name == "grav")return 6.67256e-11;
        else if(*name == "accel")return 9.80665;
        else if(*name == "light")return 299792458;
        else if(*name == "planck")return 6.6260755e-34;
        else if(*name == "avogadro")return 6.0221367e23;
        else if(*name == "faraday")return 96485.309;
        else if(*name == "rydberg")return 10973731534;
        else if(*name == "euler_gamma")return 0.5772156649015;
        else if(*name == "bohr_radius")return 0.529177249e-10;
        else if(*name == "boltzmann")return 1.380658e-23;
        else if(*name == "bohr_magneton")return 9.2740154e-24;
        else if(*name == "golden")return (1+sqrt(5))/2;
        else if(*name == "catalan")return 0.9159655941772;
        else if(*name == "amu")return 1.6605402;
        else if(*name == "electron_charge")return 1.60217733e-19;
        else if(*name == "ideal_gas")return 22.4141;
        else if(*name == "stefan_boltzmann")return 5.67051e-8;
        else if(*name == "proton_mass")return 1.67262163783e-27;
        else if(*name == "electron_mass")return 9.1093821545e-31;
        else if(*name == "neutron_mass")return 1.6749272928e-27;
        else if(*name == "pem")return 1836.152701;
        else cerr << "Unknown const func " << name << endl;
        return 0;
    }

    int ModelDefinition::addRule(string &name, string &rule)
    {
        if(rules.count(name))return -1;
        rules[name] = "_event "+name+"(_state current_state, _time current_time, _event *event_vector, _seed *seed){_event new_event;\n"+rule+"}\n";
        transfer_func_declaration += "_event "+name+"(_state current_state, _time current_time, _event *event_vector, _seed *seed);\n";
        return 0;
    }
    int ModelDefinition::setSection(vector<Param> &params)
    {
        if(section_set)return -1;
        section_set = true;
        zones = "if(0);\n";
        for(vector<Param>::iterator i = params.begin(); i != params.end(); i++)
        {
            if(i->name == "border")
            {
                if(i->value == "wrapped")section.wrapped_border = true;
            }
            else if(i->name == "initialvalue")
                section.initialvalue = atof(i->value.c_str());
            else if(i->name == "neighbors")
            {
                if(section.neighbors.count(i->value)==0 && i->value != "(0,0)")
                {
                    section.neighbors_index[i->value] = section.neighbors.size();
                    section.neighbors.insert(i->value);
                }
            }
            else if(i->name == "localtransition")
                section.local_transition = i->value;
            else if(i->name == "zone")
            {
                stringstream ss;
                ss << "else if(x>=" << i->startx << "&& x<=" << i->endx << "&& y>=" << i->starty << " && y<=" << i->endy << ")" << i->value << "(current_state, current_time, event_vector, seed);" << endl;
                zones += ss.str();
            }
            else if(i->name == "dim")
            {
                section.width = i->startx;
                section.height = i->starty;
            }
            else if(i->name == "width")
            {
                section.width = atoi(i->value.c_str());
            }
            else if(i->name == "height")
            {
                section.height = atoi(i->value.c_str());
            }
            else if(i->name == "timewindow")
            {
                section.timewindow = atof(i->value.c_str());
            }
        }
        return 0;
    }
    Section::Section()
    {
        wrapped_border = false;
        initialvalue = NAN;
        width = height = 0;
    }
}
