#ifndef _SCANNER_HH_NOU_
#define _SCANNER_HH_NOU_

#ifndef YY_DECL

#define	YY_DECL						    \
    DEVS::Parser::token_type		    \
    DEVS::Scanner::lex(				    \
	DEVS::Parser::semantic_type *yylval,\
	DEVS::Parser::location_type *yylloc)
#endif

#include <istream>
#include <ostream>
#include "parser.tab.hh"

#ifndef __FLEX_LEXER_H
#define yyFlexLexer ScannerFlexLexer
#include <FlexLexer.h>
#undef yyFlexLexer
#endif

namespace DEVS
{
    class Scanner : ScannerFlexLexer
    {
        public:
        Scanner(std::istream *in = 0, std::ostream *out = 0) : ScannerFlexLexer(in, out)
        {
        }
        virtual Parser::token_type lex(Parser::semantic_type *yylval, Parser::location_type *yylloc);
    };
};

#endif // _SCANNER_HH_NOU_
