/**************************************************************************
*   Cellula, is cellular automata simulator accelerated on GPU.           *
*   Copyright (C) 2011  Dušan Poizl                                       *
*                                                                         *
*   This program is free software: you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation, either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
***************************************************************************/

#ifndef _MODEL_H_NOU_
#define _MODEL_H_NOU_

#include <string>
#include <vector>
#include <CL/cl.h>
#include "cellspace.h"

using namespace std;

class QString;

class Machine;
/**
Abstraktna trieda poskytujuca jednotne rozhranie na pracu s roznymi modelmi.
Trieda ktora bude dedit musi implementovat tieto metody:
void run(CellSpace *pattern, int which, int iteration = 1);
int getCellSize();
void loadSource();
void renderToTexture(CellSpace *pattern, int which, cl_mem tex);
*/
class Model
{
    protected:
    cl_program program;
    vector<CellSpace*> patterns;
    public:
    Model(Machine *parent);
    virtual ~Model();
    Machine* getParent(){ return parent; }
    void addChild(CellSpace *child);
    void removeChild(CellSpace *child);
    /** Tato metoda by mala nastavit parametre pre kernel a následne zavolat parent->run()
    @param pattern ukazovatel na potomka obsahujuci stav modelu
    @param which vypocet bude prebiehat iterativne kedy sa zo stareho stavu vyratava novy. tento
        parameter urcuje ktory je ktory
    @param iteration kolko krat sa ma spustit kernel. pocas nich musi metoda preklapat which medzi 0 a 1
    */
    virtual void run(CellSpace *pattern, int which, int iteration = 1) = 0;
    /**
    @return pocet bajtov ktore zabera reprezentacia zdvojenie jednej bunky.
    */
    virtual int getCellSize() = 0;
    /**
    v tejto metode by sa mal vytvorit zdrojovy kod kernel a nasledne zavolat this->build() pre jeho
    skompletovanie
    @return vracia 0 ak sa podarilo nacitat zdrojovy kod uspesne inak -1
    */
    virtual int loadSource() = 0;
    /**
    Metoda by mala nastavit parametre kernel pre vykreslenie do buffera.
    @param pattern potom z ktoreho sa ma kreslit
    @param which ktory z dvojice sa ma vykreslit
    @param tex buffer do ktoreho sa bude kreslit
    */
    virtual void renderToTexture(CellSpace *pattern, int which, cl_mem tex) = 0;
    /**
    Metoda sparsuje definovany model.
    @param model_definition textovy retazec obsahujuci definiciu modelu.
    @return vracia nulu pri usepechu a zaporne cislo pri chybe.
    */
    virtual int compile(string model_definition) = 0;
    /**
    Vracia chybovu hlasku ak sa nepodari sparsovat definiciu modelu.
    @return string s chybovou hlaskou
    */
    virtual string getError() = 0;
    /**
    Vracia cestu k kernelom vo forme "-I /path/to/program/kernels"
    @return cesta k kernelom
    */
    string getKernelInclude();
    protected:
    cl_int build(string source, string options);
    cl_kernel kernel, render_kernel;
    Machine *parent;
};
/**
Trieda implemtujuca synchronne celularne automaty.
Su mozne dva druhy susedstva Neumanovo teda styria susedia hraniciaci s hranou alebo osem susedov
teda vsetky bunky ktore sa dotykaju strednej bunky.
Vstupnym formatom modelu je prechodovy strom vo forme matice s rozmermy pocet stavov krat pocet uzlov.
Kazdy riadok reprezentuje jeden uzol stromu a jednotlive stlpce nasledujuci uzol ktorym sa ma pokracovat.
*/
class SynchronousModel : public Model
{
    public:
    typedef int CellType;
    SynchronousModel(Machine *parent, string model_definition);
    int getCellSize(){ return sizeof(CellType)*2; }
    int loadSource();
    void run(CellSpace *pattern, int which, int iteration = 1);
    void renderToTexture(CellSpace *pattern, int which, cl_mem tex);
    int compile(string model_definition);
    string getError();
    /**
    nacita zo suboru ulozeny stav vo formate Golly RLE
    @param fr vstupny prud dat z ktorych sa bude citat. uz ma byt otovreny
    @param w vracia sa sirka nacitanych dat.
    @param h vracia vyska nacitanych dat
    @param vracia pointer na data. tie treba dalsedne uvolnit s delete
    */
    static int loadRLE(istream &fr, size_t *w, size_t *h, CellType **data);
    private:
    string nodes;
    int num_states, num_neighbors, num_nodes;
    string error_message;
};

class AsynchronousModel : public Model
{
    private:
    int queue_len,neighbor;
    cl_kernel init_kernel,intermediate_kernel;
    string error_message;
    string neighbors, transfer_functions;
    float initvalue, timewindow;
    int width, height;
    public:
    AsynchronousModel(Machine* parent, string model_definition);
    ~AsynchronousModel();
    int getCellSize();
    int loadSource();
    void run(CellSpace *pattern, int which, int iteration = 1);
    void renderToTexture(CellSpace *pattern, int which, cl_mem tex);
    int compile(string model_definition);
    string getError();
    void initialize(CellSpace *pattern, float *data = 0);
    int getWidth(){ return width; }
    int getHeight(){ return height; }
    int loadVAL(QString &input, CellSpace *cellspace);
};

#endif // _MODEL_H_NOU_
