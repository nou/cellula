CC=g++
CFLAGS=-c -Wall -O3 -s
LDFLAGS= -lOpenCL -lQtGui -lQtCore -lGL -lQtOpenGL -llua5.1 -ltolua++5.1 -lCDparser -L./CDparser
INCLUDE= -I/usr/include/qt4 -I/usr/include/qt4/QtCore -I/usr/include/qt4/QtGui -I/usr/include/lua5.1 
SOURCES=asynchronousmodel.cpp \
	cellspace.cpp \
	cellulamainwindow.cpp \
	clutility.cpp \
	clvariant.cpp \
	glwidget.cpp \
	helpview.cpp \
	lex.TreeScanner.cc \
	luaconsole.cpp \
	lua/luawrapper.cpp \
	lua/luawrapperbind.cpp \
	machine.cpp \
	main.cpp \
	moc_cellulamainwindow.cpp \
	moc_glwidget.cpp \
	moc_options.cpp \
	moc_texteditor.cpp \
	moc_newmodeldialog.cpp \
	moc_newstatedialog.cpp \
	model.cpp \
	newmodeldialog.cpp \
	newstatedialog.cpp \
	options.cpp \
	rand/mersenne.cpp \
	res.cpp \
	synchronousmodel.cpp \
	texteditor.cpp
	

OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=cellula_m


all: $(SOURCES) $(EXECUTABLE)
	
$(EXECUTABLE): $(OBJECTS) ./CDparser/libCDparser.a
	$(CC) $(OBJECTS) $(LDFLAGS) -o $@
	
moc_%.cpp: %.h
	moc $(DEFINES) $(INCPATH) $< -o $@

.cpp.o:
	$(CC) $(CFLAGS) $(INCLUDE) $< -o $@

clean:
	rm -f $(OBJECTS) $(EXECUTABLE)

./CDparser/libCDparser.a:
	cd CDparser; make libCDparser.a

lex.TreeScanner.cc :
	flex treescanner.ll
