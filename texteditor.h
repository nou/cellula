/************************************************************************** 
*   Cellula, is cellular automata simulator accelerated on GPU.           *
*   Copyright (C) 2011  Dušan Poizl                                       *
*                                                                         *
*   This program is free software: you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation, either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
***************************************************************************/

#ifndef _TEXTEDITOR_H_NOU_
#define _TEXTEDITOR_H_NOU_

#include <QTextEdit>
#include <QTextDocument>
#include <QSyntaxHighlighter>
#include <QFile>
#include "subwindowinterface.h"
#include "model.h"

class Hightligter : public QSyntaxHighlighter
{
    struct HightlightingRule
    {
        QRegExp pattern;
        QTextCharFormat format;
    };
    QVector<HightlightingRule> rules;

    public:
    Hightligter(QTextDocument *document);
    void highlightBlock(const QString &text);
};

class TextEditor : public QTextEdit, public SubWindowInterface
{
    Q_OBJECT
    Hightligter *highlighter;
    QTextDocument document;
    QFile *fr;
    QString modelName;
    static int number;
    Model *model;
    bool compiled;
    public:
    /**
    Konstruktor ktory vytvori okno na definiciu modelu a aj samotny model.
    @param text definicia modelu.
    @param model model
    */
    TextEditor(const QString &text, Model *model, QWidget *parent = 0);
    ~TextEditor();
    static int getUntitledNumber(){ return number++; }
    QString getName(){ return modelName; }
    void setName(const QString &name);
    int compile(QTextEdit *log = 0);
    //SubWindowInterface metody
    void setSubWindowTitle();
    int load(const QString &file);
    QString save(const QString &dir);
    QString getFileName();
    Model* getModel(){ return model; }
    protected:
    void closeEvent(QCloseEvent *event);
    protected slots:
    void changed();
};

#endif // _TEXTEDITOR_H_NOU_
