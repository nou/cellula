/************************************************************************** 
*   Cellula, is cellular automata simulator accelerated on GPU.           *
*   Copyright (C) 2011  Dušan Poizl                                       *
*                                                                         *
*   This program is free software: you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation, either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
***************************************************************************/

#ifndef _SCANNER_HH_NOU_
#define _SCANNER_HH_NOU_

#ifndef YY_DECL

#define	YY_DECL			\
    TreeScanner::TreeToken           \
    TreeScanner::lex(	\
	int *yylval)
#endif

#ifndef __FLEX_LEXER_H
#define yyFlexLexer TreeScannerFlexLexer
#include <FlexLexer.h>
#undef yyFlexLexer
#endif

class TreeScanner : TreeScannerFlexLexer
{
    public:
    typedef enum
    {
        END,
        STATES,
        NEIGHBORS,
        NODES,
        //EQV,
        NUMBER
    }TreeToken;
    TreeScanner(std::istream *in = 0, std::ostream *out = 0) : TreeScannerFlexLexer(in, out)
    {
    }
    virtual TreeToken lex(int *yylval);
};

#endif // _SCANNER_HH_NOU_

