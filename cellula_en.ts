<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>CellulaMainWindow</name>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="143"/>
        <source>Súbor</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="cellulamainwindow.cpp" line="154"/>
        <source>Beh</source>
        <translation>Run</translation>
    </message>
    <message>
        <location filename="cellulamainwindow.cpp" line="159"/>
        <source>Okno</source>
        <translation>Window</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="163"/>
        <source>Zobraziť</source>
        <translation>Show</translation>
    </message>
    <message>
        <location filename="cellulamainwindow.cpp" line="172"/>
        <location filename="cellulamainwindow.cpp" line="289"/>
        <source>Pomoc</source>
        <translation>Help</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="204"/>
        <source>Hlavný panel nástrojov</source>
        <translation>Main toolbar</translation>
    </message>
    <message utf8="true">
        <source>Nahrávací panel nástrojov</source>
        <translation type="obsolete">Recording toolbar</translation>
    </message>
    <message>
        <location filename="cellulamainwindow.cpp" line="231"/>
        <source>Log</source>
        <translation>Log</translation>
    </message>
    <message>
        <location filename="cellulamainwindow.cpp" line="238"/>
        <source>Lua</source>
        <translation>Lua</translation>
    </message>
    <message>
        <location filename="cellulamainwindow.cpp" line="254"/>
        <source>Cellula</source>
        <translation>Cellula</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="260"/>
        <source>Nový model</source>
        <translation>New model</translation>
    </message>
    <message>
        <location filename="cellulamainwindow.cpp" line="262"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="263"/>
        <source>Nový stav</source>
        <translation>New state</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="265"/>
        <source>Otvoriť model</source>
        <translation>Open model</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="267"/>
        <source>Ulož model</source>
        <translation>Save model</translation>
    </message>
    <message>
        <location filename="cellulamainwindow.cpp" line="269"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="270"/>
        <source>Ulož obrázok</source>
        <translation>Save image</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="272"/>
        <source>Spusti simuláciu</source>
        <translation>Start simulation</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="274"/>
        <source>Pozastav simuláciu</source>
        <translation>Pause simulation</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="276"/>
        <source>Zastav simuláciu</source>
        <translation>Stop simulation</translation>
    </message>
    <message>
        <location filename="cellulamainwindow.cpp" line="278"/>
        <source>Nastavenia</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="cellulamainwindow.cpp" line="279"/>
        <source>Koniec</source>
        <translation>Quit</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="280"/>
        <source>Načítaj RLE</source>
        <translation>Load RLE</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="281"/>
        <source>Dlaždice</source>
        <translation>Tiled</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="282"/>
        <source>Kaskáda</source>
        <translation>Cascade</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="283"/>
        <source>Štart nahrávania</source>
        <translation>Start record</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="285"/>
        <source>Stop nahrávania</source>
        <translation>Stop record</translation>
    </message>
    <message>
        <location filename="cellulamainwindow.cpp" line="287"/>
        <source>O programe</source>
        <translation>About</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="288"/>
        <source>Vyčistiť log</source>
        <translation>Celar log</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="411"/>
        <location filename="cellulamainwindow.cpp" line="462"/>
        <source>Otvoriť súbor</source>
        <translation>Open file</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="411"/>
        <source>Definícia Cellula modelu alebo stavu (*.ma *.val *.tree)</source>
        <translation>Definition of Cellula model or state (*.ma *.val *.tree)</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="432"/>
        <source>Chyba pri otváraní súboru</source>
        <translation>Error during opening file</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="432"/>
        <source>Nepodarilo sa otvoriť súbor: 
%1</source>
        <translation>Can&apos;t open file:
%1</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="462"/>
        <source>Definícia Cellula stavu (*.rle)</source>
        <translation>Definition of Cellula state (*.rle)</translation>
    </message>
    <message>
        <location filename="cellulamainwindow.cpp" line="531"/>
        <source>O Cellula</source>
        <translation>About Cellula</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="531"/>
        <source>Cellula je simulátor celulárnych automatov akcelerovaných pomocoujednotiek GPU pomocou rozhrania OpenCL.
(C) 2011 Dušan Poizl poizl@zoznam.sk
Tento program je distribuovaný pod licenciou GPLv3</source>
        <translation>Cellula is simulator celular automata aclerated with GPU throuth OpenCL interface.
(C) 2011 Dušan Poizl poizl@zoznam.sk
This program is distributed under GPLv3</translation>
    </message>
    <message utf8="true">
        <source>Definícia Cellula modelu alebo stavu (*.m *.val *.tree)</source>
        <translation type="obsolete">Definition of Cellula model or state (*.m *.val *.tree)</translation>
    </message>
    <message utf8="true">
        <source>Definícia Cellula modelu alebo stavu (*.m, *.val, *.tree)</source>
        <translation type="obsolete">Definition of Cellula model or state (*.m, *.val, *.tree)</translation>
    </message>
    <message utf8="true">
        <source>Definícia Cellula modelu alebo stavu (*.rle)</source>
        <translation type="obsolete">Definition of Cellula model or state (*.rle)</translation>
    </message>
    <message utf8="true">
        <source>Definícia Cellula modelu alebo stavu (*)</source>
        <translation type="obsolete">Definition Cellula model or state (*)</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="472"/>
        <source>Chyba pri načítavaní súboru</source>
        <translation>Error while loading file</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="472"/>
        <source>Nepodarilo sa načítať súbor %1.
Nepodarilo sa otvoriť alebo chybný formát.</source>
        <translation>Failed to load file %1.
Failed to open file or file has wrong format.</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="517"/>
        <source>Uložit obrázok</source>
        <translation>Save image</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="517"/>
        <source>Obrázky (*.png *.bmp *.jpg *.jpeg *.tiff *.tif)</source>
        <translation>Images (*.png *.bmp *.jpg *.jpeg *.tiff *.tif)</translation>
    </message>
</context>
<context>
    <name>GLwidget</name>
    <message utf8="true">
        <location filename="glwidget.cpp" line="42"/>
        <source>Nový stav </source>
        <translation>New state </translation>
    </message>
    <message>
        <location filename="glwidget.cpp" line="190"/>
        <source>Chyba</source>
        <translation>Error</translation>
    </message>
    <message utf8="true">
        <location filename="glwidget.cpp" line="190"/>
        <source>Nepodarilo sa uložit obrázok do súboru
</source>
        <translation>Failed to save image to file</translation>
    </message>
</context>
<context>
    <name>NewModelDialog</name>
    <message>
        <location filename="newmodeldialog.cpp" line="32"/>
        <source>Typ modelu</source>
        <translation>Model type</translation>
    </message>
    <message utf8="true">
        <location filename="newmodeldialog.cpp" line="34"/>
        <source>Synchrónny model</source>
        <translation>Synchronous model</translation>
    </message>
    <message utf8="true">
        <location filename="newmodeldialog.cpp" line="35"/>
        <source>Asynchrónny model</source>
        <translation>Asynchronous model</translation>
    </message>
    <message utf8="true">
        <location filename="newmodeldialog.cpp" line="37"/>
        <source>Názov modelu</source>
        <translation>Model name</translation>
    </message>
    <message utf8="true">
        <location filename="newmodeldialog.cpp" line="49"/>
        <source>Chýba názov</source>
        <translation>Name missing</translation>
    </message>
    <message utf8="true">
        <location filename="newmodeldialog.cpp" line="49"/>
        <source>Chýba názov modelu. Zadajte nejaký názov.</source>
        <translation>Name of model missing. Enter some name.</translation>
    </message>
</context>
<context>
    <name>NewStateDialog</name>
    <message utf8="true">
        <location filename="newstatedialog.cpp" line="31"/>
        <source>Nový stav</source>
        <translation>New state</translation>
    </message>
    <message utf8="true">
        <location filename="newstatedialog.cpp" line="46"/>
        <source>Výška</source>
        <translation>Height</translation>
    </message>
    <message utf8="true">
        <location filename="newstatedialog.cpp" line="45"/>
        <source>Šírka</source>
        <translation>Width</translation>
    </message>
</context>
<context>
    <name>Options</name>
    <message>
        <location filename="options.cpp" line="29"/>
        <source>Nastavenia</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="options.cpp" line="103"/>
        <source>OpenCL</source>
        <translation>OpenCL</translation>
    </message>
    <message utf8="true">
        <location filename="options.cpp" line="133"/>
        <source>Použiť OpenCL/OpenGL interoperabilitu</source>
        <translation>Use OpenCL/OpenGL interoperability</translation>
    </message>
</context>
<context>
    <name>TextEditor</name>
    <message utf8="true">
        <source>Nový model </source>
        <translation type="obsolete">New model </translation>
    </message>
    <message utf8="true">
        <location filename="texteditor.cpp" line="90"/>
        <source>Nový model %1</source>
        <translation>New model %1</translation>
    </message>
    <message>
        <location filename="texteditor.cpp" line="124"/>
        <source>Chyba pri parsovani</source>
        <translation>Error during parsing</translation>
    </message>
    <message utf8="true">
        <location filename="texteditor.cpp" line="124"/>
        <source>Nepodarilo sa sparsovať definiciu modelu</source>
        <translation>Could not parse definition of model</translation>
    </message>
    <message utf8="true">
        <location filename="texteditor.cpp" line="173"/>
        <source>Uložiť súbor</source>
        <translation>Save file</translation>
    </message>
    <message utf8="true">
        <location filename="texteditor.cpp" line="173"/>
        <source>Definícia Cellula modelu alebo stavu (*)</source>
        <translation>Definition Cellula model or state (*)</translation>
    </message>
    <message utf8="true">
        <location filename="texteditor.cpp" line="180"/>
        <source>Chyba súboru</source>
        <translation>Error file</translation>
    </message>
    <message utf8="true">
        <location filename="texteditor.cpp" line="180"/>
        <source>Nepodarilo sa otvoriť súbor %1 na zápis
</source>
        <translation>Could not open file %1 for write
</translation>
    </message>
    <message utf8="true">
        <location filename="texteditor.cpp" line="204"/>
        <source>Uložiť?</source>
        <translation>Save?</translation>
    </message>
    <message utf8="true">
        <location filename="texteditor.cpp" line="204"/>
        <source>Model bol zmenený. Chcete uložiť zmeny?</source>
        <translation>The model was changed. Do you want save the changes?</translation>
    </message>
    <message utf8="true">
        <source>Model bol zmenený. Chcete zmeny uložiť?</source>
        <translation type="obsolete">The model was changed. Do you want save the changes?</translation>
    </message>
</context>
</TS>
