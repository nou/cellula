/************************************************************************** 
*   Cellula, is cellular automata simulator accelerated on GPU.           *
*   Copyright (C) 2011  Dušan Poizl                                       *
*                                                                         *
*   This program is free software: you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation, either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
***************************************************************************/

#include "newstatedialog.h"
#include <QVBoxLayout>
#include <QLabel>
#include <QGridLayout>

NewStateDialog::NewStateDialog(QWidget *parent) : QDialog(parent)
{
    ok_button = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    connect(ok_button, SIGNAL(accepted()), this, SLOT(accept()));
    connect(ok_button, SIGNAL(rejected()), this, SLOT(reject()));

    QGridLayout *grid_layout = new QGridLayout(this);
    setWindowTitle(trUtf8("Nový stav"));


    width = new QSpinBox(this);
    width->setMinimum(8);
    width->setMaximum(8192);
    width->setValue(32);
    width->setSingleStep(8);
    height = new QSpinBox(this);
    height->setValue(32);
    height->setMinimum(8);
    height->setMaximum(8192);
    height->setSingleStep(8);

    QLabel *width_label = new QLabel(trUtf8("Šírka"), this);
    QLabel *height_label = new QLabel(trUtf8("Výška"), this);

    grid_layout->addWidget(width_label, 0, 0);
    grid_layout->addWidget(height_label, 1, 0);
    grid_layout->addWidget(width, 0, 1);
    grid_layout->addWidget(height, 1, 1);

    grid_layout->addWidget(ok_button, 2, 1);
}

void NewStateDialog::accept()
{
    int w = width->value();
    int h = height->value();
    if(w&0x7)w = (w&(~0x7))+8;
    if(h&0x7)h = (h&(~0x7))+8;
    emit createNewState(w, h);
    QDialog::accept();
}

void NewStateDialog::reject()
{
    QDialog::reject();
}
