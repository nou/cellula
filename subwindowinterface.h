/************************************************************************** 
*   Cellula, is cellular automata simulator accelerated on GPU.           *
*   Copyright (C) 2011  Dušan Poizl                                       *
*                                                                         *
*   This program is free software: you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation, either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
***************************************************************************/

#ifndef _SUBWINDOWINTERFACE_H_NOU_
#define _SUBWINDOWINTERFACE_H_NOU_

#include <QMdiSubWindow>

/**
Interface ktory budu implementovat okna v MDI.
*/
class SubWindowInterface
{
    public:
    /**
    Tato metoda bude riesit nacitanie definicie modelu z ulozeneho XML
    @param file cesta k suboru z ktoreho sa ma citat
    */
    virtual int load(const QString &file) = 0;
    /**
    Tto metoda
    @param file cesta k suboru do ktoreho sa ma zapisovat
    */
    virtual QString save(const QString &dir) = 0;
    /**
    tato metoda nastavy titulok pre MDI sub okno
    */
    virtual void setSubWindowTitle() = 0;
    /**
    Tato metoda by mala vratit nazov pod ktorym sa ma ulozit dany model/stav
    */
    virtual QString getFileName() = 0;
};

#endif // _SUBWINDOWINTERFACE_H_NOU_
