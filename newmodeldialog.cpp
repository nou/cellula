/************************************************************************** 
*   Cellula, is cellular automata simulator accelerated on GPU.           *
*   Copyright (C) 2011  Dušan Poizl                                       *
*                                                                         *
*   This program is free software: you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation, either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
***************************************************************************/

#include "newmodeldialog.h"
#include <QGridLayout>
#include <QLabel>
#include <QMessageBox>
#include <QDialogButtonBox>

NewModelDialog::NewModelDialog(QWidget *parent) : QDialog(parent)
{
    QGridLayout *grid_layout = new QGridLayout(this);
    QDialogButtonBox *ok_button = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    connect(ok_button, SIGNAL(accepted()), this, SLOT(accept()));
    connect(ok_button, SIGNAL(rejected()), this, SLOT(reject()));

    QLabel *type_label = new QLabel(trUtf8("Typ modelu"), this);
    model_type = new QComboBox(this);
    model_type->addItem(trUtf8("Synchrónny model"));
    model_type->addItem(trUtf8("Asynchrónny model"));

    QLabel *name_label = new QLabel(trUtf8("Názov modelu"), this);
    name = new QLineEdit(this);

    grid_layout->addWidget(type_label, 0, 0);
    grid_layout->addWidget(model_type, 0, 1);
    grid_layout->addWidget(name_label, 1, 0);
    grid_layout->addWidget(name, 1, 1);
    grid_layout->addWidget(ok_button, 2, 1);
}

void NewModelDialog::accept()
{
    if(name->text().isEmpty())QMessageBox::information(this, trUtf8("Chýba názov"), trUtf8("Chýba názov modelu. Zadajte nejaký názov."));
    else
    {
        emit createNewModel(model_type->currentIndex(), name->text());
        name->setText("");
        QDialog::accept();
    }
}

void NewModelDialog::reject()
{
    name->setText("");
    QDialog::reject();
}
