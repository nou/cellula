/************************************************************************** 
*   Cellula, is cellular automata simulator accelerated on GPU.           *
*   Copyright (C) 2011  Dušan Poizl                                       *
*                                                                         *
*   This program is free software: you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation, either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
***************************************************************************/

#ifndef _MACHINE_H_NOU_
#define _MACHINE_H_NOU_

#include <iostream>
#include <vector>
#include <CL/cl.h>
#include <GL/gl.h>
#ifdef __linux__
#include <GL/glx.h>
#endif
#include "clutility.h"
#include "model.h"

using namespace std;

typedef vector<cl_device_id> cl_device_v;
typedef vector<cl_platform_id> cl_platform_v;
/**
Trieda zastresuje vytvorenie OpenCL kontextu v ktorom su spravovane vypoctove zariadenia
*/
class Machine
{
    private:
    cl_context context;
    cl_device_v device;
    vector<cl_command_queue> queues;
    vector<Model*> models;
    vector<size_t> local_work_sizes;
    bool gl_interoperability;
    bool deleting;
    void init(cl_context_properties *cl_properties, cl_device_v &devices);
    public:
    Machine(int platform_index = 0, cl_device_v devices = cl_device_v());
    Machine(cl_context_properties gl_context, cl_context_properties display, int platform_index = 0, cl_device_v devices = cl_device_v());
    ~Machine();
    static cl_platform_v getPlatforms();
    static cl_device_v getDevices(cl_platform_id platform_id, cl_device_type type = CL_DEVICE_TYPE_ALL);
    void run(cl_kernel kernel, size_t w, size_t h, int device = 0);
    void acquireGLObject(cl_mem tex, int device = 0);
    void releaseGLObject(cl_mem tex, int device = 0);
    void addChild(Model *child);
    void removeChild(Model *child);
    void finish(int device = -1);
    cl_event copy(cl_mem buf, void *data, size_t size, bool block = true);
    cl_event copy(void *data, cl_mem buf, size_t size, bool block = true);
    cl_event copy(cl_mem buf1, cl_mem buf2, size_t size, bool block = true);
    cl_event copyImage(cl_mem img, void *data, size_t origin[3], size_t region[3], bool block = true);
    cl_mem createBuffer(cl_mem_flags flags, size_t size, void *ptr = 0);
    cl_mem createFromGLTexture(cl_mem_flags flags, GLuint texture);
    cl_mem createTexture(cl_mem_flags flags, size_t w, size_t h, void *ptr = NULL);
    cl_program createProgram(const string &source);
    /** @return true ak je aktivna OpenGL/OpenCL interoperabilita */
    bool GLinteroprability(){ return gl_interoperability; };
    void setLocalworkSize(int index, size_t size);
    protected:
};

#endif
