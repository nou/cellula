/****************************************************************************
** Meta object code from reading C++ file 'glwidget.h'
**
** Created: Sun May 8 18:17:19 2011
**      by: The Qt Meta Object Compiler version 62 (Qt 4.6.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "glwidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'glwidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.6.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_GLwidget[] = {

 // content:
       4,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      10,    9,    9,    9, 0x0a,
      31,   26,    9,    9, 0x0a,
      56,   51,    9,    9, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_GLwidget[] = {
    "GLwidget\0\0runSimulation()\0file\0"
    "saveImage(QString&)\0name\0setName(QString&)\0"
};

const QMetaObject GLwidget::staticMetaObject = {
    { &QGLWidget::staticMetaObject, qt_meta_stringdata_GLwidget,
      qt_meta_data_GLwidget, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &GLwidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *GLwidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *GLwidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_GLwidget))
        return static_cast<void*>(const_cast< GLwidget*>(this));
    if (!strcmp(_clname, "SubWindowInterface"))
        return static_cast< SubWindowInterface*>(const_cast< GLwidget*>(this));
    return QGLWidget::qt_metacast(_clname);
}

int GLwidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGLWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: runSimulation(); break;
        case 1: saveImage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: setName((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
