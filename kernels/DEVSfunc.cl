#define DEVS_statecount(x) DEVS_statecount_function(current_state, event_vector, x)

float DEVS_statecount_function(_state current_state, _event *event_vector, float par)
{
    float count = 0;
    for(int i=0;i<NEIGHBOR;i++)
        count += event_vector[i].state == par ? 1 : 0;
    //count += current_state == par ? 1 : 0;
    return count;
}

#ifndef M_PI_F
#define M_PI_F 3.141592654f
#endif

// without parameter function

#define DEVS_truecount() DEVS_statecount_function(current_state, event_vector, 1)
#define DEVS_falsecount() DEVS_statecount_function(current_state, event_vector, 0)
#define DEVS_undefcount() DEVS_statecount_function(current_state, event_vector, NAN)
#define DEVS_time() current_time
#define DEVS_random() DEVS_random_function(seed)
#define DEVS_randomSign() DEVS_randomSign_function(seed)
#define DEVS_randInt(a) DEVS_round(DEVS_uniform_function(0, a, seed))

float DEVS_cellPos(float a){ return get_global_id(a); }

float DEVS_random_function(_seed *seed)
{
    int s = *seed;
    s = 1103515245*s + 12345;
    *seed = s;
    return s/UINT_MAX;
}

float DEVS_randomSign_function(uint *seed){ return DEVS_random() > 0.5f ? -1 : 1; }

//unary functions
float DEVS_abs(float par){				return fabs(par);}
float DEVS_acos(float par){				return acos(par);}
float DEVS_acosh(float par){			return acosh(par);}
float DEVS_asin(float par){				return asin(par);}
float DEVS_asinh(float par){			return asinh(par);}
float DEVS_atan(float par){				return atan(par);}
float DEVS_atanh(float par){			return atanh(par);}
float DEVS_cos(float par){				return cos(par);}
float DEVS_sec(float par){				return 1.0f/cos(par);}
float DEVS_sech(float par){				return 1.0f/cosh(par);}
float DEVS_exp(float par){				return exp(par);}
float DEVS_cosh(float par){				return cosh(par);}
float DEVS_fact(float par)
{
    float ret = 1,tmp;
    if(par == 0)return 1;
    if(par < 0 || fabs(fract(par, &tmp)) > 0.0001f)return NAN;
    unsigned int n = (unsigned int)par;
    while(n)
    {
        ret *= n;
        n--;
    }
    return ret;
}
float DEVS_fractional(float par){		float tmp; return fract(par, &tmp);}
float DEVS_ln(float par){				return log(par);}
float DEVS_log(float par){				return log10(par);}
float DEVS_round(float par){			return rint(par);}
float DEVS_cotan(float par){			return 1.0f/tan(par);}
float DEVS_cosec(float par){			return 1.0f/sin(par);}
float DEVS_cosech(float par){			return 1.0f/sinh(par);}
float DEVS_sign(float par){				if(par == 0)return 0; else return sign(par);}
float DEVS_sin(float par){				return sin(par);}
float DEVS_sinh(float par){				return sinh(par);}
float DEVS_sqrt(float par){				return sqrt(par);}
float DEVS_tan(float par){				return tan(par);}
float DEVS_tanh(float par){				return tanh(par);}
float DEVS_trunc(float par){			return trunc(par);}
float DEVS_truncUpper(float par){		return ceil(par);}
//TODO float DEVS_poisson(float par){	return (par);}
//TODO float DEVS_exponential(float par){	return (par);}
//TODO float DEVS_randInt(float par){	return (par);}
//TODO float DEVS_chi(float par){		return (par);}
float DEVS_asec(float par){				return acos(1.0f/par);}
float DEVS_acotan(float par){			return 0.5f*log((par+1)/(par-1));}
float DEVS_asech(float par){			return 2/(exp(par)+exp(-par));}
float DEVS_acosech(float par){			return par < -1.0f ? log((1-sqrt(1-par*par))/par) : log((1+sqrt(1-par*par))/par);}
//TODO float DEVS_nextPrime(float par){	return (par);}
float DEVS_radToDeg(float par){			return par/M_PI_F*180.0f;}
float DEVS_degToRad(float par){			return par/180.0f*M_PI_F;}
//TODO float DEVS_nth_prime(float par){	return (par);}
float DEVS_acotanh(float par){			return (par);}
float DEVS_CtoF(float par){				return par*9.0f/5.0f+32;}
float DEVS_CtoK(float par){				return par+273.15f;}
float DEVS_KtoC(float par){				return par-273.15f;}
float DEVS_KtoF(float par){				return par*9.0f/5.0f-459.67f;}
float DEVS_FtoC(float par){				return (par-32)*5.0f/9.0f;}
float DEVS_FtoK(float par){				return (par+459.67f)*5.0f/9.0f;}

// binary functions

float DEVS_comb(float a, float b){  return DEVS_fact(a)/(DEVS_fact(b)*DEVS_fact(a-b));	}
float DEVS_logn(float a, float b){	return log(a)/log(b);}
float DEVS_max(float a, float b){	return max(a, b);}
float DEVS_min(float a, float b){	return min(a, b);}
float DEVS_power(float a, float b){	return pow(a, b);}
float DEVS_remainder(float a, float b){	return remainder(a, b);}
float DEVS_root(float a, float b){	return rootn(a, b);}
//float DEVS_beta(float a, float b){	return ;}
//float DEVS_gamma(float a, float b){	return ;}
float DEVS_gcd(float a, float b)
{
    uint x = (uint)a;
    uint y = (uint)b;
    uint t;
    while(b!=0)
    {
        t = y;
        y = x%y;
        x = t;
    }
    return x;
}
float DEVS_lcm(float a, float b){	return abs((uint)a*(uint)b)/DEVS_gcd(a, b);}
//float DEVS_normal(float a, float b){	return ;}
//float DEVS_f(float a, float b){	return ;}
#define DEVS_uniform(a, b) DEVS_uniform_function(a, b, seed)
float DEVS_uniform_function(float a, float b, uint *seed)
{
    uint d = b-a;
    bool undef = isnan(a) || isnan(b) || a>b;
    return undef ? NAN : DEVS_random()*d+a;
}
//float DEVS_binomial(float a, float b){	return ;}
float DEVS_rectToPolar_r(float a, float b){	return sqrt(a*a+b*b);}
float DEVS_rectToPolar_angle(float a, float b)
{
    if(isnan(a) || isnan(b))return NAN;
    if(a>0)return atan(b/a);
    if(a<0)
    {
        if(b>=0)return atan(b/a)+M_PI_F;
        if(b<0)return atan(b/a)-M_PI_F;
    }
    if(a==0)
    {
        if(b>0)return M_PI_F/2;
        if(b<0)return -M_PI_F/2;
        if(b==0)return 0;
    }
    return 0;
}
float DEVS_polarToRect_x(float a, float b){	return a*cos(b);}
float DEVS_polarToRect_y(float a, float b){	return a*sin(b);}
float DEVS_hip(float a, float b){	return hypot(a, b);}

// operators

float DEVS_and(float a, float b)
{
    return a && b;
    //return (isnormal(a) && isnormal(b)) ? (a && b) : ((a==0 || b==0) ? 0 : NAN);
}
float DEVS_or(float a, float b)
{
    return a || b;
    //return (isnormal(a)&&isnormal(b) ? (a||b) : (a==1||b==1 ? 1 : NAN));
}
float DEVS_xor(float a, float b){ return isnan(a)||isnan(b) ? NAN : (a!=b ? 1 : 0);}
float DEVS_imp(float a, float b)
{
    float x = isnan(a) ? 0.5f : a==0 ? 0 : 1;
    float y = isnan(b) ? 0.5f : b==0 ? 0 : 1;
    if((x==0.5f && y<=0.5f) || (x==1&&y==0.5f))return NAN;
    return !(a>b);
}
float DEVS_eqv(float a, float b){ return isnan(a)&&isnan(b) ? 1 : (a==b ? 1 : 0);}
float DEVS_not(float a){ return isnan(a) ? NAN : (a==0 ? 1 : 0);}

float DEVS_if(float a, float b, float c){ return a ? b : c;}
float DEVS_ifu(float a, float b, float c, float d){ return isnan(a) ? (c) : (a ? b : c); }
