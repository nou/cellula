//__constant int neighbors[] = {-1,1, 1,1, -1,-1, 1,-1, 0,1, -1,0, 1,0, 0,-1};
__constant uint colors[] = {0,0,0,0, 255,255,255,0, 255,0,0,0, 0,255,0,0, 0,0,255,0, 255,255,0,0, 255,0,255,0, 0,255,255,0};

//#pragma OPENCL EXTENSION cl_amd_printf : enable

#include "types.cl"
#include "queue.cl"
#include "misc.cl"
#include "DEVSfunc.cl"

// sem pojdu prechodove funkcie funkcie

_event transfer_func_old(_state current_state, _time current_time, _event *event_vector, _seed *seed)
{
    _event new_event;
    if(current_state >= 1.0f && (DEVS_truecount() == 3 || DEVS_truecount() == 2)){ new_event.time = 100; new_event.state = 1; return new_event; }
    if(current_state == 0.0f && (DEVS_truecount() == 3)){ new_event.time = 100; new_event.state = 1; return new_event; }
    if(true){ new_event.time = 100; new_event.state = 0; return new_event; }
}

