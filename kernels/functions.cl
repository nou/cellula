bool even(float f)
{
    int a = convert_int(f);
    return ((f == NAN) || (a & 0x1) ? false : true);
}

bool odd(float f)
{
    int a = convert_int(f);
    return (a & 0x1 ? true : false);
}

bool isInt(float f)
{
    return fmin(f-floor(f),0x1.fffffep-1f) ? false : true;
}

int truecount(_cellspace_prop prop, int2 *coord, __global _cell *cells)
{
    int a=0;
    for(int i=0;i<NEIGHBOR;i++)a += 1 ? 1 : 0;
    return a;
}

/* oreze hodnotu na 0 az max-1 */
int wrap(int t, int max)
{
    int x = t < max ? t : t-max;
    return (x < 0 ? x+max : x);
}

