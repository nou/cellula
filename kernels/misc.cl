// vrati x tak aby bolo v rozsahu 0-y
int wrap(int x, int y)
{
    int r = (x < 0) ? x+y : x;
    return (r < y ? r : r-y);
}

int2 wrap2(int2 x, int2 y)
{
    return (int2)(wrap(x.x, y.x), wrap(x.y, y.y));
}

#define COORD(c) (c.y*get_global_size(0) + c.x)

//inicializuje hodnoty modelu
__kernel void init(__global _cell *cells, __global _state *data, __global _seed *seed)
{
    int2 position = (int2)(get_global_id(0), get_global_id(1));
    __global _cell *cell = cells+COORD(position);
    cell->queue.index = (int2)(0, 0);//nasatvim indexy vo fronte
    cell->queue.queue[0].time = 0.0f;//nastavim cas prvej udalosti
    cell->state = cell->queue.queue[0].state = data[COORD(position)];//nastavim stav bunky a stav do prvej udalosti
    cell->seed = seed[COORD(position)];
}

//medzikrok kedy sa prekopiruju nove indexy a usporiada frontu podla casu
__kernel void intermediateStep(__global _cell *cells)
{
    int2 coord = (int2)(get_global_id(0), get_global_id(1));
    cells[COORD(coord)].queue.index = cells[COORD(coord)].queue.index2;
    //TODO sort
}

_event transfer_func(_state current_state, _time current_time, _event *event_vector, _seed *seed);
_event transfer_func_old(_state current_state, _time current_time, _event *event_vector, _seed *seed);

//posunie frontu o dalsi casovy krok. vracia cas na ktory sa posunul.
_time advance_in_queues(__global _cell *cells, __local size_t *coord, __local int2 *index, _time current_time)
{
    _time min_time = INFINITY;
    __global _cell *cell;
    int idx;
    _time first,second;
    //najdem najmensi mozny cas za current_time
    for(int i=0;i<NEIGHBOR;i++)
    {
        cell = cells+coord[i];
        first = queue_first(&cell->queue, index[i]).time;
        first = first >= current_time ? first : min_time;
        second = queue_second(&cell->queue, index[i]).time;
        second = second >= current_time ? second : min_time;
        min_time = min(first, second);
    }
    // posunutie zaciatku fronty podla toho ci
    for(int i=0;i<NEIGHBOR;i++)
    {
        cell = cells+coord[i];
        idx = queue_second_idx(index[i]);
        index[i].x = queue_second(&cell->queue, index[i]).time >= min_time ? idx : index[i].x;
    }
    return min_time;
}

void create_event_vector(__global _cell *cells, _event *event_vector, __local size_t *coord, __local int2 *index)
{
    for(int i=0;i<NEIGHBOR;i++)
        event_vector[i] = queue_first(&(cells+coord[i])->queue, index[i]);
}

#define LOC_COORD (get_local_id(0)*get_local_size(1)+get_local_id(1))

__kernel __attribute__((reqd_work_group_size(8, 8, 1))) void takeStep(__global _cell *cells, _time start_time, _time time_window)
{
    _event event_vector[QUEUE_LEN];
    __local size_t coords[64][NEIGHBOR];
    int2 position = (int2)(get_global_id(0), get_global_id(1));
    int2 size = (int2)(get_global_size(0), get_global_size(1));
    int2 queue_idx = cells[COORD(position)].queue.index;
    __local int2 queues_idx[64][NEIGHBOR];
    //vypocita koordinaty susedov
    for(int i=0;i<NEIGHBOR;i++)
    {
        int2 n = (int2)(neighbors[i*2], neighbors[i*2+1]);
        coords[LOC_COORD][i] = COORD(wrap2(position+n, size));
        queues_idx[LOC_COORD][i] = cells[coords[LOC_COORD][i]].queue.index;
    }

    _event new_event;
    _state current_state;
    _seed seed = cells[COORD(position)].seed;
    current_state = cells[COORD(position)].state;
    _time current_time = start_time, old_time = INFINITY;

    while(1)
    {
        current_time = advance_in_queues(cells, coords[LOC_COORD], queues_idx[LOC_COORD], current_time);
        //if(position.x < 4 && position.y < 4)printf("%d %d %f %f %d\n", position.x, position.y, current_time, start_time, queue_size(&(cells+COORD(position))->queue, queue_idx));
        if(current_time < start_time || current_time >= time_window || current_time == old_time)break;

        create_event_vector(cells, event_vector, coords[LOC_COORD], queues_idx[LOC_COORD]);

        old_time = current_time;
        new_event = transfer_func(current_state, current_time, event_vector, &seed);
        new_event.time += current_time;
        if(new_event.state != current_state)
        {
            current_state = new_event.state;
            queue_push(&(cells+COORD(position))->queue, &queue_idx, new_event);
        }
    }
    while(1)
    {
        if(queue_second(&(cells+COORD(position))->queue, queue_idx).time <= time_window && queue_size(queue_idx) > 1)
            queue_pop(&queue_idx);
        else break;
    }
    queue_write_index2(&(cells+COORD(position))->queue, queue_idx);
    cells[COORD(position)].state = current_state;
    cells[COORD(position)].seed = seed;
}

__kernel void renderToTexture(__global _cell *cells, __write_only image2d_t tex)
{
    int2 coord = (int2)(get_global_id(0), get_global_id(1));
    _state state = cells[COORD(coord)].state;
    int i = convert_int(state);
    float4 color = (float4)(colors[i*4]/255.0f, colors[i*4+1]/255.0f, colors[i*4+2]/255.0f, colors[i*4+3]/255.0f);
    write_imagef(tex, coord, color);
}
