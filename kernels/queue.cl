int wrap(int x, int y);

// nakoniec fronty prida prvok
void queue_push(__global _queue *q, int2 *index, _event event)
{
    (*index).y = wrap((*index).y+1, QUEUE_LEN);
    q->queue[(*index).y] = event;
}
// odstrani zo zaciatku fronty prvok
void queue_pop(int2 *index)
{
    int idx = wrap((*index).x+1, QUEUE_LEN);
    (*index).x = (*index).x==(*index).y ? (*index).x : idx;// ci je v fronte uz len jeden prvok
}
// vrati prvy prvok fronty
_event queue_first(__global _queue *q, int2 index)
{
    return q->queue[index.x];
}
// vrati druhy prvok z fronty
_event queue_second(__global _queue *q, int2 index)
{
    int i = index.x == index.y ? index.x : wrap(index.x+1, QUEUE_LEN);
    return q->queue[i];
}
// vrati pocet prvkov vo fronte
int queue_size(int2 index)
{
    return (index.x<=index.y) ? (index.y-index.x+1) : (QUEUE_LEN-index.x+index.y+1);
}

void queue_write_index(__global _queue *q, int2 index)
{
    q->index = index;
}

void queue_write_index2(__global _queue *q, int2 index)
{
    q->index2 = index;
}

int queue_second_idx(int2 index)
{
    return index.x == index.y ? index.x : wrap(index.x+1, QUEUE_LEN);
}
