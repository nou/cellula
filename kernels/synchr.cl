#ifdef NEUMANN
__constant int2 neighbors[] = {
    //(int2)(0,1), (int2)(-1,0), (int2)(1,0), (int2)(0,-1), (int2)(0,0) };
    (int2)(0,-1), (int2)(-1,0), (int2)(1,0), (int2)(0,1), (int2)(0,0) };
#define NEIGHBORS 5
#endif

#ifdef MOORE
__constant int2 neighbors[] = {
    //(int2)(-1,1), (int2)(1,1), (int2)(-1,-1), (int2)(1,-1), (int2)(0, 1), (int2)(-1,0), (int2)(1,0), (int2)(0,-1), (int2)(0,0) };
    (int2)(-1,-1), (int2)(1,-1), (int2)(-1,1), (int2)(1,1), (int2)(0, -1), (int2)(-1,0), (int2)(1,0), (int2)(0,1), (int2)(0,0) };
    //(int2)(0,0), (int2)(0,1), (int2)(-1,1), (int2)(1,1), (int2)(0, -1), (int2)(-1,0), (int2)(1,0), (int2)(0,1), (int2)(0,0) };
#define NEIGHBORS 9
#endif

#pragma OPENCL EXTENSION cl_khr_byte_addressable_store : enable

int wrap(int x, int y)
{
    int r = (x < 0) ? x+y : x;
    return (r < y ? r : r-y);
}

int2 wrap2(int2 x, int2 y)
{
    return (int2)(wrap(x.x, y.x), wrap(x.y, y.y));
}

typedef int CellType;

__kernel void takeStep(__global CellType *cells, int which)
{
    size_t x = get_global_id(0);
    size_t y = get_global_id(1);
    size_t w = get_global_size(0);
    size_t h = get_global_size(1);
    int2 coord = (int2)(x, y);
    int2 sizes = (int2)(w, h);

    //if(x==0&&y==0)printf("lol %d\n", which);

    int node = sizeof(rule_tree)/sizeof(int) - NUM_STATES;
    int state;
    for(int i=0;i<NEIGHBORS;i++)
    {
        int2 c = wrap2(coord+neighbors[i], sizes);
        state = cells[(c.y*w+c.x)*2+which];
        node = rule_tree[node+state]*NUM_STATES;
    }
    which = (which+1)&1;
    cells[(y*w+x)*2+which] = node/NUM_STATES;
}
// prepina farby ktore sa zobrazuju WIREWORLD LIFE JVN
#define WIREWORLD
//#define JVN
//#define LIFE


#ifdef LIFE
__constant uint4 colors2[] = { (uint4)(0, 0, 0, 0), (uint4)(255, 255, 255, 255) };
#endif

#ifdef JVN
__constant uint4 colors2[] = { (uint4)(48,48,48,0), (uint4)(255,0,0,0), (uint4)(255,125,0,0), (uint4)(255,150,25,0), (uint4)(255,175,50,0),
        (uint4)(255,200,75,0), (uint4)(255,225,100,0), (uint4)(255,250,125,0), (uint4)(251,255,0,0), (uint4)(89,89,255,0),
        (uint4)(106,106,255,0), (uint4)(122,122,255,0), (uint4)(139,139,255,0), (uint4)(27,176,27,0), (uint4)(36,200,36,0),
        (uint4)(73,255,73,0), (uint4)(106,255,106,0), (uint4)(255,36,36,0), (uint4)(255,56,56,0), (uint4)(255,73,73,0),
        (uint4)(255,89,89,0), (uint4)(185,56,255,0), (uint4)(191,73,255,0), (uint4)(197,89,255,0), (uint4)(203,106,255,0),
        (uint4)(0,255,128,0), (uint4)(255,128,64,0), (uint4)(255,255,128,0), (uint4)(33,215,215,0) };
#endif

#ifdef WIREWORLD
__constant uint4 colors2[] = { (uint4)(48,48,48,0),(uint4)(0,128,255,0),(uint4)(255,255,255,0),(uint4)(255,128,0,0) };
#endif

__kernel void renderToTexture(__global CellType *cells, int which, __write_only image2d_t tex)
{
    int2 coord = (int2)(get_global_id(0), get_global_id(1));
    int w = get_global_size(0);
    int state = cells[(coord.y*w+coord.x)*2+which];
    float4 color = (float4)(convert_float4(colors2[state])/255.0f);
    write_imagef(tex, coord, color);
}
