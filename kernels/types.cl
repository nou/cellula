
typedef float _time;
typedef float _state;
typedef uint _seed;

typedef struct
{
    _time time;
    _state state;
}_event;

typedef struct
{
    int2 index;
    int2 index2;
    _event queue[QUEUE_LEN];
}_queue;

typedef struct
{
    _seed seed;
    _state state;
    _queue queue;
}_cell;
