//#pragma OPENCL EXTENSION cl_amd_printf : enable

#define FLOAT 1
#define UINT 2
#define COORD(c) (c.y*get_global_size(0) + c.x)
#define LOC_COORD(i) (get_local_id(1)*get_local_size(0) + get_local_id(0) + i)

//__constant int neighbors[] = {-1,1, 1,1, -1,-1, 1,-1, 0,1, -1,0, 1,0, 0,-1};
__constant uint colors[] = {0,0,0,0, 255,255,255,0, 255,0,0,0, 0,255,0,0, 0,0,255,0, 255,255,0,0, 255,0,255,0, 0,255,255,0};

#define TIME FLOAT//definuje aky typ je pouzity pri

#if (TIME == FLOAT)
typedef float _time;
#define NON INFINITY
#endif
#if (TIME == UINT)
typedef unsigned int _time;
#define NON 4294967295
#endif
typedef float _state;

typedef struct
{
    _time x;
    _state y;
}_event;

typedef struct
{
    /*indexy na zaciatok a koniec fronty. fronty nemoze byt prazdna preto ak je start a end rovnaky
    ukazuju na jeden posledny prvok. fronta je plna ak je end o jednu mensie ako start. fronta je
    implemntovana ako ring buffer */
    int start,end;
    int start2,end2;
    _event state;
    _event queue[QUEUE_LEN];
}_cell;

int wrap(int x, int y)
{
    int r = (x < 0) ? x+y : x;
    return (r < y ? r : r-y);
}

int2 wrap2(int2 x, int2 y)
{
    return (int2)(wrap(x.x, y.x), wrap(x.y, y.y));
}

int truecount(_event *event_vector)
{
    int t=0;
    for(int i=0;i<NEIGHBOR;i++)
        t += (event_vector[i].y) ? 1 : 0;
    return t;
}

_event transfer_func(_event current, _event *event_vector)
{
    _event new_event;
    new_event.x = new_event.y = 0;
    /*[life-rule]
    rule : 1 100 { (0,0) = 1 and (truecount = 3 or truecount = 4) }
    rule : 1 100 { (0,0) = 0 and truecount = 2 }
    rule : 0 100 { t }*/
    if(current.y >= 1.0f && (truecount(event_vector) == 3 || truecount(event_vector) == 2)){ new_event.x = 100; new_event.y = 1; return new_event; }
    if(current.y == 0.0f && (truecount(event_vector) == 3)){ new_event.x = 100; new_event.y = 1; return new_event; }
    if(true){ new_event.x = 100; new_event.y = 0; return new_event; }
    // return new_event;//iba aby nevracalo warning. ale je potrebne vratit
}

int get_next_index(__global _cell *cell, int index)
{
    index++;
    int start = cell->start;
    int end = cell->end;
    index = start <= end ? max(start, min(index, end)) : index;
    index = start > end ? (index > start ? wrap(index, QUEUE_LEN) : min(end, index)) : index;
    return index;
}

void create_event_vector(__global _cell *cells, _event *event_vector, int2 *coord, int *index)
{
    int2 p = (int2)(get_global_id(0), get_global_id(1));
    for(int i=0;i<NEIGHBOR;i++)
        event_vector[i] = cells[COORD(coord[i])].queue[index[i]];
}

//posunie frontu o dalsi casovy krok. vracia cas na ktory sa posunul. ak sa nedokae posunut vracia -1
_time advance_in_queues(__global _cell *cells, int2 *coord, int *index, _time current_time)
{
    _time min_time = NON,x;
    int idx;
    __global _cell *cell;
    //najdem najmensi mozny cas za current_time
    for(int i=0;i<NEIGHBOR;i++)
    {
        cell = cells+COORD(coord[i]);
        idx = get_next_index(cell, index[i]);
        x = cell->queue[idx].x;
        min_time = (x >= current_time ? min(min_time, x) : min_time);
    }
    //if(get_global_id(0) ==0 && get_global_id(1) ==0)printf("advance %f %f\n", min_time, current_time);
    //poposuvam indexy aby boli zarovno casu
    for(int i=0;i<NEIGHBOR;i++)
    {
        cell = cells+COORD(coord[i]);
        idx = get_next_index(cell, index[i]);
        index[i] = (cell->queue[idx].x <= min_time ? idx : index[i]);
    }
    return min_time;
}

int push(__global _cell *cell, int index, _event event)
{
    index = wrap(index+1, QUEUE_LEN);
    cell->queue[index] = event;
    return index;
}
//vrati ci maju dve udalosti rovnaky stav
bool event_cmp(_event a, _event b)
{
    return a.y == b.y;
}

__kernel void takeStep(__global _cell *cells, _time start_time, _time time_window)
{
    _event event_vector[NEIGHBOR];
    int2 size = (int2)(get_global_size(0), get_global_size(1));
    //_event event_vector[NEIGHBOR];//tu sa bude uchovavat aktualne udalosti ktore dorazili
    int2 coord[NEIGHBOR];//jednotlive absolutne koordinaty susedov
    int2 position = (int2)(get_global_id(0), get_global_id(1));
    int current_index[NEIGHBOR];
    int2 new_index = (int2)(cells[COORD(position)].start, cells[COORD(position)].end);//indexy fronty fronty novych uddalosti

    //vytvorime absolutne koordinaty a startovne indexy
    for(int i=0;i<NEIGHBOR;i++)
    {
        int2 n = (int2)(neighbors[i*2], neighbors[i*2+1]);
        coord[i] = wrap2(position+n, size);
        current_index[i] = cells[COORD(coord[i])].start;
    }
    //if(get_global_id(0) == 0 && get_global_id(1) == 0)printf("HOJ\n");
    _event new_event;
    _time current_time = start_time,old_time = NON;
    _event current_state = cells[COORD(position)].state;
    while(1)
    {
        current_time = advance_in_queues(cells, coord, current_index, current_time);

        if(current_time < start_time || current_time >= time_window || old_time == current_time)break;
        create_event_vector(cells, event_vector, coord, current_index);//vytvorim stavovy vektor

        old_time = current_time;
        new_event = transfer_func(current_state, event_vector);//zavolamprechodovu funkciu
        new_event.x += current_time;//pripocitam k ulozenemu oneskoreniu aktualny cas
        if(!event_cmp(current_state, new_event))//ulozim novu udalost do fronty iba ak je ina ako terajsia
        {
            new_index.y = push(cells+COORD(position), new_index.y, new_event);
            current_state = new_event;
        }
    }
    int i = new_index.x;;
    //posuniem zaciatocny index fronty na posledny event s casom < ako time_window
    while(1)
    {
        if(cells[COORD(position)].queue[i].x < time_window)new_index.x = i;
        if(i == cells[COORD(position)].end || cells[COORD(position)].queue[i].x >= time_window)break;
        i = get_next_index(cells+COORD(position), i);
    }
    cells[COORD(position)].state.y = current_state.y;//zapisem novy stav bunky
    cells[COORD(position)].start2 = new_index.x;//zapisem indexy novo zapisanych udalosti
    cells[COORD(position)].end2 = new_index.y;
}

__kernel void renderToTexture(__global _cell *cells, __global unsigned int *tex)
{
    size_t x = get_global_id(0);
    size_t y = get_global_id(1);
    size_t w = get_global_size(0);
    _event state = cells[COORD((int2)(x,y))].state;
    int i = convert_int(state.y);
    uint4 c = (uint4)(colors[i*4], colors[i*4+1], colors[i*4+2], colors[i*4+3]);
    tex[y*w+x] = c.x + c.y*256 + c.z*65536;
}

__kernel void init(__global _cell *cells, __global float *data)
{
    int2 position = (int2)(get_global_id(0), get_global_id(1));
    __global _cell *cell = cells+COORD(position);
    cell->start = cell->end = 0;
    cell->queue[0].x = 0.0f;
    cell->state.x = 0;
    cell->state.y = cell->queue[0].y = data[COORD(position)];
}

__kernel void intermediateStep(__global _cell *cells)
{
    int2 coord = (int2)(get_global_id(0), get_global_id(1));
    cells[COORD(coord)].start = cells[COORD(coord)].start2;
    cells[COORD(coord)].end = cells[COORD(coord)].end2;
}
