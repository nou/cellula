<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="sk_SK">
<context>
    <name>CellulaMainWindow</name>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="143"/>
        <source>Súbor</source>
        <translation>Súbor</translation>
    </message>
    <message>
        <location filename="cellulamainwindow.cpp" line="154"/>
        <source>Beh</source>
        <translation>Beh</translation>
    </message>
    <message>
        <location filename="cellulamainwindow.cpp" line="159"/>
        <source>Okno</source>
        <translation>Okno</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="163"/>
        <source>Zobraziť</source>
        <translation>Zobraziť</translation>
    </message>
    <message>
        <location filename="cellulamainwindow.cpp" line="172"/>
        <location filename="cellulamainwindow.cpp" line="289"/>
        <source>Pomoc</source>
        <translation>Pomoc</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="204"/>
        <source>Hlavný panel nástrojov</source>
        <translation>Hlavný panel nástrojov</translation>
    </message>
    <message utf8="true">
        <source>Nahrávací panel nástrojov</source>
        <translation type="obsolete">Nahrávací panel nástrojov</translation>
    </message>
    <message>
        <location filename="cellulamainwindow.cpp" line="231"/>
        <source>Log</source>
        <translation>Log</translation>
    </message>
    <message>
        <location filename="cellulamainwindow.cpp" line="238"/>
        <source>Lua</source>
        <translation>Lua</translation>
    </message>
    <message>
        <location filename="cellulamainwindow.cpp" line="254"/>
        <source>Cellula</source>
        <translation>Cellula</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="260"/>
        <source>Nový model</source>
        <translation>Nový model</translation>
    </message>
    <message>
        <location filename="cellulamainwindow.cpp" line="262"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="263"/>
        <source>Nový stav</source>
        <translation>Nový stav</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="265"/>
        <source>Otvoriť model</source>
        <translation>Otvoriť model</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="267"/>
        <source>Ulož model</source>
        <translation>Ulož model</translation>
    </message>
    <message>
        <location filename="cellulamainwindow.cpp" line="269"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="270"/>
        <source>Ulož obrázok</source>
        <translation>Ulož obrázok</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="272"/>
        <source>Spusti simuláciu</source>
        <translation>Spusti simuláciu</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="274"/>
        <source>Pozastav simuláciu</source>
        <translation>Pozastav simuláciu</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="276"/>
        <source>Zastav simuláciu</source>
        <translation>Zastav simuláciu</translation>
    </message>
    <message>
        <location filename="cellulamainwindow.cpp" line="278"/>
        <source>Nastavenia</source>
        <translation>Nastavenia</translation>
    </message>
    <message>
        <location filename="cellulamainwindow.cpp" line="279"/>
        <source>Koniec</source>
        <translation>Koniec</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="280"/>
        <source>Načítaj RLE</source>
        <translation>Načítaj RLE</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="281"/>
        <source>Dlaždice</source>
        <translation>Dlaždice</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="282"/>
        <source>Kaskáda</source>
        <translation>Kaskáda</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="283"/>
        <source>Štart nahrávania</source>
        <translation>Štart nahrávania</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="285"/>
        <source>Stop nahrávania</source>
        <translation>Stop nahrávania</translation>
    </message>
    <message>
        <location filename="cellulamainwindow.cpp" line="287"/>
        <source>O programe</source>
        <translation>O programe</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="288"/>
        <source>Vyčistiť log</source>
        <translation>Vyčistiť log</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="411"/>
        <location filename="cellulamainwindow.cpp" line="462"/>
        <source>Otvoriť súbor</source>
        <translation>Otvoriť súbor</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="411"/>
        <source>Definícia Cellula modelu alebo stavu (*.ma *.val *.tree)</source>
        <translation>Definícia Cellula modelu alebo stavu (*.ma *.val *.tree)</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="432"/>
        <source>Chyba pri otváraní súboru</source>
        <translation>Chyba pri otváraní súboru</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="432"/>
        <source>Nepodarilo sa otvoriť súbor: 
%1</source>
        <translation>Nepodarilo sa otvoriť súbor: 
%1</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="462"/>
        <source>Definícia Cellula stavu (*.rle)</source>
        <translation>Definícia Cellula stavu (*.rle)</translation>
    </message>
    <message>
        <location filename="cellulamainwindow.cpp" line="531"/>
        <source>O Cellula</source>
        <translation>O Cellula</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="531"/>
        <source>Cellula je simulátor celulárnych automatov akcelerovaných pomocoujednotiek GPU pomocou rozhrania OpenCL.
(C) 2011 Dušan Poizl poizl@zoznam.sk
Tento program je distribuovaný pod licenciou GPLv3</source>
        <translation>Cellula je simulátor celulárnych automatov akcelerovaných pomocou GPU pomocou rozhrania OpenCL.
(C) 2011 Dušan Poizl poizl@zoznam.sk
Tento program je distribuovaný pod licenciou GPLv3</translation>
    </message>
    <message utf8="true">
        <source>Definícia Cellula modelu alebo stavu (*.m *.val *.tree)</source>
        <translation type="obsolete">Definícia Cellula modelu alebo stavu (*.m *.val *.tree)</translation>
    </message>
    <message utf8="true">
        <source>Definícia Cellula modelu alebo stavu (*.m, *.val, *.tree)</source>
        <translation type="obsolete">Definícia Cellula modelu alebo stavu (*.m, *.val, *.tree)</translation>
    </message>
    <message utf8="true">
        <source>Definícia Cellula modelu alebo stavu (*.rle)</source>
        <translation type="obsolete">Definícia Cellula modelu alebo stavu (*.rle)</translation>
    </message>
    <message utf8="true">
        <source>Definícia Cellula modelu alebo stavu (*)</source>
        <translation type="obsolete">Definícia Cellula modelu alebo stavu (*)</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="472"/>
        <source>Chyba pri načítavaní súboru</source>
        <translation>Chyba pri načítavaní súboru</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="472"/>
        <source>Nepodarilo sa načítať súbor %1.
Nepodarilo sa otvoriť alebo chybný formát.</source>
        <translation>Nepodarilo sa načítať súbor %1.
Nepodarilo sa otvoriť súbor alebo má súbor chybný formát.</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="517"/>
        <source>Uložit obrázok</source>
        <translation>Uložiť obrázok</translation>
    </message>
    <message utf8="true">
        <location filename="cellulamainwindow.cpp" line="517"/>
        <source>Obrázky (*.png *.bmp *.jpg *.jpeg *.tiff *.tif)</source>
        <translation>Obrázky (*.png *.bmp *.jpg *.jpeg *.tiff *.tif)</translation>
    </message>
</context>
<context>
    <name>GLwidget</name>
    <message utf8="true">
        <location filename="glwidget.cpp" line="42"/>
        <source>Nový stav </source>
        <translation>Nový stav </translation>
    </message>
    <message>
        <location filename="glwidget.cpp" line="190"/>
        <source>Chyba</source>
        <translation>Chyba</translation>
    </message>
    <message utf8="true">
        <location filename="glwidget.cpp" line="190"/>
        <source>Nepodarilo sa uložit obrázok do súboru
</source>
        <translation>Nepodarilo sa uložit obrázok do súboru
</translation>
    </message>
</context>
<context>
    <name>NewModelDialog</name>
    <message>
        <location filename="newmodeldialog.cpp" line="32"/>
        <source>Typ modelu</source>
        <translation>Typ modelu</translation>
    </message>
    <message utf8="true">
        <location filename="newmodeldialog.cpp" line="34"/>
        <source>Synchrónny model</source>
        <translation>Synchrónny model</translation>
    </message>
    <message utf8="true">
        <location filename="newmodeldialog.cpp" line="35"/>
        <source>Asynchrónny model</source>
        <translation>Asynchrónny model</translation>
    </message>
    <message utf8="true">
        <location filename="newmodeldialog.cpp" line="37"/>
        <source>Názov modelu</source>
        <translation>Názov modelu</translation>
    </message>
    <message utf8="true">
        <location filename="newmodeldialog.cpp" line="49"/>
        <source>Chýba názov</source>
        <translation>Chýba názov</translation>
    </message>
    <message utf8="true">
        <location filename="newmodeldialog.cpp" line="49"/>
        <source>Chýba názov modelu. Zadajte nejaký názov.</source>
        <translation>Chýba názov modelu. Zadajte nejaký názov.</translation>
    </message>
</context>
<context>
    <name>NewStateDialog</name>
    <message utf8="true">
        <location filename="newstatedialog.cpp" line="31"/>
        <source>Nový stav</source>
        <translation>Nový stav</translation>
    </message>
    <message utf8="true">
        <location filename="newstatedialog.cpp" line="46"/>
        <source>Výška</source>
        <translation>Výška</translation>
    </message>
    <message utf8="true">
        <location filename="newstatedialog.cpp" line="45"/>
        <source>Šírka</source>
        <translation>Šírka</translation>
    </message>
</context>
<context>
    <name>Options</name>
    <message>
        <location filename="options.cpp" line="29"/>
        <source>Nastavenia</source>
        <translation>Nastavenia</translation>
    </message>
    <message>
        <location filename="options.cpp" line="103"/>
        <source>OpenCL</source>
        <translation>OpenCL</translation>
    </message>
    <message utf8="true">
        <location filename="options.cpp" line="133"/>
        <source>Použiť OpenCL/OpenGL interoperabilitu</source>
        <translation>Použiť OpenCL/OpenGL interoperabilitu</translation>
    </message>
</context>
<context>
    <name>TextEditor</name>
    <message utf8="true">
        <source>Nový model </source>
        <translation type="obsolete">Nový model </translation>
    </message>
    <message utf8="true">
        <location filename="texteditor.cpp" line="90"/>
        <source>Nový model %1</source>
        <translation>Nový model %1</translation>
    </message>
    <message>
        <location filename="texteditor.cpp" line="124"/>
        <source>Chyba pri parsovani</source>
        <translation>Chyba pri parsovani</translation>
    </message>
    <message utf8="true">
        <location filename="texteditor.cpp" line="124"/>
        <source>Nepodarilo sa sparsovať definiciu modelu</source>
        <translation>Nepodarilo sa sparsovať definiciu modelu</translation>
    </message>
    <message utf8="true">
        <location filename="texteditor.cpp" line="173"/>
        <source>Uložiť súbor</source>
        <translation>Uložiť súbor</translation>
    </message>
    <message utf8="true">
        <location filename="texteditor.cpp" line="173"/>
        <source>Definícia Cellula modelu alebo stavu (*)</source>
        <translation>Definícia Cellula modelu alebo stavu (*)</translation>
    </message>
    <message utf8="true">
        <location filename="texteditor.cpp" line="180"/>
        <source>Chyba súboru</source>
        <translation>Chyba súboru</translation>
    </message>
    <message utf8="true">
        <location filename="texteditor.cpp" line="180"/>
        <source>Nepodarilo sa otvoriť súbor %1 na zápis
</source>
        <translation>Nepodarilo sa otvoriť súbor %1 na zápis
</translation>
    </message>
    <message utf8="true">
        <location filename="texteditor.cpp" line="204"/>
        <source>Uložiť?</source>
        <translation>Uložiť?</translation>
    </message>
    <message utf8="true">
        <location filename="texteditor.cpp" line="204"/>
        <source>Model bol zmenený. Chcete uložiť zmeny?</source>
        <translation>Model bol zmenený. Chcete uložiť zmeny?</translation>
    </message>
</context>
</TS>
