/**************************************************************************
*   Cellula, is cellular automata simulator accelerated on GPU.           *
*   Copyright (C) 2011  Dušan Poizl                                       *
*                                                                         *
*   This program is free software: you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation, either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
***************************************************************************/

#ifndef _CELLULAMAINWINDOW_H_NOU_
#define _CELLULAMAINWINDOW_H_NOU_

#include <QApplication>
#include <QMainWindow>
#include <QToolBar>
#include <QAction>
#include <QCleanlooksStyle>
#include <QMdiArea>
#include  <QDir>
#include "helpview.h"
#include "glwidget.h"
#include "options.h"
#include "newstatedialog.h"
#include "newmodeldialog.h"

class TextEditor;
class Model;

class CellulaMainWindow : public QMainWindow
{
    Q_OBJECT
    private:
    QToolBar *main_toolbar, *simulation_toolbar, *record_toolbar;
    QAction *actionRun,*actionPause,*actionStop;
    QAction *actionOpen,*actionNew,*actionNewState,*actionSave,*actionSaveImage,*actionQuit,*actionOption,*actionLoadRLE;
    QAction *actionTile,*actionCascade;
    QAction *actionStartRecording, *actionStopRecording;
    QAction *actionAbout, *actionClearLog, *actionShowHelp;
    HelpView *helpview;
    QGLWidget *gl_widget;
    map<int, int> simulationSpeed2Index, simulationSteps2Index;
    QComboBox *simulationSpeed, *simulationSteps;
    Options *option_dialog;
    NewStateDialog *newstatedialog;
    NewModelDialog *newmodeldialog;
    Machine *machine;
    QMdiArea *mdi;
    QMdiSubWindow *lastSubWindow;
    QDockWidget *log, *lua_console;
    Model *model,*asyn_model;
    void createMenu();
    void createAction();
    void createToolbar();
    void createDock();
    void translateGUI();
    public:
    CellulaMainWindow();
    ~CellulaMainWindow();
    //metody pre luawrapper
    TextEditor* createNewModel(int type, string &definition);
    GLwidget* addCellSpace(CellSpace *space);
    public slots:
    void showOptionDialog();
    void showAbout();
    void createCLMachine();
    void startSimulation();
    void stopSimulation();
    void newModel();
    void newState();
    void createNewState(int width, int height);
    void createNewModel(int type, QString name);
    void loadRLE();
    void save();
    void load();
    void saveImage();
    void speedChanged(int index);
    void stepsChanged(int index);
    void mdiWindowChanged(QMdiSubWindow *window);
    void clearLog();
    void showHelp();
};

#endif // _CELLULAMAINWINDOW_H_NOU_
