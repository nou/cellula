/************************************************************************** 
*   Cellula, is cellular automata simulator accelerated on GPU.           *
*   Copyright (C) 2011  Dušan Poizl                                       *
*                                                                         *
*   This program is free software: you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation, either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
***************************************************************************/

#ifndef _OPTIONS_H_NOU_
#define _OPTIONS_H_NOU_

#include <QDialog>
#include <QDialogButtonBox>
#include <QComboBox>
#include <QTabWidget>
#include <QVBoxLayout>
#include <QStackedWidget>
#include <QSettings>
#include <QSpinBox>
#include <QCheckBox>
#include "machine.h"
#include "clutility.h"

class Options : public QDialog
{
    Q_OBJECT
    public:
    Options(QWidget *parent);
    ~Options();
    void accept();
    void reject();
    cl_device_v getDevices();
    int getPlatform(){ return platforms->currentIndex(); }
    bool useCLGLInteroperability();
    protected:
    private:
    QDialogButtonBox *ok_cancel;
    QTabWidget *tabs;
    QWidget *opencl;
    QVBoxLayout *vertical_layout;
    QComboBox *platforms;
    QStackedWidget *platform_stack;
    QCheckBox *cl_gl_interoperablity;
    QSettings settings;
    void createOpenCL();
};

#endif // _OPTIONS_H_NOU_
