/**************************************************************************
*   Cellula, is cellular automata simulator accelerated on GPU.           *
*   Copyright (C) 2011  Dušan Poizl                                       *
*                                                                         *
*   This program is free software: you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation, either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
***************************************************************************/

#include "cellulamainwindow.h"
#include <QTime>
#include <QMenuBar>
#include <QMenu>
#include <QMdiSubWindow>
#include <QFileDialog>
#include <QDockWidget>
#include <QMessageBox>
#include "texteditor.h"
#include "luaconsole.h"
#include <fstream>

CellulaMainWindow::CellulaMainWindow()
{
    createAction();
    createToolbar();
    createDock();
    createMenu();
    translateGUI();

    option_dialog = new Options(this);
    setWindowIcon(QIcon(":/images/glide.svg"));

    newstatedialog = new NewStateDialog(this);
    connect(newstatedialog, SIGNAL(createNewState(int, int)), this, SLOT(createNewState(int, int)));

    newmodeldialog = new NewModelDialog(this);
    connect(newmodeldialog, SIGNAL(createNewModel(int, QString)), this, SLOT(createNewModel(int, QString)));

    mdi = new QMdiArea(this);

    gl_widget = new QGLWidget(this);
    gl_widget->resize(1, 1);
    //gl_widget->show();

    setCentralWidget(mdi);
    setGeometry(0,0, 800, 600);
    connect(actionOption, SIGNAL(activated()), this, SLOT(showOptionDialog()));
    connect(actionNew, SIGNAL(activated()), this, SLOT(newModel()));
    connect(actionNewState, SIGNAL(activated()), this, SLOT(newState()));
    connect(actionRun, SIGNAL(activated()), this, SLOT(startSimulation()));
    connect(actionPause, SIGNAL(activated()), this, SLOT(stopSimulation()));
    connect(actionQuit, SIGNAL(activated()), this, SLOT(close()));
    connect(actionTile, SIGNAL(activated()), mdi, SLOT(tileSubWindows()));
    connect(actionCascade, SIGNAL(activated()), mdi, SLOT(cascadeSubWindows()));
    connect(actionOpen, SIGNAL(activated()), this, SLOT(load()));
    connect(actionSave, SIGNAL(activated()), this, SLOT(save()));
    connect(actionSaveImage, SIGNAL(activated()), this, SLOT(saveImage()));
    connect(actionLoadRLE, SIGNAL(activated()), this, SLOT(loadRLE()));
    connect(actionShowHelp, SIGNAL(activated()), this, SLOT(showHelp()));
    connect(actionAbout, SIGNAL(activated()), this, SLOT(showAbout()));
    connect(actionClearLog, SIGNAL(activated()), this, SLOT(clearLog()));
    connect(simulationSpeed, SIGNAL(currentIndexChanged(int)), this, SLOT(speedChanged(int)));
    connect(simulationSteps, SIGNAL(currentIndexChanged(int)), this, SLOT(stepsChanged(int)));
    connect(mdi, SIGNAL(subWindowActivated(QMdiSubWindow*)), this, SLOT(mdiWindowChanged(QMdiSubWindow*)));

    machine = 0;
    createCLMachine();

    QString text = "[top]"
        "components : life\n"
        "[life]\n"
        "type : cell\n"
        "width : 20\n"
        "height : 20\n"
        "delay : transport\n"
        "border : wrapped\n"
        "neighbors : life(-1,-1) \n"
        "neighbors : life(-1,0) \n"
        "neighbors : life(-1,1)\n"
        "neighbors : life(0,-1) \n"
        "neighbors : life(0,0) \n"
        "neighbors : life(0,1)\n"
        "neighbors : life(1,-1) \n"
        "neighbors : life(1,0) \n"
        "neighbors :life(1,1)\n"
        "\n"
        "[liferule]\n"
        "rule : 1 100 { (0,0) = 1 and (truecount = 3 or truecount = 4) }\n"
        "rule : 1 100 { (0,0) = 0 and truecount = 2 }\n"
        "rule : 0 100 { t }\n";

    /*TextEditor *editor = new TextEditor(text, new AsynchronousModel(machine, text.toStdString()), this);
    mdi->addSubWindow(editor);
    editor->setName("Live");*/

    lastSubWindow = 0;

    const char *src = "num_states=2\n" "num_neighbors=8\n" "num_nodes=32\n" "1 0 0\n"
            "2 0 0\n" "1 0 1\n" "2 0 2\n" "3 1 3\n" "1 1 1\n" "2 2 5\n"
			"3 3 6\n" "4 4 7\n" "2 5 0\n" "3 6 9\n" "4 7 10\n"
			"5 8 11\n" "3 9 1\n" "4 10 13\n" "5 11 14\n" "6 12 15\n"
			"3 1 1\n" "4 13 17\n" "5 14 18\n" "6 15 19\n" "7 16 20\n"
            "4 17 17\n" "5 18 22\n" "6 19 23\n" "7 20 24\n" "8 21 25\n"
			"5 22 22\n" "6 23 27\n" "7 24 28\n" "8 25 29\n" "9 26 30\n";
    model = new SynchronousModel(machine, src);
    QMdiSubWindow *sub = mdi->addSubWindow(new TextEditor(src, model, this));
    sub->show();
    dynamic_cast<TextEditor*>(sub->widget())->setSubWindowTitle();

    QSettings settings;
    restoreGeometry(settings.value("cellulamainwindow/geometry").toByteArray());
    restoreState(settings.value("cellulamainwindow/windowState").toByteArray());

    helpview = 0;
    helpview = new HelpView(this);
    //delete helpview;
}

CellulaMainWindow::~CellulaMainWindow()
{
    //delete model;
    delete mdi;
    delete machine;
    delete helpview;
    //delete asyn_model;
    QSettings settings;
    settings.setValue("cellulamainwindow/geometry", saveGeometry());
    settings.setValue("cellulamainwindow/windowState", saveState());
}

void CellulaMainWindow::createMenu()
{
    QMenuBar *menuBar = new QMenuBar(this);
    QMenu *file_menu = new QMenu(trUtf8("Súbor"), menuBar);
    file_menu->addAction(actionNew);
    file_menu->addAction(actionNewState);
    file_menu->addAction(actionOpen);
    file_menu->addAction(actionSave);
    file_menu->addAction(actionSaveImage);
    file_menu->addAction(actionLoadRLE);
    file_menu->addSeparator();
    file_menu->addAction(actionOption);
    file_menu->addAction(actionQuit);

    QMenu *run_menu = new QMenu(trUtf8("Beh"), menuBar);
    run_menu->addAction(actionRun);
    run_menu->addAction(actionPause);
    //run_menu->addAction(actionStop);

    QMenu *window_menu = new QMenu(trUtf8("Okno"), menuBar);
    window_menu->addAction(actionTile);
    window_menu->addAction(actionCascade);

    QMenu *view_menu = new QMenu(trUtf8("Zobraziť"), menuBar);
    view_menu->addAction(main_toolbar->toggleViewAction());
    view_menu->addAction(simulation_toolbar->toggleViewAction());
    //view_menu->addAction(record_toolbar->toggleViewAction());
    view_menu->addSeparator();
    view_menu->addAction(lua_console->toggleViewAction());
    view_menu->addAction(log->toggleViewAction());
    view_menu->addAction(actionClearLog);

    QMenu *help_menu = new QMenu(trUtf8("Pomoc"), menuBar);
    help_menu->addAction(actionShowHelp);
    help_menu->addAction(actionAbout);

    menuBar->addMenu(file_menu);
    menuBar->addMenu(run_menu);
    menuBar->addMenu(window_menu);
    menuBar->addMenu(view_menu);
    menuBar->addMenu(help_menu);

    setMenuBar(menuBar);
}

void CellulaMainWindow::createToolbar()
{
    simulationSpeed = new QComboBox(this);
    simulationSpeed->addItem("10s", 10000); simulationSpeed2Index[10000] = 0;
    simulationSpeed->addItem("5s", 5000); simulationSpeed2Index[5000] = 1;
    simulationSpeed->addItem("2s", 2000); simulationSpeed2Index[2000] = 2;
    simulationSpeed->addItem("1s", 1000); simulationSpeed2Index[1000] = 3;
    simulationSpeed->addItem("0.5s", 500); simulationSpeed2Index[500] = 4;
    simulationSpeed->addItem("0.2s", 200); simulationSpeed2Index[200] = 5;
    simulationSpeed->addItem("0.1s", 100); simulationSpeed2Index[100] = 6;
    simulationSpeed->setCurrentIndex(3);

    simulationSteps = new QComboBox(this);
    simulationSteps->addItem("1", 1); simulationSteps2Index[1] = 0;
    simulationSteps->addItem("10", 10); simulationSteps2Index[10] = 1;
    simulationSteps->addItem("100", 100); simulationSteps2Index[100] = 2;
    simulationSteps->addItem("1000", 1000); simulationSteps2Index[1000] = 3;
    simulationSteps->addItem("10000", 10000); simulationSteps2Index[10000] = 4;

    main_toolbar = new QToolBar(trUtf8("Hlavný panel nástrojov"), this);
    main_toolbar->setObjectName("main_toolbar");
    main_toolbar->addAction(actionNew);
    main_toolbar->addAction(actionNewState);
    main_toolbar->addAction(actionOpen);
    main_toolbar->addAction(actionSave);
    addToolBar(main_toolbar);

    simulation_toolbar = new QToolBar(trUtf8("Ovládanie simulácie"), this);
    simulation_toolbar->setObjectName("simulation_toolbar");
    simulation_toolbar->addAction(actionRun);
    simulation_toolbar->addAction(actionPause);
    //simulation_toolbar->addAction(actionStop);
    simulation_toolbar->addWidget(simulationSpeed);
    simulation_toolbar->addWidget(simulationSteps);
    addToolBar(simulation_toolbar);


    /*record_toolbar = new QToolBar(trUtf8("Nahrávací panel nástrojov"), this);
    record_toolbar->setObjectName("record_toolbar");
    record_toolbar->addAction(actionStartRecording);
    record_toolbar->addAction(actionStopRecording);
    addToolBar(record_toolbar);*/
}

void CellulaMainWindow::createDock()
{
    log = new QDockWidget(trUtf8("Log"), this);
    log->setObjectName("log_dock");
    QTextEdit *text = new QTextEdit();
    text->setReadOnly(true);
    LuaConsole *lua = new LuaConsole(this);
    log->setWidget(text);
    log->setFeatures(QDockWidget::DockWidgetVerticalTitleBar | QDockWidget::DockWidgetClosable | QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);
    lua_console = new QDockWidget(trUtf8("Lua"), this);
    lua_console->setObjectName("lua_dock");
    lua_console->setWidget(lua);
    lua_console->setFeatures(QDockWidget::DockWidgetVerticalTitleBar | QDockWidget::DockWidgetClosable | QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);


    QMainWindow::DockOptions dock_opt = dockOptions();
    setDockOptions(dock_opt | QMainWindow::ForceTabbedDocks);
    addDockWidget(Qt::BottomDockWidgetArea, log);
    addDockWidget(Qt::BottomDockWidgetArea, lua_console);
    setDockOptions(dock_opt);
    tabifyDockWidget(lua_console, log);
}

void CellulaMainWindow::translateGUI()
{
    setWindowTitle(trUtf8("Cellula"));
}

void CellulaMainWindow::createAction()
{
    QStyle *cur_style = QApplication::style();
    actionNew = new QAction(trUtf8("Nový model"), this);
    actionNew->setIcon(cur_style->standardIcon(QStyle::SP_FileIcon));
    actionNew->setShortcut(trUtf8("Ctrl+N"));
    actionNewState = new QAction(trUtf8("Nový stav"), this);
    actionNewState->setIcon(cur_style->standardIcon(QStyle::SP_FileLinkIcon));
    actionOpen = new QAction(trUtf8("Otvoriť model"), this);
    actionOpen->setIcon(cur_style->standardIcon(QStyle::SP_DirIcon));
    actionSave = new QAction(trUtf8("Ulož model"), this);
    actionSave->setIcon(cur_style->standardIcon(QStyle::SP_DriveFDIcon));
    actionSave->setShortcut(trUtf8("Ctrl+S"));
    actionSaveImage = new QAction(trUtf8("Ulož obrázok"), this);
    actionSave->setIcon(cur_style->standardIcon(QStyle::SP_DriveFDIcon));
    actionRun = new QAction(trUtf8("Spusti simuláciu"), this);
    actionRun->setIcon(cur_style->standardIcon(QStyle::SP_MediaPlay));
    actionPause = new QAction(trUtf8("Pozastav simuláciu"), this);
    actionPause->setIcon(cur_style->standardIcon(QStyle::SP_MediaPause));
    actionStop = new QAction(trUtf8("Zastav simuláciu"), this);
    actionStop->setIcon(cur_style->standardIcon(QStyle::SP_MediaStop));
    actionOption = new QAction(trUtf8("Nastavenia"), this);
    actionQuit = new QAction(trUtf8("Koniec"), this);
    actionLoadRLE = new QAction(trUtf8("Načítaj RLE"), this);
    actionTile = new QAction(trUtf8("Dlaždice"), this);
    actionCascade = new QAction(trUtf8("Kaskáda"), this);
    actionStartRecording = new QAction(trUtf8("Štart nahrávania"), this);
    actionStartRecording->setIcon(cur_style->standardIcon(QStyle::SP_DriveDVDIcon));
    actionStopRecording = new QAction(trUtf8("Stop nahrávania"), this);
    actionStopRecording->setIcon(cur_style->standardIcon(QStyle::SP_MediaStop));
    actionAbout = new QAction(trUtf8("O programe"), this);
    actionClearLog = new QAction(trUtf8("Vyčistiť log"), this);
    actionShowHelp = new QAction(trUtf8("Pomoc"), this);
    actionShowHelp->setShortcut(Qt::Key_F1);
}

void CellulaMainWindow::showOptionDialog()
{
    option_dialog->show();
}

void CellulaMainWindow::createCLMachine()
{
    gl_widget->makeCurrent();
    cl_context_properties gl_context, glx_hdc;
    #ifdef __linux__
    gl_context = (cl_context_properties)glXGetCurrentContext();
    glx_hdc = (cl_context_properties)glXGetCurrentDisplay();
    #endif
    #ifdef _WIN32
    gl_context = (cl_context_properties)wglGetCurrentContext();
    glx_hdc = (cl_context_properties)wglGetCurrentDC();
    #endif

    if(option_dialog->useCLGLInteroperability())machine = new Machine(gl_context, glx_hdc, option_dialog->getPlatform(), option_dialog->getDevices());
    else machine = new Machine(option_dialog->getPlatform(), option_dialog->getDevices());

    /*asyn_model = new AsynchronousModel(machine, "");
    GLwidget *asyn_widget = new GLwidget(QGLFormat(QGL::DoubleBuffer), this, new CellSpace(asyn_model, 16, 16), gl_widget);
    float asyn_data[] = {1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,
                         1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,
                         1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,
                         0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                         0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                         0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                         0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                         0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                         0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                         0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,
                         0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,
                         0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                         0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                         0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                         0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                         0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};
    ((AsynchronousModel*)asyn_model)->initialize(asyn_widget->getSpace(), asyn_data);
    QMdiSubWindow *subwindow = mdi->addSubWindow(asyn_widget);
    subwindow->show();
    asyn_widget->setSubWindowTitle();*/
}

void CellulaMainWindow::startSimulation()
{
    GLwidget *active_window = dynamic_cast<GLwidget*>(mdi->activeSubWindow() ? mdi->activeSubWindow()->widget() : 0);
    if(active_window)
        active_window->startSimulation();

    TextEditor *editor = dynamic_cast<TextEditor*>(mdi->activeSubWindow() ? mdi->activeSubWindow()->widget() : 0);
    QTextEdit *l = dynamic_cast<QTextEdit*>(log->widget());
    if(editor)editor->compile(l);
}

void CellulaMainWindow::stopSimulation()
{
    GLwidget *active_window = dynamic_cast<GLwidget*>(mdi->activeSubWindow() ? mdi->activeSubWindow()->widget() : 0);
    if(active_window)
        active_window->stopSimulation();
}

void CellulaMainWindow::newModel()
{
    newmodeldialog->show();
    /*GLwidget *new_glwidget = new GLwidget(QGLFormat(QGL::DoubleBuffer), this, new CellSpace(model, 256, 256), gl_widget);
    QMdiSubWindow *subwindow = mdi->addSubWindow(new_glwidget);
    subwindow->show();
    new_glwidget->setSubWindowTitle();*/
}

void CellulaMainWindow::createNewModel(int type, QString name)
{
    Model *model = 0;
    switch(type)
    {
        case 0:
        model = new SynchronousModel(machine, "");
        break;
        case 1:
        model = new AsynchronousModel(machine, "");
        break;
    }
    if(model)
    {
        TextEditor *editor = new TextEditor("", model, this);
        editor->setName(name);
        QMdiSubWindow *subwindow = mdi->addSubWindow(editor);
        subwindow->show();
        editor->setSubWindowTitle();
    }
}

void CellulaMainWindow::newState()
{
    newstatedialog->show();
}

void CellulaMainWindow::createNewState(int width, int height)
{
    TextEditor *editor = dynamic_cast<TextEditor*>(mdi->activeSubWindow() ? mdi->activeSubWindow()->widget() : 0);
    if(editor)
    {
        CellSpace *space = new CellSpace(editor->getModel(), width, height);
        AsynchronousModel *model = dynamic_cast<AsynchronousModel*>(editor->getModel());
        if(model)model->initialize(space);
        GLwidget *new_glwidget = new GLwidget(QGLFormat(QGL::DoubleBuffer), this, space, gl_widget);
        QMdiSubWindow *subwindow = mdi->addSubWindow(new_glwidget);
        subwindow->show();
        new_glwidget->setSubWindowTitle();
    }
}

void CellulaMainWindow::load()
{
    QSettings setting;
    QString dir = setting.value("file/recent_dir", QDir::homePath()).toString();
    QString file = QFileDialog::getOpenFileName(this, trUtf8("Otvoriť súbor"), dir, trUtf8("Definícia Cellula modelu alebo stavu (*.ma *.val *.tree)"));
    if(file.isNull())return;
    QFileInfo file_info(file);
    QDir recent_dir = file_info.absoluteDir();
    setting.setValue("file/recent_dir", recent_dir.absolutePath());

    QString suf = file_info.suffix();
    TextEditor *new_model = 0;

    if(suf=="tree")new_model = new TextEditor("", new SynchronousModel(machine, ""), this);
    else if(suf=="ma")new_model = new TextEditor("", new AsynchronousModel(machine, ""), this);
    else if(suf=="val")
    {
        if(lastSubWindow==0)return;
        TextEditor *editor = dynamic_cast<TextEditor*>(lastSubWindow->widget());
        if(editor==0)return;
        AsynchronousModel *model = dynamic_cast<AsynchronousModel*>(editor->getModel());
        if(model==0)return;
        QFile fr(file);
        if(!fr.open(QIODevice::ReadOnly))
        {
            QMessageBox::warning(this, trUtf8("Chyba pri otváraní súboru"), trUtf8("Nepodarilo sa otvoriť súbor: \n%1").arg(file));
            return;
        }

        QString val = fr.readAll();
        CellSpace *cellspace = new CellSpace(model, model->getWidth(), model->getHeight());
        model->loadVAL(val, cellspace);
        GLwidget *new_gl =new GLwidget(QGLFormat(QGL::DoubleBuffer), this, cellspace, gl_widget);
        mdi->addSubWindow(new_gl)->show();
        new_gl->setName(file);
    }

    if(new_model)
    {
        mdi->addSubWindow(new_model)->show();
        new_model->load(file);
        QTextEdit *l = dynamic_cast<QTextEdit*>(log->widget());
        new_model->compile(l);
    }
}

void CellulaMainWindow::loadRLE()
{
    TextEditor *editor = dynamic_cast<TextEditor*>(mdi->activeSubWindow() ? mdi->activeSubWindow()->widget() : 0);
    if(editor==0)return;
    SynchronousModel *model = dynamic_cast<SynchronousModel*>(editor->getModel());
    if(model==0)return;//pokracujeme iba ak je vybrany synchronny model

    QSettings setting;
    QString dir = setting.value("file/recent_dir_rle", QDir::homePath()).toString();
    QString file = QFileDialog::getOpenFileName(this, trUtf8("Otvoriť súbor"), dir, trUtf8("Definícia Cellula stavu (*.rle)"));
    if(file.isNull())return;
    QFileInfo file_info(file);
    QDir recent_dir = file_info.absoluteDir();
    setting.setValue("file/recent_dir_rle", recent_dir.absolutePath());

    size_t w,h;
    SynchronousModel::CellType *data;
    ifstream fr(file.toStdString().c_str(), ios::binary|ios::in);
    int ret = SynchronousModel::loadRLE(fr, &w, &h, &data);
    if(ret)QMessageBox::warning(this, trUtf8("Chyba pri načítavaní súboru"), trUtf8("Nepodarilo sa načítať súbor %1.\nNepodarilo sa otvoriť alebo chybný formát.").arg(file));
    else
    {
        CellSpace *space = new CellSpace(editor->getModel(), w, h);
        space->loadData(model->getCellSize()*w*h, data);
        delete data;
        GLwidget *new_glwidget = new GLwidget(QGLFormat(QGL::DoubleBuffer), this, space, gl_widget);
        QMdiSubWindow *subwindow = mdi->addSubWindow(new_glwidget);
        subwindow->show();
        new_glwidget->setName(file);
        new_glwidget->setSubWindowTitle();
    }
}

void CellulaMainWindow::save()
{
    QSettings setting;
    QString dir = setting.value("file/recent_dir", QDir::homePath()).toString();
    QMdiSubWindow *subWindow = mdi->activeSubWindow();
    if(subWindow)
    {
        SubWindowInterface *interface = dynamic_cast<SubWindowInterface*>(subWindow->widget());
        if(interface)
        {
            QString file = interface->save(dir);
            if(!file.isNull())
            {
                QFileInfo file_info(file);
                QDir recent_dir = file_info.absoluteDir();
                setting.setValue("file/recent_dir", recent_dir.absolutePath());
            }
        }
    }
}

void CellulaMainWindow::saveImage()
{
    QSettings setting;
    QString dir = setting.value("file/recent_dir", QDir::homePath()).toString();
    QMdiSubWindow *subWindow = mdi->activeSubWindow();
    if(subWindow)
    {
        GLwidget *glwid = dynamic_cast<GLwidget*>(subWindow->widget());
        if(glwid)
        {
            QString filename = QFileDialog::getSaveFileName(this, trUtf8("Uložit obrázok"), dir, trUtf8("Obrázky (*.png *.bmp *.jpg *.jpeg *.tiff *.tif)"));
            if(!filename.isNull())
            {
                glwid->saveImage(filename);
                QFileInfo file_info(filename);
                QDir recent_dir = file_info.absoluteDir();
                setting.setValue("file/recent_dir", recent_dir.absolutePath());
            }
        }
    }
}

void CellulaMainWindow::showAbout()
{
    QMessageBox::about(this, trUtf8("O Cellula"), trUtf8("Cellula je simulátor celulárnych automatov akcelerovaných pomocou"
                                                         "jednotiek GPU pomocou rozhrania OpenCL.\n"
                                                         "(C) 2011 Dušan Poizl poizl@zoznam.sk\n"
                                                         "Tento program je distribuovaný pod licenciou GPLv3"));
}

void CellulaMainWindow::speedChanged(int index)
{
    GLwidget *gl = dynamic_cast<GLwidget*>(mdi->activeSubWindow() ? mdi->activeSubWindow()->widget() : 0);
    if(gl)
    {
        gl->setSpeed(simulationSpeed->itemData(index).toInt());
    }
}

void CellulaMainWindow::stepsChanged(int index)
{
    GLwidget *gl = dynamic_cast<GLwidget*>(mdi->activeSubWindow() ? mdi->activeSubWindow()->widget() : 0);
    if(gl)
    {
        gl->setSteps(simulationSteps->itemData(index).toInt());
    }
}

void CellulaMainWindow::mdiWindowChanged(QMdiSubWindow *window)
{
    if(window)lastSubWindow = window;
    GLwidget *gl = dynamic_cast<GLwidget*>(mdi->activeSubWindow() ? mdi->activeSubWindow()->widget() : 0);
    if(gl)
    {
        simulationSpeed->setCurrentIndex(simulationSpeed2Index[gl->getSpeed()]);
        simulationSteps->setCurrentIndex(simulationSteps2Index[gl->getSteps()]);
    }
}

void CellulaMainWindow::clearLog()
{
    QTextEdit *l = dynamic_cast<QTextEdit*>(log->widget());
    if(l)l->clear();
}

void CellulaMainWindow::showHelp()
{
    helpview->show();
}

TextEditor* CellulaMainWindow::createNewModel(int type, string &definition)
{
    TextEditor *new_model = 0;
    if(type==1)new_model = new TextEditor(definition.c_str(), new SynchronousModel(machine, definition), this);
    if(type==2)new_model = new TextEditor(definition.c_str(), new AsynchronousModel(machine, definition), this);
    return new_model;
}

GLwidget* CellulaMainWindow::addCellSpace(CellSpace *space)
{
    GLwidget *new_glwidget = new GLwidget(QGLFormat(QGL::DoubleBuffer), this, space, gl_widget);
    mdi->addSubWindow(new_glwidget)->show();
    return new_glwidget;
}

