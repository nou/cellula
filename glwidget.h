/**************************************************************************
*   Cellula, is cellular automata simulator accelerated on GPU.           *
*   Copyright (C) 2011  Dušan Poizl                                       *
*                                                                         *
*   This program is free software: you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation, either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
***************************************************************************/

#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QtOpenGL/QGLWidget>
#include <QTimer>
#include <GL/gl.h>
#include "subwindowinterface.h"

class CellSpace;

class GLwidget : public QGLWidget, public SubWindowInterface
{
    Q_OBJECT
    protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int w, int h);
    public:
    GLwidget(const QGLFormat &format, QWidget *parent = 0, CellSpace *space = NULL, QGLWidget *share = 0);
    ~GLwidget();
    void loadTexture(int w, int h, unsigned char *texture_data);
    QSize sizeHint()const{ return QSize(300, 300); }
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);
    void startSimulation();
    void stopSimulation();
    void setSpeed(int ms);
    int getSpeed();
    void setSteps(int st){ steps = st; }
    int getSteps(){ return steps; }
    static int getUntitledNumber();
    CellSpace* getSpace(){ return space; }
    //SubWindowInterface metody
    void setSubWindowTitle();
    int load(const QString &file);
    QString save(const QString &dir);
    QString getFileName();
    public slots:
    void runSimulation();
    void saveImage(QString &file);
    void setName(QString &name){ spaceName = name; }
    private:
    GLuint tex;
    static bool initialized;
    static int number;
    QString spaceName;
    float zoom;
    float pos_x,pos_y;
    int w,h;
    int tex_w,tex_h;
    int mouse_x,mouse_y;
    int steps;
    QTimer timer;
    CellSpace *space;
};

#endif // GLWIDGET_H
