/**************************************************************************
*   Cellula, is cellular automata simulator accelerated on GPU.           *
*   Copyright (C) 2011  Dušan Poizl                                       *
*                                                                         *
*   This program is free software: you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation, either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
***************************************************************************/

#include "luaconsole.h"
#include <QKeyEvent>
#include <QTextBlock>
#include <tolua++.h>

extern int  tolua_luawrapper_open (lua_State* tolua_S);

LuaConsole::LuaConsole(CellulaMainWindow *mainw, QWidget * parent) : QTextEdit(parent)
{
    this->mainw = mainw;
    init();
}

LuaConsole::LuaConsole(const QString & text, CellulaMainWindow *mainw, QWidget * parent) : QTextEdit(text, parent)
{
    this->mainw = mainw;
    init();
}

void LuaConsole::init()
{
    luamain = new LuaMain(mainw);
    Lstate = luaL_newstate();
    luaL_openlibs(Lstate);
    lua_register(Lstate, "print", LuaConsole::luaPrint);
    lua_pushlightuserdata(Lstate, this);
    lua_setglobal(Lstate, "_LuaConsole");
    tolua_luawrapper_open(Lstate);
    tolua_pushusertype(Lstate, luamain, "LuaMain");
    lua_setglobal(Lstate, "I");

    const char *readfile = "function readfile(name) "
                            "local fr = io.open(name);"
                            "if fr ~= nil then "
                            "local def = fr:read(\"*a\");"
                            "fr:close();"
                            "return def;"
                            "else return nil;"
                            "end;end;";

    luaL_dostring(Lstate, readfile);
    luaL_dostring(Lstate, "readfile(\"life.ma\")");

    setAcceptRichText(false);
    history_index = 0;
    //connect(this, SIGNAL(cursorPositionChanged()), this, SLOT(moveCursorToEnd()));
}

LuaConsole::~LuaConsole()
{
    lua_close(Lstate);
}

void LuaConsole::keyPressEvent(QKeyEvent *e)
{
    if(document()->blockCount()-1 != textCursor().blockNumber())//ak nie je v poslednom riadku presunut ho tam
        moveCursor(QTextCursor::End, QTextCursor::MoveAnchor);

    if(e->key() == Qt::Key_Enter || e->key() == Qt::Key_Return)//ak stlacim enter vykonat posledny riadok
    {

        if(document()->lastBlock().text().isEmpty())
        {
            e->ignore();
            return;
        }
        else
        {
            execute();
            moveCursor(QTextCursor::End, QTextCursor::MoveAnchor);
        }
    }

    if(e->key() == Qt::Key_Up || e->key() == Qt::Key_Down)////historia
    {
        e->ignore();
        historyKey(e->key());
    }
    else QTextEdit::keyPressEvent(e);
}

void LuaConsole::execute()
{
    history.push_back(document()->lastBlock().text());
    history_index = history.size()-1;
    const char *s = document()->lastBlock().text().toStdString().c_str();
    int status = luaL_dostring(Lstate, s);
    if(status)
    {
        std::cerr << "Lua error: " << lua_tostring(Lstate, -1) << std::endl;
        lua_pop(Lstate, 1); // remove error message
    }
}

void LuaConsole::historyKey(int key)
{
    if(!history.empty())
    {
        moveCursor(QTextCursor::End, QTextCursor::MoveAnchor);
        moveCursor(QTextCursor::StartOfBlock, QTextCursor::KeepAnchor);
        QTextCursor cursor = textCursor();
        cursor.insertText(history[history_index]);
    }
    if(key == Qt::Key_Up)
    {
        history_index = --history_index > 0 ? history_index : 0;
    }
    if(key == Qt::Key_Down)
    {
        history_index = ++history_index < (int)history.size() ? history_index : history.size()-1;
    }
}

int LuaConsole::luaPrint(lua_State *L)
{
    stringstream s;
    lua_getglobal(L, "_LuaConsole");
    int argc = lua_gettop(L);

    LuaConsole *t = (LuaConsole*)lua_touserdata(L, argc--);

    for(int n=1; n<=argc; ++n)
        s << lua_tostring(L, n);

    t->luaPrint(s.str());
    return 0;
}

void LuaConsole::luaPrint(string s)
{
    moveCursor(QTextCursor::End, QTextCursor::MoveAnchor);
    QTextCursor t = textCursor();
    t.insertBlock();
    t.insertText(s.c_str());
}
