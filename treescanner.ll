%{
#include <string>
#include <stdlib.h>
#include <iostream>
#include "treescanner.h"

//#define yyterminate() return token::END
#define YY_NO_UNISTD_H

#define yyterminate() return TreeScanner::END

using namespace std;

%}


%option c++
%option prefix="TreeScanner"
%option batch
%option yywrap nounput
%option stack

%%

"#[^\n]*/\n"
num_states=     { return TreeScanner::STATES; }
num_neighbors=  { return TreeScanner::NEIGHBORS; }
num_nodes=      { return TreeScanner::NODES; }
[0-9]+          { *yylval = atoi(YYText()); return TreeScanner::NUMBER; }
.

%% /* dodatocny kod */

int TreeScannerFlexLexer::yylex()
{
    std::cerr << "in ExampleFlexLexer::yylex() !" << std::endl;
    return 0;
}

int TreeScannerFlexLexer::yywrap()
{
    return 1;
}
