/****************************************************************************
** Meta object code from reading C++ file 'cellulamainwindow.h'
**
** Created: Tue May 10 13:39:50 2011
**      by: The Qt Meta Object Compiler version 62 (Qt 4.6.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "cellulamainwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'cellulamainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.6.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_CellulaMainWindow[] = {

 // content:
       4,       // revision
       0,       // classname
       0,    0, // classinfo
      18,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      19,   18,   18,   18, 0x0a,
      38,   18,   18,   18, 0x0a,
      50,   18,   18,   18, 0x0a,
      68,   18,   18,   18, 0x0a,
      86,   18,   18,   18, 0x0a,
     103,   18,   18,   18, 0x0a,
     114,   18,   18,   18, 0x0a,
     138,  125,   18,   18, 0x0a,
     172,  162,   18,   18, 0x0a,
     200,   18,   18,   18, 0x0a,
     210,   18,   18,   18, 0x0a,
     217,   18,   18,   18, 0x0a,
     224,   18,   18,   18, 0x0a,
     242,  236,   18,   18, 0x0a,
     260,  236,   18,   18, 0x0a,
     285,  278,   18,   18, 0x0a,
     318,   18,   18,   18, 0x0a,
     329,   18,   18,   18, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_CellulaMainWindow[] = {
    "CellulaMainWindow\0\0showOptionDialog()\0"
    "showAbout()\0createCLMachine()\0"
    "startSimulation()\0stopSimulation()\0"
    "newModel()\0newState()\0width,height\0"
    "createNewState(int,int)\0type,name\0"
    "createNewModel(int,QString)\0loadRLE()\0"
    "save()\0load()\0saveImage()\0index\0"
    "speedChanged(int)\0stepsChanged(int)\0"
    "window\0mdiWindowChanged(QMdiSubWindow*)\0"
    "clearLog()\0showHelp()\0"
};

const QMetaObject CellulaMainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_CellulaMainWindow,
      qt_meta_data_CellulaMainWindow, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CellulaMainWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CellulaMainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CellulaMainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CellulaMainWindow))
        return static_cast<void*>(const_cast< CellulaMainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int CellulaMainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: showOptionDialog(); break;
        case 1: showAbout(); break;
        case 2: createCLMachine(); break;
        case 3: startSimulation(); break;
        case 4: stopSimulation(); break;
        case 5: newModel(); break;
        case 6: newState(); break;
        case 7: createNewState((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 8: createNewModel((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 9: loadRLE(); break;
        case 10: save(); break;
        case 11: load(); break;
        case 12: saveImage(); break;
        case 13: speedChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 14: stepsChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 15: mdiWindowChanged((*reinterpret_cast< QMdiSubWindow*(*)>(_a[1]))); break;
        case 16: clearLog(); break;
        case 17: showHelp(); break;
        default: ;
        }
        _id -= 18;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
