/************************************************************************** 
*   Cellula, is cellular automata simulator accelerated on GPU.           *
*   Copyright (C) 2011  Dušan Poizl                                       *
*                                                                         *
*   This program is free software: you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation, either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
***************************************************************************/

#ifndef _NEWMODELDIALOG_H_NOU_
#define _NEWMODELDIALOG_H_NOU_

#include <QDialog>
#include <QComboBox>
#include <QLineEdit>

class NewModelDialog : public QDialog
{
    Q_OBJECT
    public:
    NewModelDialog(QWidget *parent = 0);
    protected:
    private:
    void accept();
    void reject();
    QComboBox *model_type;
    QLineEdit *name;
    signals:
    void createNewModel(int type, QString name);
};

#endif // _NEWMODELDIALOG_H_NOU_
