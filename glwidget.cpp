/**************************************************************************
*   Cellula, is cellular automata simulator accelerated on GPU.           *
*   Copyright (C) 2011  Dušan Poizl                                       *
*                                                                         *
*   This program is free software: you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation, either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
***************************************************************************/

#include "glwidget.h"
#include <iostream>
#include <QPixmap>
#include <QImage>
#include <QMouseEvent>
#include <QMdiSubWindow>
#include <QFile>
#include <QMessageBox>
#include "cellspace.h"
#include "model.h"
#include "machine.h"

/**
@param format format ktory sa ma pouzit pri vytvarani OpenGL okna. pozri Qt dokumentaciu ku QGLWidget
@param parent Qt rodic
@param space model ktory sa priradi aktualnemu oknu
*/
GLwidget::GLwidget(const QGLFormat &format, QWidget *parent, CellSpace *space, QGLWidget *share) : QGLWidget(format, parent, share)
{
    pos_x = pos_y = 0;
    zoom = 1;
    this->space = space;
    if(space)connect(&timer, SIGNAL(timeout()), this, SLOT(runSimulation()));
    spaceName = trUtf8("Nový stav ")+QString::number(getUntitledNumber());
    steps = 1;
    timer.setInterval(1000);
}

GLwidget::~GLwidget()
{
    delete space;
    makeCurrent();
    glDeleteTextures(1, &tex);
}
/**
Inicializacia OpenGL
*/
void GLwidget::initializeGL()
{
    //glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glEnable(GL_TEXTURE_2D);
    if(!initialized)
    {
        std::cout << "Inicializujem OpenGL" << std::endl;
        std::cout << "\tVendor: \t" << glGetString(GL_VENDOR) << std::endl;
        std::cout << "\tRenderer:\t" << glGetString(GL_RENDERER) << std::endl;
        std::cout << "\tVersion:\t" << glGetString(GL_VERSION) << std::endl;
        //std::cout << "\tGLSL version:\t" << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;
        initialized = true;
    }
    if(space)
    {
        tex_w = space->getWidth();
        tex_h = space->getHeight();
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, tex_w, tex_h, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
        space->setGLTexture(tex);
        glFinish();
        space->renderToTexture();
    }
}
/**
Vykreslovanie OpenGL sceny
*/
void GLwidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, tex);
    glBegin(GL_QUADS);
    //glColor3f(1.0f, 0.0f, 0.0f);
    /*glTexCoord2d(0.0f, 0.0f);glVertex3f(-1.0f, -1.0f, 0.0f);
    glTexCoord2d(1.0f, 0.0f);glVertex3f( 1.0f, -1.0f, 0.0f);
    glTexCoord2d(1.0f, 1.0f);glVertex3f( 1.0f,  1.0f, 0.0f);
    glTexCoord2d(0.0f, 1.0f);glVertex3f(-1.0f,  1.0f, 0.0f);*/
    glTexCoord2d((float)pos_x/tex_w, (float)(pos_y+h*zoom)/tex_h);glVertex3f(-1.0f, -1.0f, 0.0f);
    glTexCoord2d((float)(pos_x+w*zoom)/tex_w, (float)(pos_y+h*zoom)/tex_h);glVertex3f( 1.0f, -1.0f, 0.0f);
    glTexCoord2d((float)(pos_x+w*zoom)/tex_w, (float)pos_y/tex_h);glVertex3f( 1.0f,  1.0f, 0.0f);
    glTexCoord2d((float)pos_x/tex_w, (float)pos_y/tex_h);glVertex3f(-1.0f,  1.0f, 0.0f);
    glEnd();

}

void GLwidget::resizeGL(int w,int h)
{
    glViewport(0, 0, w, h);
    this->w = w;
    this->h = h;
}
/**
Nacita texturu do OpenGL do akktualneho widgetu
@param w sirka textury
@param h vyska textury
@param texture pixelove data textury
*/
void GLwidget::loadTexture(int w, int h, unsigned char *texture)
{
    glBindTexture(GL_TEXTURE_2D, tex);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture);
    tex_w = w;
    tex_h = h;
}

void GLwidget::mouseMoveEvent(QMouseEvent *event)
{
    pos_x -= (event->x() - mouse_x)*zoom;
    pos_y -= (event->y() - mouse_y)*zoom;
    mouse_x = event->x();
    mouse_y = event->y();
    updateGL();
}

void GLwidget::mousePressEvent(QMouseEvent *event)
{
    mouse_x = event->x();
    mouse_y = event->y();
}

void GLwidget::wheelEvent(QWheelEvent *event)
{

    if(event->delta() > 0)//zoom in
    {
        zoom *= 2;
        /*pos_x = pos_x + w/zoom;
        pos_y = pos_y + h/zoom;*/
    }
    else//zoom out
    {
        if(zoom <= 1/16.f)return;//nad sestnastnasobne zvetsenie nejdem
        zoom /= 2;
        /*pos_x = pos_x - w/zoom;
        pos_y = pos_y - h/zoom;*/
    }
    updateGL();
}

bool GLwidget::initialized = false;
int GLwidget::number = 1;

void GLwidget::startSimulation()
{
    timer.start();
    runSimulation();
}

void GLwidget::stopSimulation()
{
    timer.stop();
}

void GLwidget::runSimulation()
{
    space->run(steps);
    makeCurrent();
    glFinish();
    space->renderToTexture();
    updateGL();
}

void GLwidget::saveImage(QString &file)
{
    makeCurrent();
    uchar *bits = new uchar[w*h*4];
    glReadPixels(0, 0, w, h, GL_BGRA, GL_UNSIGNED_INT_8_8_8_8_REV, bits);
    QImage img(bits, w, h, w*4, QImage::Format_RGB32);
    QImage flip = img.mirrored(false, true);
    cout << glGetError() << endl;
    if(!flip.save(file))QMessageBox::warning(this, trUtf8("Chyba"), trUtf8("Nepodarilo sa uložit obrázok do súboru\n").append(file));
}

void GLwidget::setSubWindowTitle()
{
    QMdiSubWindow *parent = dynamic_cast<QMdiSubWindow*>(parentWidget());
    if(parent)parent->setWindowTitle(spaceName);
}

int GLwidget::getUntitledNumber()
{
    return number++;
}

int GLwidget::load(const QString &file)
{
    QFile fr(file);
    if(!fr.exists())return fr.error();
    if(!fr.open(QIODevice::ReadOnly | QIODevice::Text))return fr.error();

    return 0;
}

QString GLwidget::save(const QString &dir)
{
    return "";
}

QString GLwidget::getFileName()
{
    return spaceName;
}

void GLwidget::setSpeed(int ms)
{
    timer.setInterval(ms);
}

int GLwidget::getSpeed()
{
    return timer.interval();
}
