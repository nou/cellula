/**************************************************************************
*   Cellula, is cellular automata simulator accelerated on GPU.           *
*   Copyright (C) 2011  Dušan Poizl                                       *
*                                                                         *
*   This program is free software: you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation, either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
***************************************************************************/

#include <QString>
#include <QCoreApplication>
#include "model.h"
#include "machine.h"


Model::Model(Machine *parent)
{
    this->parent = parent;
    parent->addChild(this);
    kernel = NULL;
    render_kernel = NULL;
    program = NULL;
}

Model::~Model()
{
    parent->removeChild(this);
    clReleaseKernel(kernel);
    clReleaseKernel(render_kernel);
    clReleaseProgram(program);
}
/**
Zaregistruje potomka.
@param child ukazovatel na potomka ktory sa ma pridat.
*/
void Model::addChild(CellSpace *child)
{
    patterns.push_back(child);
}
/**
Odstrani potomka zo zoznamu. Treba ho nasledne vymazat osobitne.
*/
void Model::removeChild(CellSpace *child)
{
    for(vector<CellSpace*>::iterator i = patterns.begin(); i != patterns.end();i++)
        if(*i == child){ patterns.erase(i); return; }
}

/**
Vytvori program pre model a kernel.
Zostvy program z predaneho zdrojoveho kodu. Ten musi definovat dva kernel.
takeStep a renderToTexture ktore su potom pouzivane.
@param source zdrojovy kod modelu
@param options volitelne volby kompilacie
*/
cl_int Model::build(string source, string options)
{
    if(kernel){ clReleaseKernel(kernel); kernel = 0; }
    if(render_kernel){ clReleaseKernel(render_kernel); render_kernel = 0; }
    if(program){ clReleaseProgram(program); program = 0; }
    cl_int err_code;
    //size_t len[] = { source.length() };
    program = parent->createProgram(source);
    err_code = clBuildProgram(program, 0, NULL, options.c_str(), NULL, NULL);
    if(err_code){ cerr << "Nepodarilo sa skompilovat program " << endl << getBuildLog(program) << endl; return err_code; }
    kernel = clCreateKernel(program, "takeStep", &err_code);
    if(err_code){ cerr << "Nepodarilo sa vytvorit kernel "; clGetError(err_code); return err_code; }
    render_kernel = clCreateKernel(program, "renderToTexture", &err_code);
    if(err_code){ cerr << "Nepodarilo sa vytvorit kresliaci kernel "; clGetError(err_code); return err_code; }
    return err_code;
}

string Model::getKernelInclude()
{
    QString path = QCoreApplication::applicationDirPath();
    return "-I "+path.toStdString()+"/kernels/";
}
