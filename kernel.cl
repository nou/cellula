#define NUM_NEIGHBORS 8

typedef struct
{
    float time;
    float state;
}event;

typedef struct
{
    event events[4];
    int num;
}queue;

typedef struct
{
    float state;
    queue queues[NUM_NEIGHBORS];
}cell;

void neighbors_fill(float *neighbors)
{
}

float transition()
{
    /*rule : 1 100 { (0,0) = 1 and (truecount = 3 or truecount = 4) }
rule : 1 100 { (0,0) = 0 and truecount = 2 }
rule : 0 100 { t }
    if()*/
    return 0.0f;
}

int wrap(int x, int y)
{
    int r = x%y;
    if(r<0)return y+r;
    else return r;
}

__kernel void size()
{
    int r = remainder(-22, 5);
    printf("hello %d\n", r);
}

__kernel void take_step(__global cell *cellspace)
{
    size_t x = get_global_id(0);
    size_t y = get_global_id(0);
    float susedia[9];
    neighbors_fill(susedia);
    printf("%f %f %f\n", susedia[0], susedia[1], susedia[2]);
}

__kernel void init(__global cell *cell_space, __constant int *neighbors, __global float *init_state)
{
    size_t x = get_global_id(0);
    size_t y = get_global_id(1);
    size_t width = get_global_size(0);
    size_t height = get_global_size(1);

    cell_space[x+y*height].state = init_state[x+y*height];
    for(int i=0;i<NUM_NEIGHBORS;i++)
    {
        size_t pos = wrap(x+neighbors[i*2], width)+wrap(y+neighbors[i*2+1], height)*width;
        cell_space[pos].queues[i].events[0].state = init_state[x*width+y];
        cell_space[pos].queues[i].events[0].time = 0.0f;
        cell_space[pos].queues[i].num = 1;
    }
}

/* Usporiada udalosti v jednotlivych frontach podla casu */
__kernel void sort_queue(__global cell *cell_space)
{
    size_t x = get_global_id(0);
    size_t y = get_global_id(1);
    size_t width = get_global_size(0);

    __global cell *c = cell_space+x+y*width;
    int min;
    event ev;
    for(int i=0;i<NUM_NEIGHBORS;i++)//prejdem vsetkych susedov
    {
        for(int o=0;o<c->queues[i].num;o++)
        {
            min = o;
            for(int p=o+1;p<c->queues[i].num;p++)if(c->queues[i].events[p].time < c->queues[i].events[min].time)min=p;//najdem udalost s najmensim casom
            ev = c->queues[i].events[min];//vymenim
            c->queues[i].events[min] = c->queues[i].events[o];
            c->queues[i].events[o] = ev;
        }
    }
}
